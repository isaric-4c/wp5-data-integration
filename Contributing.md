# Contributing

This pipeline is open source and as such is open to contributions. Contributions include bug reports, feature requests,
code changes and documentation.


## Bug reports

To report a problem, open an Issue on this repo. A good bug report should describe, as specifically as possible,
observed vs. expected results. Extra information like stack traces, software versions and log messages are useful, as
well as a minimal test case.

If you do not have access to open Issues on this repo, contact the developers and they can do it.


## Change requests

As for bug reports, open or request an Issue on this repo to request changes. Even if you'll be making the changes
yourself, it's still good to have a ticket so people can assign and track work, add notes, discuss and collaborate, etc.


## Code changes

All code contributions should be made via a Merge Request on this repo from a separate branch or fork. This way, all
changes to master are peer-reviewed and the main copy of the software is always functional.

To do this:

- Create a new Git branch in your local clone
- Make changes, providing brief but descriptive commit messages
- Ensure that there is a relevant test for your changes - either a new test case if it's a new feature, or an updated
  test if it's a fix or improvement. Ideally, the resulting Merge Request should always have changes both in the main
  `data_linkage` module and in `tests`. You can run the repo's tests locally as per Readme.md.
- Push your changes
- Open a Merge Request with an appropriate name and summary, noting also any Issues that this Merge Request addresses. The
  automated build will also run to ensure all the tests still pass. Changes do not have to be complete for a Merge
  Request to be opened - indeed, one of their best uses is for reviewing all the changes before accepting or requesting
  changes.

The maintainers should then:

- Review the MR, either requesting further changes or accepting it and incorporating the changes from the branch into
  master.
- Bump the version in `data_linkage/__init__.py`
  - If making a major backward-incompatible change, increment the first number, e.g. `v1.5 -> v2.0`
  - If making a backward-compatible change, increment the second number, e.g. `v1.5.1 -> v1.6`
  - If making an emergency fix, increment as a third number, e.g. `v1.5 -> v1.5.1`
- Update Changelog.md with details from the MR
- Push these changes to master
  - The commit message should be 'Release <new_version_number>'
- Create a new Git tag/release


## Documentation

Updates to documentation are always useful. There are several types of documentation in this repo:

- Docstrings in individual functions/classes/modules
- Code comments describing complicated bits, reasoning behind implementations, workarounds and any other gotchas and
  jankiness
- Readme documentation for the pipeline in the top level of this repo
- Release documentation for the data, output alongside the pipeline's main table outputs
  - This includes readmes in the release folder and database table/column descriptions

Documentation updates should be made in the same way as for code changes above.
