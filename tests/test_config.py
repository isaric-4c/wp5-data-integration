from tests import TestLinkage
from data_linkage.config import cfg


class TestConfig(TestLinkage):
    def test_getitem(self):
        self.assertEqual(
            'tests/assets/clinical_data/???_ccp_*/topline.csv',
            cfg['raw_data_sources']['clinical_topline']
        )

    def test_query(self):
        self.assertIsNone(cfg.query('does', 'not', 'exist'))
        self.assertEqual(
            'tests/assets/clinical_data/???_ccp_*/topline.csv',
            cfg.query('raw_data_sources', 'clinical_topline')
        )
