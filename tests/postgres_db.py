"""
Connect with `postgresql://<user>:<password>@localhost:5432/isaric`. The host network option is not needed - the -p port
publishing maps the port in the container at, e.g, 172.17.0.1 to the port on localhost.
"""

import os
import yaml
import subprocess
import sqlalchemy
from time import sleep
from data_linkage.config import cfg

postgres_version = None
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'environment.yml')) as f:
    env_file = yaml.safe_load(f)
    for d in env_file['dependencies']:
        if d.startswith('postgresql'):
            postgres_version = d.split('=')[-1]
            break


def docker_available():
    try:  # docker running/not running
        return subprocess.call(['docker', 'info'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == 0
    except FileNotFoundError:  # docker not available
        return False


def start_postgres_container():
    pgpass = os.getenv('PGPASSFILE')
    with open(pgpass) as f:
        pg_password = f.readline().strip().split(':')[4]

    args = [
        'docker',
        'run',
        '-d',
        '--rm',
        '--name', 'isaric_db',
        '-e', 'POSTGRES_USER=' + cfg['postgres']['user'],
        '-e', 'POSTGRES_PASSWORD=' + pg_password,
        '-e', 'POSTGRES_DB=' + cfg['postgres']['database'],
        '-p', '5432:5432',
        f'postgres:{postgres_version}'
    ]
    print(args)
    subprocess.check_call(args)
    sleep(3)


def postgres_container_is_running():
    args = ['docker', 'container', 'ls', "--format='{{.Names}}'"]
    print(args)
    running_containers = subprocess.check_output(['docker', 'container', 'ls', '--format={{.Names}}']).decode().split('\n')
    return 'isaric_db' in running_containers


def stop_postgres_container():
    args = ['docker', 'stop', 'isaric_db']
    print(args)
    subprocess.check_call(args)


def create_roles():
    db = cfg['postgres']['database']
    user = cfg['postgres']['user']
    dbhost = cfg['postgres']['server']

    engine = sqlalchemy.create_engine(
        f'postgresql://{user}@{dbhost}/{db}',
        isolation_level='AUTOCOMMIT'
    )
    with engine.connect() as c:
        c.execute('CREATE ROLE isaric_admin')
        c.execute(f'GRANT ALL PRIVILEGES ON DATABASE {db} TO isaric_admin')
        c.execute('CREATE SCHEMA isaric AUTHORIZATION isaric_admin')
        c.execute(f'GRANT isaric_admin TO {user}')
        print('Ready')


if __name__ == '__main__':
    start_postgres_container()
    create_roles()
