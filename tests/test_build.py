import os
import csv
import glob
import shutil
from io import StringIO
from unittest import skipIf
from unittest.mock import Mock, MagicMock, patch
from data_linkage import release, __version__
from data_linkage.db_engine import engine
from data_linkage.config import cfg
from data_linkage.utils import discover_files
from tests import postgres_db, TestLinkage, postgres_environment
from bin import data_release


class TestRelease(TestLinkage):
    @staticmethod
    def patched_connect(fake_index):
        return patch(
            'data_linkage.db_engine.engine.SQLiteDatabase.connect',
            return_value=MagicMock(
                __enter__=Mock(
                    return_value=Mock(
                        execute=Mock(
                            return_value=tuple(
                                {'name': k, 'value': v}
                                for k, v in fake_index.items()
                            )
                        )
                    )
                )
            )
        )

    def test_should_run(self):
        fake_database = engine.SQLiteDatabase()

        fake_index = {
            k: v.source
            for k, v in fake_database.primary_data_sources.items()
        }
        fake_index['automation_version'] = __version__
        with self.patched_connect(fake_index):
            self.assertFalse(release.ReleaseManager(db=fake_database).should_run())

        fake_index['automation_version'] = 'some_other_version'
        with self.patched_connect(fake_index):
            self.assertTrue(release.ReleaseManager(db=fake_database).should_run())

        fake_index['automation_version'] = __version__
        fake_index['linkage_sample_to_patient'] = 'some_other_file'
        with self.patched_connect(fake_index):
            self.assertTrue(release.ReleaseManager(db=fake_database).should_run())


class TestSQLiteBuild(TestLinkage):
    name = 'sqlite'
    cached_config = None

    @classmethod
    def setUpClass(cls):
        cfg.content['database_engine'] = 'sqlite'
        cls.prepare_test()

        data_release.main(['--force'])
        cls.db = engine.SQLiteDatabase()

    @classmethod
    def prepare_test(cls):
        cls.cached_config = cfg.content
        cfg._content = cls.translate_config(cfg.content, '/postgres_release', f'/{cls.name}_release')

        # clean up beforehand
        for f in glob.glob(os.path.join(cfg['data_release_path'], '20*')) + glob.glob(os.path.join(cfg['data_release_path'], 'subset_export', '20*')):
            shutil.rmtree(f)

        os.makedirs(cfg['data_release_path'], exist_ok=True)

    @classmethod
    def translate_config(cls, config, replace_str, replace_with):
        if isinstance(config, dict):
            return {k: cls.translate_config(v, replace_str, replace_with) for k, v in config.items()}
        elif isinstance(config, list):
            return [cls.translate_config(v, replace_str, replace_with) for v in config]
        elif isinstance(config, str):
            return config.replace(replace_str, replace_with)
        else:
            return config

    @classmethod
    def tearDownClass(cls):
        cfg._content = cls.cached_config


def make_test_func(table_name, output_file):
    def test_func(_self):
        with _self.db.connect() as c:
            table = _self.db.all_tables[table_name]
            cursor = c.execute(table.table.select())

            with StringIO(newline=None) as i:
                writer = csv.writer(i)
                writer.writerow(cursor.keys())
                for line in cursor:
                    writer.writerow(line)

                obs = i.getvalue()

        with open(output_file) as f:
            exp = f.read()
            _self.assertEqual(obs, exp)

    return test_func


@skipIf(postgres_environment is None, 'Postgres Docker environment not available')
class TestPostgresBuild(TestSQLiteBuild):
    name = 'postgres'

    @classmethod
    def setUpClass(cls):
        cfg.content['database_engine'] = 'postgres'
        cls.prepare_test()

        # pgpass files need to be -rw------- but git doesn't support rw permissions, only executability
        for f in glob.glob('tests/assets/*pgpass'):
            os.chmod(f, 0o600)  # -rw-------

        if postgres_environment == 'local_transient':
            postgres_db.start_postgres_container()
            postgres_db.create_roles()
        elif postgres_environment == 'ci':
            postgres_db.create_roles()

        data_release.main(['--force'])
        cls.db = engine.PostgresDatabase()

    @classmethod
    def tearDownClass(cls):
        if postgres_environment == 'local_transient' and postgres_db.postgres_container_is_running():
            postgres_db.stop_postgres_container()

        super().tearDownClass()


for output_file in discover_files('tests/assets/expected_outputs/*.csv'):
    if 'postgres' in output_file:
        table_name = os.path.basename(output_file).replace('_postgres.csv', '')
        setattr(TestPostgresBuild, 'test_' + table_name, make_test_func(table_name, output_file))
    elif 'sqlite' in output_file:
        table_name = os.path.basename(output_file).replace('_sqlite.csv', '')
        setattr(TestSQLiteBuild, 'test_' + table_name, make_test_func(table_name, output_file))

    else:
        table_name = os.path.basename(output_file).replace('.csv', '')
        setattr(TestSQLiteBuild, 'test_' + table_name, make_test_func(table_name, output_file))
        setattr(TestPostgresBuild, 'test_' + table_name, make_test_func(table_name, output_file))
