import datetime
import pandas
from tests import TestLinkage
from data_linkage import cleaning

na = pandas.NA
nan = float('nan')


class TestCleaning(TestLinkage):
    def test_canonical_isaric_id_pattern(self):
        self.assertTrue(cleaning.canonical_isaric_id_pattern.match('ABCDE-1234'))
        self.assertFalse(cleaning.canonical_isaric_id_pattern.match('ABCDE/1234'))

    def test_clean_isaric_id(self):
        patient_ids = pandas.DataFrame.from_records(
            (
                ('REMRQ0001',   'REMRQ-0001'),
                ('REMRQ--0001', 'REMRQ-0001'),
                ('REMRQ/0001',  'REMRQ-0001'),
                ('REMRQ001',    'REMRQ001')
            ),
            columns=('dirty', 'clean')
        )
        with self.assertLogs(cleaning.logger) as l:
            self.assertSeriesEqual(
                cleaning.clean_isaric_id(patient_ids['dirty']),
                patient_ids['clean']
            )
            self.assertListEqual(l.output, ["INFO:cleaning:Found 1 invalid ISARIC IDs: ['REMRQ001']"])

    def test_clean_timepoint(self):
        timepoints = pandas.DataFrame.from_records(
            (
                ('Day 1',               'Day 1'),
                ('1',                   'Day 1'),
                (1,                     'Day 1'),
                ('Recruitment',         'Day 1'),
                ('Recruitment / Day 1', 'Day 1'),
                ('Convalescent',        'Convalescent'),
                ('NA',                  nan),
                ('N/A',                 nan),
                (None,                  nan),
                ('None',                nan),
                (float('nan'),          nan),
                ('',                    nan),
                ('unknown',             nan)
            ),
            columns=('dirty', 'clean')
        )
        with self.assertLogs(cleaning.logger) as l:
            self.assertSeriesEqual(cleaning.clean_timepoint(timepoints['dirty']), timepoints['clean'])
            self.assertListEqual(
                l.output,
                [
                    "INFO:cleaning:Could not cast 7 timepoints: [nan 'NA' 'N/A' None 'None' '' 'unknown']"
                ]
            )

    def test_clean_boolean(self):
        bools = pandas.DataFrame.from_records(
            (
                (True,      True),
                (False,     False),
                ('Yes',     True),
                ('YES',     True),
                ('y',       True),
                ('1',       True),
                (1,         True),
                ('no',      False),
                ('NO',      False),
                ('n',       False),
                ('0',       False),
                (0,         False),
                (None,      None),
                ('',        None),
                ('unknown', None)
            ),
            columns=('dirty', 'clean')
        )
        self.assertSeriesEqual(cleaning.clean_boolean(bools['dirty']), bools['clean'])

    def test_clean_float(self):
        floats = pandas.Series((1, 1.0, 1.2345, '1', '1.0', '1.2345', None, '',  'unknown'))
        exp = pandas.Series((1.0,  1.0, 1.2345, 1.0, 1.0,   1.2345,   nan,  nan, nan), dtype='float')
        self.assertSeriesEqual(cleaning.clean_float(floats), exp)

    def test_clean_int(self):
        ints = pandas.Series((1, 1.0, 1.2345, 1.9, '1', '1.0', '1.2345', '1.9', None,  '',  'unknown'))
        exp = pandas.Series((1,  1,   1,      2,   1,   1,     1,        2,     None,  None, None), dtype='Int64')
        self.assertSeriesEqual(cleaning.clean_int(ints), exp)

    def test_clean_date(self):
        d = datetime.date(2021, 5, 21)
        dates = pandas.DataFrame.from_records(
            (
                (d,            d),
                (datetime.datetime(2021, 5, 21, 12, 30), d),
                (20210521,     d),
                ('20210521',   d),
                ('2021-05-21', d),
                ('2021/05/21', d),
                ('21052021',   d),
                ('21-05-2021', d),
                ('21/05/2021', d),
                ('210521',     None),
                (None,         None),
                ('',           None),
                ('unknown',    None)
            ),
            columns=('dirty', 'clean')
        )
        self.assertSeriesEqual(cleaning.clean_date(dates['dirty']), dates['clean'])

    def test_time(self):
        d = datetime.time(12, 30, 30)
        dm = datetime.time(12, 30, 30, 500)
        times = pandas.DataFrame.from_records(
            (
                (d,          d),
                (dm,         dm),
                (datetime.datetime(2021, 5, 21, 12, 30, 30), d),
                ('12:30:30', d),
                ('1230',     None),
                (1230,       None),
                (None,       None),
                ('',         None),
                ('unkown',   None)
            ),
            columns=('dirty', 'clean')
        )
        self.assertSeriesEqual(cleaning.clean_time(times['dirty']), times['clean'])

    def test_clean_numeric_id(self):
        ids = pandas.DataFrame.from_records(
            (
                (202211000001.0,   '202211000001'),
                (202211000001,     '202211000001'),
                ('202211000001.0', '202211000001'),
                ('202211000001',   '202211000001'),
                ('unknown', 'unknown'),
                ('', ''),
                (None, None),
            ),
            columns=('dirty', 'clean')
        )
        self.assertSeriesEqual(cleaning.clean_numeric_id(ids['dirty']), ids['clean'])
