from io import StringIO
from tests import TestLinkage
from unittest.mock import Mock, MagicMock, patch, call
from bin import possible_mappings

fake_ccp_data = '''subjid,dag_id,redcap_event_name,redcap_data_access_group,place_name,daily_dsstdat
ABCDE-0001,ABCDE,Day 1 (Arm 1: TIER 0),some_place,Some Place,2020-10-01
ABCDE-0001,ABCDE,Day 3 (Arm 1: TIER 0),some_place,Some Place,2020-10-03
ABCDE-0001,ABCDE,Day 9 (Arm 1: TIER 0),some_place,Some Place,2020-10-09
ABCDE-1001,ABCDE,Day 1 (Arm 1: TIER 0),some_place,Some Place,2020-10-04
ABCDE-1001,ABCDE,Day 9 (Arm 1: TIER 0),some_place,Some Place,2020-10-12
'''

fake_lims_data = {'CCP1': '20201003'}


def fake_get_lines(data_source, Kit_ID):
    if Kit_ID in fake_lims_data:
        return [{'Date_Collected': fake_lims_data[Kit_ID]}]


class TestPossibleMappings(TestLinkage):
    def test_possible_mappings(self):
        with patch('bin.possible_mappings.Database', return_value=Mock(get_lines=fake_get_lines)), \
             patch('builtins.open', return_value=MagicMock(__enter__=Mock(return_value=StringIO(fake_ccp_data)))), \
             patch('builtins.print') as patched_print:
            args = [
                'ccp.csv',
                'CCP1', 'ABCDE', '001',
                'CCP2', 'ABCDE', '001'
            ]
            possible_mappings.main(args)
            patched_print.assert_has_calls(
                (
                    call('Sample CCP1, site code ABCDE, patient number 001, collected: 2020-10-03'),
                    call('Potential matches:'),
                    call('ABCDE-0001 - clinical observations between 2020-10-01 and 2020-10-09, including one on the day sample was taken'),
                    call('Sample CCP2, site code ABCDE, patient number 001, collected: None'),
                    call('Potential matches:'),
                    call('ABCDE-0001 - clinical observations between 2020-10-01 and 2020-10-09'),
                    call('ABCDE-1001 - clinical observations between 2020-10-04 and 2020-10-12')
                )
            )
