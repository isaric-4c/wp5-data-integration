import pandas
from data_linkage import utils, data_sources
from data_linkage.exceptions import InvalidDataException
from unittest.mock import Mock, patch
from tests import TestLinkage


class TestUtils(TestLinkage):
    def test_read_file(self):
        with self.assertRaises(InvalidDataException):
            utils.read_file('some_xls_file.xls')

        df = utils.read_file('tests/assets/wp5_rnaseq/RNA_Data_ISARIC_All_20200803.xlsx')
        self.assertListEqual(
            list(df.columns)[:5],
            ['Patient ID', 'Date Taken', 'Sample ID', 'Day ', 'CRF Barcode']  # raw colnames
        )

        self.assertDataFrameEqual(
            utils.read_file('tests/assets/wp5_rnaseq/filtered_rnaseq_samples_list_20201104.csv'),
            pandas.DataFrame.from_records(
                (
                    ('CCP0001', 'a user', '2020-11-04', 'high_intergenic'),
                    ('10394162_NS2000_75', 'a user', '2020-11-04', 'high_intergenic')
                ),
                columns=('rnaseq_sample_id', 'name', 'date', 'reason')
            )
        )

        with patch('pandas.read_csv') as patched_read_csv:
            utils.read_file('tests/assets/wp6_cytokines/2021-05-31_ISARIC_plasma_cytokine_data.csv')  # utf-8-sig
            patched_read_csv.assert_called_with(
                'tests/assets/wp6_cytokines/2021-05-31_ISARIC_plasma_cytokine_data.csv',
                index_col=False,
                delimiter=',',
                encoding='utf-8-sig',
                float_precision='round_trip'
            )
            patched_read_csv.reset_mock()

            utils.read_file('tests/assets/wp6_cytokines/faecal_cytokines_20210604.csv')  # utf-8
            patched_read_csv.assert_called_with(
                'tests/assets/wp6_cytokines/faecal_cytokines_20210604.csv',
                index_col=False,
                delimiter=',',
                encoding='utf-8',
                float_precision='round_trip'
            )
            patched_read_csv.reset_mock()

            utils.read_file('tests/assets/wp5_rnaseq/filtered_rnaseq_samples_list_20201104.csv')  # tab-separated
            patched_read_csv.assert_called_with(
                'tests/assets/wp5_rnaseq/filtered_rnaseq_samples_list_20201104.csv',
                index_col=False,
                delimiter='\t',
                encoding='utf-8',
                float_precision='round_trip'
            )


class TestLinker(TestLinkage):
    def setUp(self):
        fake_db = Mock()

        self.linkage = data_sources.all_tables['linkage_sample_to_patient'](fake_db)
        self.sample_linkage = data_sources.all_tables['linkage_sample_to_sample'](fake_db)
        self.lims = data_sources.all_tables['lims_data'](fake_db)

        self.linker = utils.Linker(
            self.linkage.output_clean_data(),
            self.sample_linkage.output_clean_data()
        )

    def test_data(self):
        self.assertDataFrameEqual(
            self.linker.samples_to_patients,
            pandas.DataFrame.from_records(
                (
                    ('CCP0001', 'ABCDE-0001'),
                    ('CCP0002', 'ABCDE-0001'),
                    ('CCP0004', 'ABCDE-0003'),
                    ('CCP0005', 'ABCDE-0001'),
                    ('CVRI0003', 'ABCDE-0002')
                ),
                columns=('sample_id', 'canonical_isaric_id')
            )
        )

        self.assertDataFrameEqual(
            self.linker.samples_to_samples,
            pandas.DataFrame.from_records(
                (
                    ('CCP0001-R', 'CCP0001'),
                    ('10394162-R', '10394162')
                ),
                columns=('sample_id', 'isaric_sample_id')
            )
        )

    def test_register_lims_data(self):
        self.lims.register_linker(self.linker)
        self.linker.register_lims_data(self.lims.output_clean_data())
        self.assertDataFrameEqual(
            self.linker.lims_patient_timepoint_to_kits,
            pandas.DataFrame.from_records(
                (
                    ('CCP0002', 'ABCDE-0001', 'Day 3'),
                    ('CVRI0003', 'ABCDE-0002', 'Day 1'),
                    ('CCP0004', 'ABCDE-0003', 'Day 1')
                    # sorted by patient/timepoint. ABCDE-0001/Day 1 maps to both CCP0001 and CCP0005, so makes no call
                ),
                columns=('kit_id', 'patient_id', 'timepoint')
            )
        )

        df = self.linker.lims_barcodes_to_kits
        self.assertDataFrameEqual(
            df[df['barcode'].str.endswith('10032490')].reset_index(drop=True),
            pandas.DataFrame(
                {
                    'barcode': ['010032490', '10032490', 'SMG010032490'],
                    'clean_kit_id': ['CVRI0003', 'CVRI0003', 'CVRI0003']
                }
            )
        )
        self.assertDataFrameEqual(
            self.linker.samples_to_patients,
            pandas.DataFrame.from_records(
                (
                    ('CCP0001', 'ABCDE-0001'),
                    ('CCP0002', 'ABCDE-0001'),
                    ('CCP0004', 'ABCDE-0003'),
                    ('CCP0005', 'ABCDE-0001'),
                    ('CVRI0003', 'ABCDE-0002')
                ),
                columns=('sample_id', 'canonical_isaric_id')
            )
        )
        self.assertDataFrameEqual(
            self.linker.lims_kits,
            pandas.DataFrame.from_records(
                (
                    ('CCP0001', 'ABCDE-0001', 'Day 1'),
                    ('CCP0002', 'ABCDE-0001', 'Day 3'),
                    ('CCP0004', 'ABCDE-0003', 'Day 1'),
                    ('CCP0005', 'ABCDE-0001', 'Day 1'),
                    ('CVRI0003', 'ABCDE-0002', 'Day 1')
                ),
                columns=('kit_id', 'patient_id', 'timepoint')
            )
        )
        self.assertDataFrameEqual(
            self.linker.lims_patient_timepoint_to_kits,
            pandas.DataFrame.from_records(
                (
                    ('CCP0002', 'ABCDE-0001', 'Day 3'),
                    ('CVRI0003', 'ABCDE-0002', 'Day 1'),
                    ('CCP0004', 'ABCDE-0003', 'Day 1')
                ),
                columns=('kit_id', 'patient_id', 'timepoint')
            )
        )
