import pandas
from sqlalchemy import Column, Float, Integer
from unittest.mock import Mock, patch
from data_linkage import data_sources
from data_linkage.utils import Linker
from data_linkage.cleaning import SampleID, PatientID, Timepoint
from tests import TestLinkage


class TestPrimaryDataSource(TestLinkage):
    def setUp(self):
        with patch('data_linkage.data_sources.primary_data_sources.PrimaryDataSource.get_source', return_value='some_data.csv'):
            self.ds = data_sources.primary_data_sources.PrimaryDataSource(Mock())

        self.ds.columns = (
            Column('Sample ID', SampleID),
            Column('Clean Kit ID', SampleID),
            Column('Patient ID', PatientID),
            Column('Timepoint', Timepoint),
            Column('Column 1', Float),
            Column('Column 2', Integer)
        )
        self.ds.rename_columns['Column1'] = 'Column 1'
        self.ds.clean_kit_id_col = 'Clean Kit ID'
        self.ds.dirty_sample_id_col = 'Sample ID'

        # we need these because we're messing with columns after initialisation
        self.ds.patient_id_col = 'Patient ID'
        self.ds.timepoint_col = 'Timepoint'

        self.linker = Linker(
            pandas.DataFrame.from_records(
                (
                    ('CCP0001', 'ABCDE-0001'),
                    ('CCP0002', 'ABCDE-0002')
                ),
                columns=('sample_id', 'canonical_isaric_id')
            ),
            pandas.DataFrame.from_records(
                (
                    ('CCP0001-R', 'CCP0001'),
                ),
                columns=('sample_id', 'isaric_sample_id')
            )

        )

        self.linker.register_lims_data(
            pandas.DataFrame.from_records(
                (
                    ('SM2004001', 'ABCDE-0001', 'CCP0001', 'Day 1'),
                    ('SM2004002', 'ABCDE002', 'CCP0002', None),
                    ('SM2004003', 'ABCDE-0004', 'CCP1337', 'Day 3'),
                ),
                columns=('sample_code', 'patient_id', 'kit_id', 'timepoint')
            )
        )
        self.ds.register_linker(self.linker)

    def test_clean_dataframe(self):
        df = pandas.DataFrame.from_records(
            (
                ('sample1', '1', '2'),
                ('sample2', 2.5, 3),
            ),
            columns=('Sample ID', 'Column1 ', ' Column 2 ')
        )

        exp = pandas.DataFrame.from_records(
            (
                ('sample1', 1.0, 2),
                ('sample2', 2.5, 3),
            ),
            columns=('Sample ID', 'Column 1', 'Column 2')
        )
        exp['Column 2'] = exp['Column 2'].astype('Int64')  # otherwise dtype doesn't match cleaning.clean_int

        self.assertDataFrameEqual(self.ds.clean_dataframe(df), exp)

    def test_drop_unrelated_columns(self):
        df = pandas.DataFrame.from_records(
            (
                ('ABCDE-0001', 'Day 1', 'sample1', 'CCP0001', 1.0, 2, 'some extra column'),
                ('ABCDE-0001', 'Day 3', 'sample2', 'CCP0002', 2.5, 3, 'some extra column'),
                (None, None, None, None, 1.0, 1, 'some control measurement with no ID')
            ),
            columns=('Patient ID', 'Timepoint', 'Sample ID', 'Clean Kit ID', 'Column 1', 'Column 2', 'Column 3')
        )

        exp = pandas.DataFrame.from_records(
            (
                ('sample1', 'CCP0001', 'ABCDE-0001', 'Day 1', 1.0, 2),
                ('sample2', 'CCP0002', 'ABCDE-0001', 'Day 3', 2.5, 3),
                (None, None, None, None, 1.0, 1)
            ),
            columns=('Sample ID', 'Clean Kit ID', 'Patient ID', 'Timepoint', 'Column 1', 'Column 2')
        )

        self.assertDataFrameEqual(self.ds.drop_unrelated_columns(df), exp)

    def test_join_dataframe_clean_existing_col(self):
        l = pandas.DataFrame.from_records(
            (
                ('CCP0001', None),  # null, should be cleaned
                ('CCP0002', 'ABCDE002'),  # invalid, should be cleaned
                ('CCP0003', 'ABCDE003')  # not in linkage
            ),
            columns=('kit_id', 'canonical_isaric_id')
            # cleaning col and col to clean will have the same name, resulting in _x and _y suffixes
        )

        exp = pandas.Series(('ABCDE-0001', 'ABCDE-0002', None))
        obs = self.ds._join_dataframe(
            l, self.linker.samples_to_patients, 'kit_id', 'sample_id', 'canonical_isaric_id'
        )
        self.assertSeriesEqual(obs, exp)

    def test_join_dataframe_add_new_col(self):
        l = pandas.DataFrame(data={'kit_id': ('CCP0001', 'CCP0003', 'CCP0004', 'CCP0002', 'CCP0005')})

        exp = pandas.Series(('ABCDE-0001', None, None, 'ABCDE-0002', None))
        obs = self.ds._join_dataframe(
            l, self.linker.samples_to_patients, 'kit_id', 'sample_id', 'canonical_isaric_id'
        )
        self.assertSeriesEqual(obs, exp)

    def test_multijoin(self):
        lims = pandas.DataFrame.from_records(
            (
                ('ABCDE-0001', 'CCP0001', 'Day 1'),
                ('ABCDE-0002', 'CCP0002', None),
                ('ABCDE-0004', 'CCP1337', 'Day 3'),
            ),
            columns=('patient_id', 'kit_id', 'timepoint')
        )

        l = pandas.DataFrame.from_records(
            (
                ('CCP0001', None, None),
                ('CCP0004', 'ABCDE-0004', 'Day 3'),  # dodgy mapping not in linkage table
                (None, 'ABCDE-0002', 'Day 1'),
            ),
            columns=('kit_id', 'patient_id', 'timepoint')
        )

        # CCP0001 and CCP0002 have missing timepoints in the data/LIMS, so return nothing
        # CCP1337 represents a case where CCP0004 isn't in the LIMS yet and the mapping's wrong for whatever reason
        # using _merge_joined_data with conservative=True avoids overwriting the kit ID
        exp = pandas.Series((None, 'CCP1337', None))
        obs = self.ds._join_dataframe(
            l, lims, ['patient_id', 'timepoint'], ['patient_id', 'timepoint'], 'kit_id'
        )
        self.assertSeriesEqual(obs, exp)

    def test_merge_joined_data(self):
        left = pandas.Series((None, 'that', 'other'))
        right = pandas.Series(('this', 'more', None))
        self.ds._merge_joined_data(left, right)

        # 'that' is overwritten
        exp = pandas.Series(('this', 'more', 'other'))
        self.assertSeriesEqual(left, exp)

    def test_merge_joined_data_conservative(self):
        left = pandas.Series((None, 'that', 'other'))
        right = pandas.Series(('this', 'more', None))
        self.ds._merge_joined_data(left, right, conservative=True)

        # 'that' not overwritten
        exp = pandas.Series(('this', 'that', 'other'))
        self.assertSeriesEqual(left, exp)

    @patch('data_linkage.data_sources.primary_data_sources.PrimaryDataSource.init_dataframe')
    def test_clean_linkages(self, patched_init_df):
        df = pandas.DataFrame.from_records(
            (
                ('SM2004001', 'ABCDE-0001', 'Day 1', '1.54',    '2'),  # fully linked
                ('SM2004002', None,          None,   '2.3',     '2'),  # barcode only
                ('CCP0001-R', None,          None,   '2',       '2'),  # kit only, needs cleaned
                ('CCP0002',   None,          None,   '1.5',     '2'),  # kit only
                (None,        'ABCDE-0001', 'Day 1', '1.40',    '2'),  # patient/timepoint only
                (None,        'ABCDE-0001', None,    '1.54001', '2'),  # patient only - unlinkable
            ),
            columns=('Sample ID', 'Patient ID', 'Timepoint', 'Column1', 'Column 2')
        )
        exp = pandas.DataFrame.from_records(
            (
                ('SM2004001', 'CCP0001', 'ABCDE-0001', 'Day 1', 1.54,    2),
                ('SM2004002', 'CCP0002', 'ABCDE-0002', 'null',  2.3,     2),
                ('CCP0001-R', 'CCP0001', 'ABCDE-0001', 'Day 1', 2.0,     2),
                ('CCP0002',   'CCP0002', 'ABCDE-0002', 'null',  1.50,    2),
                ('null',      'CCP0001', 'ABCDE-0001', 'Day 1', 1.4,     2),
                ('null',      'null',    'ABCDE-0001', 'null',  1.54001, 2),
            ),
            columns=('Sample ID', 'Clean Kit ID', 'Patient ID', 'Timepoint', 'Column 1', 'Column 2')
        )
        exp['Column 2'] = exp['Column 2'].astype('Int64')
        patched_init_df.return_value = df
        self.assertDataFrameEqual(self.ds.output_clean_data(), exp)
