import re
import pandas
from numpy import NaN
from io import StringIO
from data_linkage import cleaning
from data_linkage.data_sources.primary_data_sources.clinical import ClinicalOneline, ClinicalData
from tests import TestLinkage


def to_dataframe(test_case):
    """Prepare a Pandas DataFrame from the test cases below"""
    csv_pattern = re.compile(r' {2,}')
    df_str = ''
    for line in test_case.strip().split('\n'):
        df_str += csv_pattern.sub(',', line.strip()) + '\n'

    df = pandas.read_csv(StringIO(df_str))

    cleaning_funcs = {
        'daily_fio2_combined': cleaning.clean_float,
        'severity_score': cleaning.clean_int,
        'dsstdtc': cleaning.clean_date,
        'hostdat': cleaning.clean_date,
        'cestdat': cleaning.clean_date,
        'any_invasive': cleaning.clean_boolean,
        'any_noninvasive': cleaning.clean_boolean,
        'any_oxygen': cleaning.clean_boolean,
        'any_daily_invasive_prtrt': cleaning.clean_boolean,
        'any_daily_noninvasive_prtrt': cleaning.clean_boolean,
        'daily_invasive_prtrt': cleaning.clean_boolean,
        'daily_noninvasive_prtrt': cleaning.clean_boolean
    }

    for col in df.columns:
        cleaning_func = cleaning_funcs.get(col)
        if cleaning_func:
            df[col] = cleaning_func(df[col])

    return df


class TestSeverityScoring(TestLinkage):
    """Test for overall severity scoring on the clinical_oneline table"""

    """The severity function being tested"""
    severity_func = ClinicalOneline.severity_score

    """
    Test data and expected outputs. The severity_score and severity_indication columns are the expected outputs of
    running the algorithm on the first 10 columns.
    """
    test_case = '''
    dsterm                dsstdtc     hostdat     cestdat     any_invasive  any_noninvasive  any_oxygen  oxy_vsorresu       any_daily_invasive_prtrt  any_daily_noninvasive_prtrt  severity_score  severity_indication
    Unknown               NA          NA          NA          NA            NA               NA          NA                 NA                        NA                           NA              NA
    NA                    NA          NA          NA          NA            NA               NA          NA                 NA                        NA                           NA              NA
    Palliative discharge  2021-08-31  2021-08-26  2021-08-25  NA            NA               NA          NA                 NA                        NA                           5               Death/palliative discharge within 28 days of admission
    Palliative discharge  2021-08-31  2021-08-01  2021-08-15  NA            NA               NA          NA                 NA                        NA                           5               Death/palliative discharge within 28 days of nosocomial infection
    Palliative discharge  2021-08-31  2021-08-01  2021-08-02  Yes           NA               NA          NA                 NA                        NA                           4               Invasive mechanical ventilation
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  Yes           NA               NA          NA                 NA                        NA                           4               Invasive mechanical ventilation
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  No            Yes              NA          NA                 NA                        NA                           3               Noninvasive ventilation
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            Yes              NA          NA                 NA                        NA                           3               Noninvasive ventilation
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            No               Yes         NA                 NA                        NA                           2               Oxygen therapy
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            No               No          Oxygen therapy     NA                        NA                           2               Oxygen therapy
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            No               No          NA                 NA                        NA                           1               No oxygen requirement
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            No               No          Room air           NA                        NA                           1               No oxygen requirement
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            No               No          Some weird status  No                        No                           1               No oxygen requirement
    Discharged alive      2021-08-31  2021-08-30  2021-08-29  NA            No               No          Some weird status  NA                        NA                           NA              NA
    '''

    def test_severity(self):
        input_df = to_dataframe(self.test_case)
        obs = input_df.apply(
            lambda row: pandas.Series(self.severity_func(row), index=('severity_score', 'severity_indication')),
            axis='columns'
        )
        obs['severity_score'] = cleaning.clean_int(obs['severity_score'])
        obs['severity_indication'] = obs['severity_indication'].fillna(NaN)
        self.assertTrue(obs.equals(input_df[['severity_score', 'severity_indication']]))


class TestDailySeverityScoring(TestSeverityScoring):
    """As above for TestSeverityScoring, but for the daily measurements in clinical_data"""

    severity_func = ClinicalData.severity_score
    test_case = '''
        dsterm                daily_invasive_prtrt  daily_noninvasive_prtrt  daily_fio2_combined  oxy_vsorresu    severity_score  severity_indication
        Palliative discharge  No                    No                       NA                   NA              5               Death/palliative discharge
        NA                    Yes                   No                       0.28                 NA              4               Invasive mechanical ventilation
        NA                    No                    Yes                      NA                   NA              3               Noninvasive ventilation
        NA                    NA                    Yes                      NA                   NA              3               Noninvasive ventilation
        NA                    No                    No                       0.28                 NA              2               Oxygen therapy
        NA                    No                    No                       NA                   Oxygen therapy  2               Oxygen therapy
        NA                    No                    No                       0.21000000004        Room air        1               No oxygen requirement
        NA                    NA                    NA                       NA                   NA              1               No oxygen requirement
        NA                    NA                    NA                       0.21                 Unknown         NA              NA
    '''
