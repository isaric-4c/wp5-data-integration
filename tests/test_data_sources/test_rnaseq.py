import pandas
from os import makedirs
from unittest.mock import Mock
from data_linkage.data_sources.primary_data_sources import WP5RNASeqAnalysis
from tests import TestLinkage


class TestWP5RNASeqGeneCountsPipelineReader(TestLinkage):
    def setUp(self):
        self.r = WP5RNASeqAnalysis(Mock())
        makedirs(f'{self.r.rnaseq_dir}/analysis/rnaseq/archive', exist_ok=True)  # empty folder, should be ignorable

    def test_analyses_on_ultra(self):
        self.assertListEqual(
            ['20200903_NS550', '20200921_NS2000'],
            self.r.rnaseq_processing_runs()
        )

    def test_source(self):
        self.assertEqual('20200903_NS550,20200921_NS2000', self.r.source)

    def test_init_dataframe(self):
        obs = self.r.init_dataframe()
        exp = pandas.DataFrame.from_records(
            [
                [
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200903_NS550/salmon/CCP0001_salmon_gene_counts.csv',
                    '20200903_NS550',
                    '20200903',
                    'NS550',
                    'CCP0001',
                    'CCP0001',
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200903_NS550/MultiQC/multiqc_data/multiqc_salmon.txt'
                ],
                [
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200903_NS550/salmon/CCP0002_salmon_gene_counts.csv',
                    '20200903_NS550',
                    '20200903',
                    'NS550',
                    'CCP0002',
                    'CCP0002',
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200903_NS550/MultiQC/multiqc_data/multiqc_salmon.txt'
                ],
                [
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200921_NS2000/salmon/10394162-R_NS2000_75_salmon_gene_counts.csv',
                    '20200921_NS2000',
                    '20200921',
                    'NS2000',
                    '10394162-R_NS2000_75',
                    '10394162-R',
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200921_NS2000/MultiQC/multiqc_data/multiqc_salmon.txt'
                ],
                [
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200921_NS2000/salmon/CCP0001-R_NS2000_75_salmon_gene_counts.csv',
                    '20200921_NS2000',
                    '20200921',
                    'NS2000',
                    'CCP0001-R_NS2000_75',
                    'CCP0001-R',
                    'tests/assets/wp5_rnaseq/analysis/rnaseq/20200921_NS2000/MultiQC/multiqc_data/multiqc_salmon.txt'
                ]
            ],
            columns=('gene_count_file', 'run_id', 'run_date', 'sequencer_type', 'rnaseq_sample_id', 'clean_rnaseq_sample_id', 'multiqc_salmon_file')
        )
        self.assertDataFrameEqual(obs, exp)
