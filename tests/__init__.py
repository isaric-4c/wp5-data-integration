import os
import pandas
import unittest
from unittest.mock import Mock
from tests import postgres_db
from data_linkage.config import cfg

top_level = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
assets = os.path.join(top_level, 'tests', 'assets')

if os.getenv('CI') == 'true':
    cfg.content['postgres']['server'] = 'isaric_db'
    postgres_environment = 'ci'

elif postgres_db.docker_available() and os.getenv('NO_PGBUILD') != 'true':
    cfg.content['postgres']['server'] = 'localhost'
    os.environ['PGPASSFILE'] = os.path.join(assets, 'pgpass')
    postgres_environment = 'local_persistent' if postgres_db.postgres_container_is_running() else 'local_transient'

else:
    postgres_environment = None


class TestLinkage(unittest.TestCase):
    def assertSeriesEqual(self, s1, s2):
        for s in (s1, s2):
            if not isinstance(s, pandas.Series):
                raise TypeError(f'Argument {s} is not of type pandas.Series')

        return self.assertTrue(s1.equals(s2), f'Series not equal:\nFirst:\n{s1}\nSecond:\n{s2}')

    def assertDataFrameEqual(self, df1, df2):
        for df in (df1, df2):
            if not isinstance(df, pandas.DataFrame):
                raise TypeError(f'Argument {df} is not of type pandas.DataFrame')

        _df1 = df1.fillna('null')
        _df2 = df2.fillna('null')

        return self.assertTrue(
            _df1.equals(_df2),
            f'Data frames not equal:\nFirst:\n{_df1}\nSecond:\n{_df2}'
        )


class NamedMock(Mock):
    @property
    def name(self):
        return self._name
