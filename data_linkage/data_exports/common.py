import os
import pandas
from data_linkage import utils, __now__
from data_linkage.db_engine.engine import SQLiteDatabase
from data_linkage.config import cfg


class Export:
    """
    Base export class. Called in ReleaseManager after database insertion has occurred. If configured to export SQLite, a
    a new SQLite database will be created with schema for relevant tables and views, and relevant primary/secondary
    tables will be populated via pandas.DataFrame.to_sql.
    """

    def __init__(self, name, db):
        self.name = name
        self.db = db
        self.config = cfg['exports'][name]
        self.logger = utils.setup_logger(self.name)

        for section in ('primary_data_sources', 'views', 'secondary_data_sources'):
            if section not in self.config:
                continue

            for k, v in self.config[section].items():
                if v:
                    self.config[section][k] = v.format(now=__now__)

        index = self.db.all_tables['index']
        with self.db.connect() as c:
            index_data = dict(
                c.execute(
                    index.table.select(index.table.c.name.in_(('automation_version', 'release_name', 'release_number')))
                ).fetchall()
            )

        self.release_name = index_data['release_name']
        self.release_number = index_data['release_number']
        self.export_dir = self.config['location'].format(release_name=self.release_name)
        os.makedirs(self.export_dir, exist_ok=True)

        self.sqlite_db = None

    def run(self):
        """
        Main behaviour of the export. This may include writing tables out to SQLite, CSVs, writing docs from templates,
        and packaging/compression.
        """

        self.logger.info('Running export %s for release %s', self.name, self.release_name)
        sqlite_cfg = self.config.get('sqlite', True)
        if sqlite_cfg:
            sqlite_export = os.path.join(self.export_dir, 'db.sqlite')
            self.sqlite_db = SQLiteDatabase(
                tuple(self.config.get('primary_data_sources', {})),
                tuple(self.config.get('views', {})),
                tuple(self.config.get('secondary_data_sources', {})),
                sqlite_export
            )

            self.sqlite_db.init_tables()

        with self.db.connect() as source_connection:
            self.export_tables(source_connection)

        self.logger.info('Writing documentation')
        utils.write_document(
            utils.render_template('Readme.md', release=self.release_name),
            os.path.join(self.export_dir, 'Readme.md')
        )
        utils.write_document(utils.render_template('linkage.md'), os.path.join(self.export_dir, 'linkage.md'))

        utils.write_document(
            utils.render_template(
                'data_map.md',
                now=__now__,
                primary_data_sources=[self.db.all_tables[ds] for ds in sorted(self.config['primary_data_sources'])],
                views=[self.db.all_tables[v] for v in sorted(self.config['views'])],
                secondary_data_sources=[self.db.all_tables[ds] for ds in sorted(self.config['secondary_data_sources'])],
                export_csvs=self.csvs_to_export()
            ),
            os.path.join(self.export_dir, 'data_map.md')
        )

        group_access = self.config.get('group_access')
        if group_access:
            self.logger.info('Setting permissions for %s', self.export_dir)
            utils.set_directory_permissions(
                self.export_dir,
                group_access,
                utils.permissions_urwx_grx,
                utils.permissions_urw_gr
            )

    def export_tables(self, connection):
        """Perform any part of the export process that requires access to the database connection"""
        for ds_name, export_csv in tuple(self.config['primary_data_sources'].items()) + tuple(self.config['secondary_data_sources'].items()):
            ds = self.db.all_tables[ds_name]
            df = self.read_table_for_export(connection, ds.table.select())
            self.export_to_sqlite(df, ds_name)

            if export_csv:
                self.export_to_csv(df, os.path.join(self.export_dir, export_csv.format(now=__now__)))

        for view_name, export_csv in self.config['views'].items():
            v = self.db.views[view_name]
            if export_csv:
                df = self.read_table_for_export(connection, v.table.select())
                self.export_to_csv(df, os.path.join(self.export_dir, export_csv.format(now=__now__)))

    @staticmethod
    def read_table_for_export(connection, query):
        """
        Read a table from the main database into a Pandas dataframe
        :param sqlalchemy.engine.Connection connection:
        :param sqlalchemy.sql.selectable.Select query:
        """
        return pandas.read_sql(query, connection)

    @staticmethod
    def export_to_csv(df, filename):
        """
        Export a dataframe to CSV
        :param pandas.DataFrame df:
        :param str filename:
        """
        df.to_csv(filename, index=False)

    def export_to_sqlite(self, df, table_name):
        """
        Export a dataframe to the given SQLAlchemy connection. Assumes the Export has been created with `sqlite=True`
        :param pandas.DataFrame df:
        :param str table_name:
        """

        with self.sqlite_db.connect() as dest_connection:
            df.to_sql(table_name, dest_connection, if_exists='append', index=False)

    def csvs_to_export(self):
        """All configured CSV names for the export"""
        all_datasets = tuple(self.config['primary_data_sources'].items()) + \
                       tuple(self.config['views'].items()) + \
                       tuple(self.config['secondary_data_sources'].items())

        return {k: v for k, v in all_datasets if v}
