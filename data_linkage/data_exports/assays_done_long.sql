/*
assays_done_long.sql

Sample status report per patient, in long form. Data comes from:

- sample_assays - an enumeration of everything that can be done on a sample in ISARIC, e.g. RNA-seq, IDAMAC requests, biochem assays
- LIMS sample data
- LIMS samples-dispatched reports
- Assay data from the rest of the data linkage

This then gets joined together:
- we combine sample_assays and the LIMS data into all possible combinations of sample_assays per sample
- we query all samples in the table lims_samples_dispatched
- we take a union of all samples in all assays
- these three datasets are joined by kit ID, and a status is derived:
  - assay data available -> 'complete'
  - sample appears in lims_samples_dispatched -> 'dispatched'
  - else -> 'not_started'
- other information is also joined in, including clinical site and dates of onset, admission, recruitment and sample collection

Example:

patient_id  kit_id     timepoint  assay_name        status       clinical_site cestdat     hostdat     dsstdat     date_collected
ABCDE-0001  CCP01234   Day 1      wp4_biochemistry  dispatched   A Site        2021-01-10  2021-01-11  2021-01-12  2021-01-13
ABCDE-0001  CCP01234   Day 1      wp5_rnaseq        complete     A Site        2021-01-10  2021-01-11  2021-01-12  2021-01-13
ABCDE-0001  CCP01235   Day 3      wp4_biochemistry  not_started  A Site        2021-01-10  2021-01-11  2021-01-12  2021-01-13
ABCDE-0001  CCP01235   Day 3      wp5_rnaseq        dispatched   A Site        2021-01-10  2021-01-11  2021-01-12  2021-01-13
*/

-- enumeration of what could be done on each sample, including assays, IDAMAC requests and other dispatch destinations
WITH sample_assays(name) as (
    SELECT * FROM (
        VALUES
            ('idamac_request_stool'),
            ('idamac_request_urine_metabolomics'),
            ('wp3_viralseq'),
            ('wp3_viralseq_eta_16s'),
            ('wp3_viralseq_urt_16s'),
            ('wp4_biochemistry'),
            ('wp4_biochemistry_vitd_steroids'),
            ('wp5_rnaseq'),
            ('wp5_wgs'),
            ('wp6_mucosal_cytokines'),
            ('wp6_ngs_serology'),
            ('wp6_plasma_cytokines'),
            ('wp6_serology_serum'),
            ('wp6_serology_oral_fluids'),
            ('wp6_serum_antibody')
    )
)

SELECT DISTINCT
    view_lims_data.canonical_isaric_id AS patient_id,
    view_lims_data.'Kit_ID' AS kit_id,
    view_lims_data.'Timepoint' AS timepoint,
    sample_assays.name AS assay_name,
    CASE
        WHEN assays_done.assay_name IS NOT NULL THEN 'complete'
        WHEN lims_samples_dispatched.assay IS NOT NULL THEN 'dispatched'
        ELSE 'not_started'
    END AS status,
    clinical_topline.sitename AS clinical_site,
    clinical_topline.cestdat AS cestdat, -- onset
    clinical_topline.hostdat AS hostdat, -- admission
    clinical_topline.dsstdat AS dsstdat, -- recruitment
    view_lims_data.'Date_Collected' AS date_collected  -- Todo: figure out duplicate lines for kits with multiple dates

FROM view_lims_data, sample_assays  -- all possible combinations

LEFT OUTER JOIN clinical_topline ON view_lims_data.canonical_isaric_id = clinical_topline.subjid

-- what sample assays have been dispatched
-- we join by kit ID and assay because we don't want to multiply assays per barcode
LEFT OUTER JOIN lims_samples_dispatched
    ON view_lims_data.'Kit_ID' = lims_samples_dispatched.'Kit ID'
    AND sample_assays.name = lims_samples_dispatched.assay

-- what sample assays we have data for
LEFT OUTER JOIN (
    SELECT DISTINCT
        kit_id,
        assay_name
    FROM (
        SELECT view_wp4_biochemistry.'Kit ID' AS kit_id, 'wp4_biochemistry' AS assay_name FROM view_wp4_biochemistry
        UNION
        SELECT view_wp5_rnaseq_integration.lims_kit_id AS kit_id, 'wp5_rnaseq' AS assay_name FROM view_wp5_rnaseq_integration
        UNION
        SELECT view_wp6_plasma_cytokines.kit_id AS kit_id, 'wp6_plasma_cytokines' AS assay_name FROM view_wp6_plasma_cytokines
        UNION
        SELECT view_wp6_serology_oral_fluids.'Kit_ID' AS kit_id, 'wp6_serology_oral_fluids' AS assay_name FROM view_wp6_serology_oral_fluids
        UNION
        SELECT view_wp6_serology_serum.'Kit_ID' AS kit_id, 'wp6_serology_serum' AS assay_name FROM view_wp6_serology_serum
    )
) AS assays_done ON view_lims_data.'Kit_ID' = assays_done.kit_id AND assays_done.assay_name = sample_assays.name

ORDER BY view_lims_data.canonical_isaric_id, view_lims_data.'Kit_ID', sample_assays.name
