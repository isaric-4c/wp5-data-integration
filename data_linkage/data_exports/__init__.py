from data_linkage.config import cfg
from .isaric_research_environment import MainExport
from .nhs_safe_haven import SafeHavenExport
from .common import Export


def all_exports(db):
    exports = set()

    for export_name in cfg.query('exports', ret_default={}):
        if export_name == 'main':
            cls = MainExport
        elif export_name == 'nhs_safe_haven':
            cls = SafeHavenExport
        else:
            cls = Export

        exports.add(cls(export_name, db))

    return exports
