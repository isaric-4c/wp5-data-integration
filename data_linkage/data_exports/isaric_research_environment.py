"""
This module contains queries to build alternate views of the data. Linkages are made according to predefined joins per
dataset, where defined. The results are exported to CSV in the release dir.
"""

import os
import csv
import datetime
import sqlalchemy
from data_linkage import __now__
from data_linkage.data_exports.common import Export

today = datetime.date.today()


class MainExport(Export):
    """
    Main data export to the release folder. Everything is exported to SQLite, and everything in self.export_csvs is
    output to CSV.
    """

    """
    Dict of datasets to write to CSV, where the key is the dataset name and the value is the basename of the created
    file (files will be written as `{basename}_{__now__}.csv`).
    """
    no_csv_export = {
        # already clean
        'clinical_data',
        'clinical_topline',
        'clinical_oneline',
        'wp5_clinical_imputation'
        
        # not relevant to users
        'sample_assays'
    }

    def __init__(self, name, db):
        super().__init__(name, db)

        for section, collection in (
            ('primary_data_sources', self.db.primary_data_sources),
            ('views', self.db.views),
            ('secondary_data_sources', self.db.secondary_data_sources)
        ):
            self.config[section] = {}
            for name in collection:
                if name in self.no_csv_export:
                    csv_export = None
                else:
                    csv_export = f'{name}_{__now__}.csv'

                self.config[section][name] = csv_export

    def export_tables(self, connection):
        super().export_tables(connection)

        with open(os.path.join(self.export_dir, f'unlinked_data_{__now__}.log'), 'w', newline='') as f:
            writer = csv.writer(f)
            for line in self.unlinked_data(connection):
                writer.writerow(line)

    def unlinked_data(self, connection):
        """
        Report on what kits/patients can't currently be linked to the LIMS and clinical data.
        """
        queries = []

        tables = {
            k: v for k, v in self.db.primary_data_sources.items()
            if k not in ('clinical_topline', 'clinical_data')  # no point in checking these since we're checking oneline
        }
        clinical = tables.pop('clinical_oneline').table
        lims = tables.pop('lims_data').table

        for data_source in tables.values():
            table = data_source.table

            if data_source.patient_id_col:
                queries.append(
                    (
                        sqlalchemy.select(
                            [table],
                            from_obj=table.outerjoin(clinical, table.c[data_source.patient_id_col] == clinical.c.subjid),
                            whereclause=clinical.c.subjid == None
                        ).distinct(),
                        data_source.name,
                        'clinical'
                    )
                )

            if data_source.clean_kit_id_col:
                queries.append(
                    (
                        sqlalchemy.select(
                            [table],
                            from_obj=table.outerjoin(
                                lims,
                                table.c[data_source.clean_kit_id_col] == lims.c.kit_id
                            ),
                            whereclause=lims.c.kit_id == None
                        ).distinct(),
                        data_source.name,
                        'LIMS'
                    )
                )

        # lims
        queries.append(
            (
                sqlalchemy.select(
                    [lims],
                    from_obj=lims.outerjoin(clinical, lims.c.patient_id == clinical.c['subjid']),
                    whereclause=clinical.c['subjid'] == None
                ).distinct(),
                'LIMS',
                'clinical'
            )
        )

        for query, table_name, linking_to in queries:
            nresults = connection.execute(query.with_only_columns([sqlalchemy.func.count()])).scalar()
            yield [f'Found {nresults} lines not linkable to {linking_to} for {table_name}']
            if nresults:
                results = connection.execute(query)
                yield results.keys()
                for line in results:
                    yield line

            yield []
