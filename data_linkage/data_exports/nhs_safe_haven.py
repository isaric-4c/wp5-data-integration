import os
import bz2
import json
import hashlib
import tarfile
import sqlalchemy
from data_linkage import __today__
from data_linkage.data_exports.common import Export
from data_linkage.cleaning import SampleID
from data_linkage.utils import write_document, render_template


class SafeHavenExport(Export):
    """
    Data export to the ISARIC NHS Safe Haven. Exports all lab datasets to CSV, without sample IDs but with the release
    version number. CSVs are packaged into Tar archives along with a JSON metadata file and SQL schema declaration.
    """

    """Set of primary data sources that this export should have"""
    export_csvs = {
        'wp3_blood_viral_load',
        'wp4_biochemistry',
        'wp5_clinical_imputation',
        'wp5_rnaseq_crf_report',
        'wp5_rnaseq_analysis',
        'wp5_rnaseq_viral_read_counts',
        'wp5_wgs',
        'wp6_anticytokineantibodies_iga',
        'wp6_anticytokineantibodies_igg',
        'wp6_faecal_cytokines',
        'wp6_nasosorption_mediators',
        'wp6_plasma_cytokines',
        'wp7_serology_neutralisation',
        'wp7_serology_oral_fluids',
        'wp7_serology_serum',
        'idamac_complement'
    }

    def run(self):
        """
        Main function. Select the patient, timepoint and all measurement columns for each assay, and output to CSV. Also
        output a Postgres-format schema for each table, minus sample IDs.
        """
        self.logger.info('Running NHS safe haven export for release %s', self.release_name)

        if self.db.engine.dialect.name != 'postgresql':
            # fake Postgres engine to write out the Postgres-dialect schemas
            pg_engine = sqlalchemy.create_engine('postgresql://test@localhost/test')
        else:
            pg_engine = self.db.engine

        with self.db.connect() as connection:
            for table_name in self.export_csvs:
                ds = self.db.primary_data_sources[table_name]
                csv_file = os.path.join(self.export_dir, f'isaric-{table_name}.csv')

                # select all columns that are not an ISARIC sample ID, and add release number
                cols = [sqlalchemy.literal(self.release_number).label('release_number')] + [
                    col for col in ds.table.c
                    if not isinstance(col.type, SampleID)
                ]

                if ds.timepoint_col:
                    where_clause = sqlalchemy.and_(
                        ds.table.c[ds.patient_id_col] != None,
                        ds.table.c[ds.timepoint_col] != None
                    )
                else:
                    where_clause = ds.table.c[ds.patient_id_col] != None

                query = sqlalchemy.select(cols, whereclause=where_clause)
                df = self.read_table_for_export(connection, query)
                self.export_to_csv(df, csv_file)

                compressed_csv = csv_file + f'-{__today__}.bz2'
                md5 = hashlib.md5()
                nlines = 0
                with open(csv_file) as i, bz2.open(compressed_csv, 'w') as o:
                    for line in i:
                        nlines += 1
                        line = line.encode()
                        md5.update(line)
                        o.write(line)

                nlines -= 1  # don't count the line number
                metadata_file = csv_file + f'-{__today__}.json'
                metadata = {
                    'check_sum': md5.hexdigest(),
                    'file_size': os.stat(csv_file).st_size,
                    'n_records': nlines,
                    'n_updates': 0,
                    'n_inserts': nlines,
                    'description': ds.description
                }

                with open(metadata_file, 'w') as f:
                    json.dump(metadata, f, indent=2)

                schema_file = csv_file + f'-{__today__}.sql'
                create_table = sqlalchemy.schema.CreateTable(ds.table)
                create_table.columns = [
                   sqlalchemy.schema.CreateColumn(sqlalchemy.Column('release_number', sqlalchemy.Integer))
                ] + [
                    create_col for create_col in create_table.columns
                    if not isinstance(create_col.element.type, SampleID)
                ]
                schema = str(create_table.compile(pg_engine))
                with open(schema_file, 'w') as f:
                    f.write(schema)

                archive_name = csv_file + f'-{__today__}.tar'
                with tarfile.TarFile.open(archive_name, 'w') as a:
                    for f in (compressed_csv, metadata_file, schema_file):
                        a.add(f, arcname=os.path.basename(f))

                for f in (compressed_csv, metadata_file, schema_file, csv_file):
                    os.remove(f)

        write_document(
            render_template(
                'nhs_export_readme.md',
                tables=[self.db.primary_data_sources[table_name] for table_name in self.export_csvs]
            ),
            os.path.join(self.export_dir, 'Readme.md')
        )
