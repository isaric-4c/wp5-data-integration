import os
import logging
import hashlib
import datetime
from sqlalchemy.exc import ProgrammingError, OperationalError
from data_linkage import utils, __version__, __today__
from data_linkage.data_exports import all_exports
from data_linkage.db_engine.engine import get_database
from data_linkage.config import cfg
from data_linkage.utils import Linker


class ReleaseManager:
    """
    Class to manage the creation of a new data release. A release consists of:
    - Reinitialising the database
    - Finding data for all defined data sources
    - Populating a Linker object from the LIMS and linkage tables in the right order
    - Loading each one up, cleaning it and inserting into the database
    - Running defined queries and CSV exports
    - Outputting documentation to the release and export folders
    """
    def __init__(self, dry_run=False, force=False, db=None):
        self.dry_run = dry_run
        self.force = force
        self.logger = utils.setup_logger(self.__class__.__name__)

        self.release_dir = os.path.abspath(cfg['data_release_path'])
        self.db = db or get_database()

    def list_datasets(self):
        print('ISARIC datasets')
        print('---------------')
        print('Primary data sources:')
        for d in sorted(self.db.primary_data_sources.values()):
            print(f'{d.name}: {d.source}')

        print('\nViews:')
        for v in sorted(self.db.views.values()):
            print(v.name)

        print('\nSecondary data sources:')
        for d in sorted(self.db.secondary_data_sources.values()):
            print(d.name)

    def should_run(self):
        """
        Decide whether a new release is needed. If there is no new data since the last release, no new version of the
        data linkage has been deployed and --force is not enabled, no release is run.
        """

        try:
            with self.db.connect() as c:
                index_data = {
                    line['name']: line['value']
                    for line in c.execute(self.db.all_tables['index'].table.select())
                }

        # new database, so index table doesn't exist yet
        except (FileNotFoundError, KeyError, OperationalError, ProgrammingError):
            index_data = {}

        should_run = False

        if index_data.get('automation_version') != __version__:
            self.logger.info('New automation version available')
            should_run = True

        for ds in self.db.primary_data_sources.values():
            if ds.source != index_data.get(ds.name):
                self.logger.info('New data found for %s: %s', ds.name, ds.source)
                should_run = True

        if self.force:
            self.logger.info('--force enabled - running release')
            should_run = True

        if self.dry_run:
            should_run = False

        return should_run

    def full_release(self):
        self.build_database()
        self.run_exports()

    def build_database(self):
        new_release_name, new_version_number = self._get_new_release_name()
        new_release_dir = os.path.join(self.release_dir, new_release_name)
        log_dir = os.path.join(new_release_dir, 'log')
        self.logger.info('Releasing new data at %s', new_release_dir)

        os.makedirs(log_dir)
        handler = logging.FileHandler(os.path.join(log_dir, 'data_release.log'))
        utils.setup_handler(handler)

        self.logger.info('Building current database')
        self.db.drop()
        self.db.init_tables()

        primary_sources = self.db.primary_data_sources.copy()  # copies the dict, not the instances contained

        # we need to load the linkage tables first...
        sample_patient_linkage = primary_sources.pop('linkage_sample_to_patient')
        sample_patient_linkage_df = self.load_sql_table(sample_patient_linkage, log_dir)
        sample_sample_linkage = primary_sources.pop('linkage_sample_to_sample')
        sample_sample_linkage_df = self.load_sql_table(sample_sample_linkage, log_dir)

        # ... then initialise the linker with them...
        linker = Linker(sample_patient_linkage_df, sample_sample_linkage_df)

        # ... then clean the LIMS data and register that into the linker...
        lims = primary_sources.pop('lims_data')
        lims.register_linker(linker)
        lims_df = self.load_sql_table(lims, log_dir)
        linker.register_lims_data(lims_df)

        for ds in primary_sources.values():
            # ... that we can then use to clean the rest of the datasets
            ds.register_linker(linker)

        primary_sources['index'].add_version_info(new_release_name, new_version_number)

        for ds in list(primary_sources.values()) + list(self.db.secondary_data_sources.values()):
            self.load_sql_table(ds, log_dir)

        # self.release_new_document_version('Readme.md', cfg['top_level_readme_dir'], 'Readme')

        latest_release = os.path.join(self.release_dir, 'latest')
        if os.path.islink(latest_release):
            os.remove(latest_release)

        os.symlink(new_release_name, latest_release)

        # snapshot the new database
        self.db.backup(new_release_dir)

    def run_exports(self):
        exports = all_exports(self.db)
        errors = [
            e.export_dir for e in exports
            if os.path.isfile(os.path.join(e.export_dir, 'db.sqlite'))
        ]

        if any(errors):
            self.logger.error('Exports already exist at %s', ', '.join(errors))
            return

        for e in exports:
            e.run()

    def release_new_document_version(self, doc_template, release_dir, released_doc_basename, docs_folder='docs'):
        """
        Publish a new datestamped document in release_dir/docs and link to the latest one at
        release_dir/released_doc_basename-__today__.md.
        :param str doc_template: The template in docs/ to publish
        :param str release_dir: The directory containing the docs folder and latest doc link.
        :param str released_doc_basename: What to publish the document as, sans datestamp and file extension
        :param docs_folder: Alternative name for the docs folder
        """

        content = utils.render_template(doc_template)
        new_md5 = hashlib.md5()
        new_md5.update(content.encode())

        current_link = os.path.join(release_dir, released_doc_basename + '.md')
        current_md5 = hashlib.md5()
        if os.path.islink(current_link):
            with open(os.path.join(release_dir, os.readlink(current_link))) as f:
                current_md5.update(f.read().encode())

        if new_md5.hexdigest() != current_md5.hexdigest():
            new_doc = f'Readme_{__today__}.csv'
            self.logger.info('Publishing new document at %s', new_doc)
            os.makedirs(os.path.join(release_dir, docs_folder), exist_ok=True)

            utils.write_document(content, os.path.join(release_dir, docs_folder, new_doc))
            if os.path.islink(current_link):  # not releasing for the first time
                os.remove(current_link)

            os.symlink(os.path.join(docs_folder, new_doc), current_link)

    def load_sql_table(self, data_source, log_dir):
        """Set up logging and import a data source into SQL."""

        log_file = os.path.join(log_dir, data_source.name + '.log')
        handler = logging.FileHandler(log_file)
        utils.setup_handler(handler)

        df = data_source.load_data()
        utils.remove_handler(handler)
        return df

    def _existing_releases(self):
        try:
            return sorted(
                os.path.basename(d)
                for d in utils.discover_files(f'{self.release_dir}/????????_v*')
                if os.path.isdir(d)
            )
        except FileNotFoundError:
            return []

    def _get_new_release_name(self):
        existing_releases = self._existing_releases()
        now = datetime.datetime.now()
        today = now.date().strftime('%Y%m%d')

        new_version_number = len(existing_releases) + 1
        return f'{today}_v{new_version_number}', new_version_number
