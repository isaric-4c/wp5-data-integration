import os
import csv
import sys
import glob
import stat
import shutil
import logging
import pandas
import jinja2
import sqlalchemy
from data_linkage import __version__
from data_linkage.exceptions import InvalidDataException

"""u=rw-,g=r--,o=---"""
permissions_urw_gr = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP

"""u=rw-,g=rw-,o=---"""
permissions_urw_grw = permissions_urw_gr | stat.S_IWGRP

"""u=rwx,g=r-x,o=---"""
permissions_urwx_grx = permissions_urw_grw | stat.S_IXUSR | stat.S_IXGRP


class Formatter(logging.Formatter):
    def format(self, record):
        record.app_version = __version__
        return super().format(record)


formatter = Formatter('[%(asctime)s][%(app_version)s][%(name)s][%(levelname)s] %(message)s')
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setFormatter(formatter)
stdout_handler.setLevel(logging.INFO)
handlers = [stdout_handler]
loggers = []


def setup_handler(handler, register_loggers=True):
    """Register a new handler to all known loggers and set log level to INFO"""
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    handlers.append(handler)
    if register_loggers:
        for l in loggers:
            l.addHandler(handler)


def remove_handler(handler):
    """Close a handler and unregister a handler from all loggers"""
    handler.close()
    handlers.remove(handler)
    for l in loggers:
        l.removeHandler(handler)


def setup_logger(name):
    """Create a new logger and register to all known handlers"""
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    for h in handlers:
        logger.addHandler(h)

    loggers.append(logger)
    return logger


def discover_files(pattern) -> list:
    """Search a file path, potentially containing Unix wildcards, and return alphabetically sorted results"""
    files = sorted(glob.glob(pattern))
    if not files:
        raise FileNotFoundError(f'Could not find any input files for pattern {pattern}')

    return files


def read_file(file, headerless=False):
    basename, ext = os.path.splitext(file)
    kwargs = {}
    if headerless:
        kwargs['header'] = None

    if ext in ('.csv', '.tsv', '.txt'):
        input_file_encoding = 'utf-8'
        with open(file, 'rb') as f:
            if f.read(3) == b'\xef\xbb\xbf':  # some files use weird encodings
                input_file_encoding = 'utf-8-sig'

        with open(file) as f:
            header = f.readline()
            if '\t' in header:
                delimiter = '\t'
            elif ',' in header:
                delimiter = ','
            else:
                raise InvalidDataException(
                    f"Could not infer delimiter in header '{header[:30]}...' for file {file}"
                )

        kwargs.update(
            index_col=False,
            delimiter=delimiter,
            encoding=input_file_encoding,
            float_precision='round_trip'
            # floating point weirdness - see https://stackoverflow.com/questions/12877189/float64-with-pandas-to-csv
        )
        return pandas.read_csv(file, **kwargs)

    elif ext == '.xls':
        raise InvalidDataException(
            'Remember what happened the last time somebody used XLS: https://www.bbc.com/news/technology-54423988'
        )

    elif ext == '.xlsx':
        kwargs['engine'] = 'openpyxl'
        return pandas.read_excel(file, **kwargs)


def list_files_to_dataframe(pattern, colname):
    """
    Glob a file path for files and return a single-column data frame
    :param pattern: path to search for files
    :param str colname: Column name to create
    """
    return pandas.DataFrame(data={colname: discover_files(pattern)})


def _unpack_cursor(cursor):
    yield cursor.keys()
    for _line in cursor:
        yield _line


def export_to_csv(lines, output_file):
    """
    Take some database lines and export to CSV format.
    :param lines: A SQLAlchemy ResultProxy or an iterable representing the lines of a table to export (including header)
    :param output_file: Path to the created CSV file
    """

    if isinstance(lines, sqlalchemy.engine.result.ResultProxy):  # sqlalchemy cursor
        lines = _unpack_cursor(lines)

    with open(output_file, 'w', newline='') as f:
        writer = csv.writer(f)
        for line in lines:
            writer.writerow(line)


template_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'docs'))
)


def render_template(doc_template, **kwargs):
    """
    Interpret a Jinja2 template from `docs/` and output as a string.
    :param str doc_template: Template basename
    :param kwargs: Arguments to pass to Template.render()
    """
    return template_env.get_template(doc_template).render(kwargs)


def write_document(content, output_file):
    """
    Write a string, e.g. interpreted from a template, and output to file.
    :param str content:
    :param str output_file:
    """
    with open(output_file, 'w') as f:
        f.write(content)


def set_file_permissions(f, chgrp=None, chmod=None):
    """
    Set group ownership and permissions on a file
    :param str f:
    :param str chgrp:
    :param int chmod: Numerical permissions to set on the file
    """
    if chgrp:
        shutil.chown(f, group=chgrp)

    if chmod:
        os.chmod(f, chmod)


def set_directory_permissions(directory, chgrp=None, dir_chmod=None, file_chmod=None):
    """
    Recursively change group ownership of a directory and set permissions
    :param str directory:
    :param str chgrp:
    :param int dir_chmod: Numerical permissions to set for directories
    :param int file_chmod: Numerical permissions to set for files
    """
    for base, dirs, files in os.walk(directory):
        shutil.chown(base, group=chgrp)
        if dir_chmod:
            os.chmod(base, dir_chmod)

        for f in files:
            set_file_permissions(os.path.join(base, f), chgrp, file_chmod)


class Linker:
    """
    Holding class for cleaning kits, patients and timepoints:
    - Sample repeats and remappings are accounted for with the sample-sample linkage table
    - Barcodes cannot be cleaned from other IDs without an accurate sample type, so are currently not cleaned
    - Kit IDs are cleaned from the LIMS via the barcode, falling back on the patient and timepoint
    - Patient IDs are cleaned using the linkage table, falling back on the LIMS
    - Missing timepoints are cleaned using the LIMS

    Contains various data frames for applying the above cleaning. LIMS information needs to be cleaned with the linkage
    tables first before it can be used, so we need setter methods to add the LIMS data later on.
    """
    def __init__(self, samples_to_patients, samples_to_samples):
        """
        :param pandas.DataFrame samples_to_patients:
        :param pandas.DataFrame samples_to_samples:
        """

        self.lims_data_loaded = False

        """
        Initialised from the linkage table, and updated from the LIMS:
        sample_id  canonical_isaric_id
        CCP0001    ABCDE-0001 
        """
        self.samples_to_patients = samples_to_patients[['sample_id', 'canonical_isaric_id']]

        """
        Populated from the sample linkage table:
        rnaseq_sample_id  isaric_sample_id
        CCP0001-R         CCP0001

        Todo: the RNA-seq linkage table is the sample linkage table - make this generic
        """
        self.samples_to_samples = samples_to_samples[['sample_id', 'isaric_sample_id']]

        """
        LIMS barcode info, mapping to kits:
        barcode      clean_kit_id
        SM200401234  CCP0001
        SMG012345    CVRI001
        012345       CVRI001
        12345        CVRI001
        """
        self.lims_barcodes_to_kits = None

        """
        LIMS kit information:
        Kit_ID   Patient_ID  Timepoint
        CCP0001  ABCDE-0001  Day 1
        CCP0002  ABCDE-0002  Day 1
        CCP0003  ABCDE-0002  Day 1
        """
        self.lims_kits = None

        """
        As above for lims_kits, but any conflicts where one patient/timepoint has multiple kits are removed
        Kit_ID   Patient_ID  Timepoint
        CCP0001  ABCDE-0001  Day 1
        """
        self.lims_patient_timepoint_to_kits = None

    def add_barcode_kit_mappings(self, df, barcode_col, clean_kit_col):
        self.lims_barcodes_to_kits = pandas.concat(
            [
                self.lims_barcodes_to_kits,
                df.rename(columns={barcode_col: 'barcode', clean_kit_col: 'clean_kit_id'})[['barcode', 'clean_kit_id']]
            ]
        ).sort_values(by=['barcode'], ignore_index=True)

    def add_sample_patient_mappings(self, df, sample_col, patient_col):
        self.samples_to_patients = pandas.concat(
            [
                self.samples_to_patients,
                df.rename(columns={sample_col: 'sample_id', patient_col: 'canonical_isaric_id'})[['sample_id', 'canonical_isaric_id']]
            ]
        ).drop_duplicates(
            subset='sample_id',
            keep='first'  # don't let the LIMS override the original linkage table mapping
        ).sort_values(by=['sample_id'], ignore_index=True)

    def register_lims_data(self, df):
        self.add_sample_patient_mappings(df, 'kit_id', 'patient_id')
        self.add_barcode_kit_mappings(df, 'sample_code', 'kit_id')

        # for Glasgow barcodes, e.g. SMG012345, we also need to register shortened versions like 012345...
        glasgow_barcodes = df[df['sample_code'].str.startswith('SMG')][['sample_code', 'kit_id']]
        glasgow_barcodes['sample_code'] = glasgow_barcodes['sample_code'].str.slice(3)
        self.add_barcode_kit_mappings(glasgow_barcodes, 'sample_code', 'kit_id')

        # ... and 12345
        exceled_glasgow_barcodes = glasgow_barcodes.copy()
        exceled_glasgow_barcodes['sample_code'] = exceled_glasgow_barcodes['sample_code'].str.slice(1)
        self.add_barcode_kit_mappings(exceled_glasgow_barcodes, 'sample_code', 'kit_id')

        # get LIMS entries distinct on kit/patient/timepoint...
        self.lims_kits = df[['kit_id', 'patient_id', 'timepoint']].drop_duplicates().sort_values(
            by=['kit_id'], ignore_index=True
        )

        # ... then get any unambiguous patient/timepoint -> kit mappings
        self.lims_patient_timepoint_to_kits = self.lims_kits.drop_duplicates(
            ['patient_id', 'timepoint'],
            keep=False,  # drop all conflicts
            ignore_index=True
        ).sort_values(by=['patient_id', 'timepoint'], ignore_index=True)

        self.lims_data_loaded = True
