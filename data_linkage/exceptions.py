

class MalformedFileException(Exception):
    pass


class InvalidDataException(Exception):
    pass


class ScriptError(Exception):
    pass
