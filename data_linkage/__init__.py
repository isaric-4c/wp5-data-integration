import datetime

__version__ = 'v1.14'
__now__ = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
__today__ = datetime.date.today().strftime('%Y-%m-%d')
