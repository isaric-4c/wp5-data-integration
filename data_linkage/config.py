import os
import yaml


class Config:
    """
    Loads a yaml file into a dict and defines methods for accessing it.
    """
    def __init__(self, file=None, env=None):
        self.file = file
        self.config_env = env
        self._content = None

    @property
    def content(self):
        if self._content is None:
            cfg_file = self.discover_file() if self.config_env else self.file
            with open(cfg_file) as f:
                self._content = yaml.safe_load(f)

        return self._content

    def discover_file(self):
        if not os.getenv(self.config_env):
            raise AssertionError('Config file not set - set %s' % self.config_env)

        config_file = os.getenv(self.config_env)
        if not os.path.isfile(config_file):
            raise AssertionError('Config file %s not found' % config_file)

        return config_file

    def __getitem__(self, item):
        return self.content[item]

    def get(self, item, ret_default=None):
        return self.content.get(item, ret_default)

    def query(self, *args, ret_default=None):
        """
        Drill down into the config, e.g.
        `cfg.query('sample_dispatch_parameters', 'some_dispatch_location', 'sample_types')`
        :param str args: Sequence of keys to drill down into
        :param ret_default: What to return if nothing is found
        """
        current_level = self.content

        for a in args:
            current_level = current_level.get(a)
            if not current_level:
                return ret_default

        return current_level


cfg = Config(env='ISARIC_LINKAGE_CONFIG')
