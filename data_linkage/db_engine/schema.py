from sqlalchemy import Column, String, Integer, case, func, event
from sqlalchemy.sql import table
from sqlalchemy.sql.expression import FunctionElement
from sqlalchemy.ext import compiler
from sqlalchemy.schema import DDLElement, SetTableComment, SetColumnComment


"""
Semi-official SQLAlchemy view creation from https://github.com/sqlalchemy/sqlalchemy/wiki/Views
When defining the selection for a view, we should use `.label()` to explicitly name columns. Apparently officially
supporting views in SQLAlchemy would be non-trivial. Also see http://www.sqlite.org/c3ref/column_name.html
"""


class CreateView(DDLElement):
    """Compiles to a CREATE VIEW statement. Takes a view name and a sqlalchemy select statement."""
    def __init__(self, name, selectable):
        """
        :param str name: The name of the view to create
        :param sqlalchemy.Select selectable: SQLAlchemy query that this view will perform
        """
        self.name = name
        self.selectable = selectable


class DropView(DDLElement):
    """Compiles to a DROP VIEW statement."""
    def __init__(self, name, cascade=False):
        self.name = name
        self.cascade = cascade


class CreateSchemaIfNotExists(DDLElement):
    def __init__(self, name):
        self.name = name


class SetViewComment(SetTableComment):
    """Compiles to a COMMENT ON VIEW <view> IS <comment> statement."""
    pass


@compiler.compiles(CreateSchemaIfNotExists)
def compile(element, compiler, **kw):
    return 'CREATE SCHEMA IF NOT EXISTS %s' % element.name


@compiler.compiles(CreateView)
def compile(element, compiler, **kw):
    view_name = element.name
    if element.target.schema:
        view_name = element.target.schema + '.' + view_name

    return "CREATE VIEW %s AS %s" % (
        view_name,
        compiler.sql_compiler.process(element.selectable, literal_binds=True)
    )


@compiler.compiles(DropView)
def compile(element, compiler, **kw):
    expression = "DROP VIEW %s" % element.name
    if element.cascade:
        expression += " CASCADE"
    return expression


@compiler.compiles(SetViewComment)
def compile(element, compiler, **kw):
    """Run COMMENT ON VIEW... similar to DDLCompiler.visit_set_table_comment."""

    return 'COMMENT ON VIEW %s IS %s' % (
        compiler.dialect.identifier_preparer.format_table(element.element),
        compiler.sql_compiler.render_literal_value(element.element.comment, String())
    )


def view(name, metadata, selectable, comment=None, statement_descriptions=None):
    """
    Create a SQL view from the given selectable and register to metadata.
    :param str name: View name
    :param sqlalchemy.MetaData metadata:
    :param sqlalchemy.Select selectable: SQLAlchemy query for the view
    :param str comment: View description to set
    :param dict statement_descriptions: Column descriptions to set on any calculated view columns, e.g. case statements
    """
    t = table(name, schema=metadata.schema)
    t.comment = comment  # bit janky, but works

    for source_col in selectable.c:
        source_col._make_proxy(t)

        # Set column comment based on statement_descriptions. If a column has just been pulled in from a table, the
        # comment will also include the name of the table it's pulled from, plus the name and any description of the
        # source column
        col_comment = []
        if len(source_col.base_columns) == 1:
            base_col = list(source_col.base_columns)[0]
            if isinstance(base_col, Column):
                col_comment = [f'{base_col.table.name}.{base_col.name}']
                if base_col.comment:
                    col_comment.append(base_col.comment)

        if statement_descriptions and source_col.name in statement_descriptions:
            col_comment.append(statement_descriptions[source_col.name])

        t.c[source_col.name].comment = '. '.join(col_comment)

    event.listen(metadata, 'after_create', CreateView(name, selectable))
    event.listen(metadata, 'after_create', SetViewComment(t).execute_if(dialect='postgresql'))
    for col in t.c:
        event.listen(metadata, 'after_create', SetColumnComment(col).execute_if(dialect='postgresql'))

    event.listen(metadata, 'before_drop', DropView(name))
    return t


# adapted from https://github.com/sqlalchemy/sqlalchemy/issues/5687
class CreateTempTableAs(DDLElement):
    def __init__(self, name, meta, selectable):
        schema_name = meta.schema + '.' if meta.schema else ''
        self.table_name = schema_name + name
        self.selectable = selectable


class DropTempTable(DDLElement):
    def __init__(self, name):
        self.name = name


@compiler.compiles(CreateTempTableAs)
def compile(element, compiler, **kw):
    return "CREATE TABLE %s AS %s" % (
        element.table_name,
        compiler.sql_compiler.process(element.selectable, literal_binds=True),
    )


@compiler.compiles(DropTempTable)
def compile(element, compiler, **kw):
    return 'DROP TABLE %s' % element.name


def temp_table(name, metadata, selectable):
    """Create a temporary table from a selectable."""
    t = table(name)

    for c in selectable.c:
        c._make_proxy(t)

    CreateTempTableAs(name, metadata, selectable).execute_at('after-create', metadata)
    DropTempTable(name).execute_at('before-drop', metadata)
    return t


class absoluteday(FunctionElement):
    """Generic function that allows generic date comparisons in Postgres and SQLite - see compilers below."""
    name = 'absoluteday'


@compiler.compiles(absoluteday)
def compile(element, compiler, **kw):
    return compiler.process(element.clauses, **kw)  # do nothing by default


@compiler.compiles(absoluteday, 'sqlite')
def compile(element, compiler, **kw):
    return "julianday(%s)" % compiler.process(element.clauses, **kw)


def days_elapsed(label, d1, d2):
    diff = absoluteday(d2) - absoluteday(d1)
    return diff.cast(Integer).label(label)


class catunique(FunctionElement):
    """Generic aggregate function that concatenates strings with the supplied separator."""
    name = 'strcat'


@compiler.compiles(catunique)
def compile(element, compiler, **kw):
    return "STRING_AGG(DISTINCT %s, ',')" % compiler.process(element.clauses, **kw)


@compiler.compiles(catunique, 'postgresql')
def compile(element, compiler, **kw):
    return "string_agg(DISTINCT CAST(%s AS VARCHAR), ',')" % compiler.process(element.clauses, **kw)


@compiler.compiles(catunique, 'sqlite')
def compile(element, compiler, **kw):
    return 'group_concat(DISTINCT %s)' % compiler.process(element.clauses, **kw)


def value_if_unique(col):
    """Aggregate function. If there's one value present then return it, if there are multiple values then return null"""
    return case(
        [(func.count(col.distinct()) == 1, func.max(col))],
        else_=None
    )
