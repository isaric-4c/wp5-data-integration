import os
import re
import subprocess
from stat import filemode
from sqlalchemy import create_engine, MetaData, event
from sqlalchemy.sql.ddl import sort_tables_and_constraints
from data_linkage import utils
from data_linkage.db_engine import schema
from data_linkage.config import cfg
from data_linkage.exceptions import ScriptError
from data_linkage.data_sources import all_tables, all_primary_data_sources, all_secondary_data_sources, all_views


def get_database():
    """Return the right Database engine depending on configuration"""
    c = cfg['database_engine']
    if c == 'sqlite':
        return SQLiteDatabase()

    elif c == 'postgres':
        return PostgresDatabase()

    else:
        raise KeyError(f'Unexpected database engine config: {c}')


class Database:
    """Base SQL engine handler class"""

    name = None

    def __init__(self, primary_data_sources=None, views=None, secondary_data_sources=None):
        self.logger = utils.setup_logger(self.__class__.__name__)
        self.engine = self._get_engine()
        self.meta = self.get_meta()

        self.primary_data_source_names = primary_data_sources or tuple(all_primary_data_sources)
        self.view_names = views or tuple(all_views)
        self.secondary_data_source_names = secondary_data_sources or tuple(all_secondary_data_sources)

        """
        All configured primary data source objects for this database. Reference SQLalchemy tables with, e.g,
        `db.primary_data_sources['clinical_data'].table`
        """
        self.primary_data_sources = {k: all_tables[k](self) for k in self.primary_data_source_names}

        """All declared views for this database"""
        self.views = {k: all_tables[k](self) for k in self.view_names}

        """All secondary data sources for this database"""
        self.secondary_data_sources = {k: all_tables[k](self) for k in self.secondary_data_source_names}

        """Combination of self.primary_data_sources, self.views and self.secondary_data_sources"""
        self.all_tables = {}
        for d in (self.primary_data_sources, self.views, self.secondary_data_sources):
            for k, v in d.items():
                self.all_tables[k] = v

    def get_meta(self):
        """
        Create a sqlalchemy.Metadata object. We want to do this differently for different database engines because
        Postgres backends may have a schema defined, which SQLite doesn't like.
        """
        raise NotImplementedError

    def drop(self):
        """Reset self.meta, drop all SQL tables and reset all data sources"""
        self.meta = self.get_meta()
        self._drop_tables()
        for v in self.all_tables.values():
            v._table = None

    def init_tables(self):
        """Init all data source tables and reflect that in self.meta with create_all"""
        for v in self.all_tables.values():
            _ = v.table  # initialise the SQL table, cascading to others if needed

        self.meta.create_all(self.engine)

    def _drop_tables(self):
        """
        Drop all known views and tables according to the defined data sources and schema. There may be other tables in
        the database not added by this linkage, but they will not be dropped.
        """
        meta = self.get_meta()
        schema_name = meta.schema + '.' if meta.schema else ''

        with self.engine.connect() as c:
            meta.reflect(self.engine, views=True)
            present_views = [
                v for v in self.engine.dialect.get_view_names(c, schema=meta.schema)
                if v in self.view_names
            ]

            # SQLAlchemy can handle table dependencies with foreign keys, but we need to handle views ourselves.
            # Regexing selectables out of the string view definition is janky, but couldn't find any better way of doing
            # it at inspect-time. Can just use Table.add_is_dependent_on though, and the rest works.
            for name in present_views:
                view_def = self.engine.dialect.get_view_definition(c, name, meta.schema)
                dependencies = set(
                    re.findall(r'FROM \(*([^\n ();,]+)', view_def) + re.findall(r'JOIN ([^ ]+) ON', view_def)
                )

                # `FROM (SELECT ...` statements complicate things - distinguishing from `FROM (some_table...` in regex
                # is hard, so just remove any SELECT matches
                if 'SELECT' in dependencies:
                    dependencies.remove('SELECT')

                for d in dependencies:
                    meta.tables[schema_name + name].add_is_dependent_on(meta.tables[d])

            # drop the views in the right order using the dependencies defined above
            views = sort_tables_and_constraints([meta.tables[schema_name + v] for v in present_views])
            for v, foreign_key_constraints in reversed(views):
                if v is not None:
                    c.execute(schema.DropView(v))

            tables = []
            for ds in list(self.primary_data_source_names) + list(self.secondary_data_source_names):
                table_name = schema_name + ds
                if table_name in meta.tables:
                    tables.append(meta.tables[table_name])

            meta.drop_all(
                self.engine,
                tables=tables
            )

    def connect(self):
        return self.engine.connect()  # .execution_options(schema_translate_map={None: "isaric"})

    def _get_engine(self):
        raise NotImplementedError

    def backup(self, release_dir):
        """
        Take a snapshot of the current ISARIC database and write to disk, e.g. through pg_dump or copying a SQLite file.
        """
        pass


class SQLiteDatabase(Database):
    name = 'sqlite'

    def __init__(self, primary_data_sources=None, views=None, secondary_data_sources=None, db_file=None):
        """
        :param db_file: Alternate SQLite database for running exports
        """
        self.db_file = db_file or os.path.join(os.path.abspath(cfg['data_release_path']), 'isaric.sqlite')
        super().__init__(primary_data_sources, views, secondary_data_sources)

    def _get_engine(self):
        return create_engine('sqlite:///' + self.db_file)

    def get_meta(self):
        return MetaData()


class PostgresDatabase(Database):
    """Postgres engine. Requires a pgpass file to be configured."""

    name = 'postgres'

    def _get_engine(self):
        pgpass = os.getenv('PGPASSFILE', os.path.join(os.getenv('HOME'), '.pgpass'))
        if not os.path.isfile(pgpass):
            raise FileNotFoundError(f'pgpass file at {pgpass} not found')

        permissions = filemode(os.stat(pgpass).st_mode)
        if permissions != '-rw-------':
            raise PermissionError(
                f'Incorrect permissions for pgpass file at {pgpass}: {permissions}, should be -rw-------'
            )

        postgres_server = cfg['postgres']['server']
        postgres_user = cfg['postgres']['user']

        engine = create_engine(f'postgresql://{postgres_user}@{postgres_server}/isaric')

        # Make sure the right role gets set each time. This gets passed to Engine.connect and MetaData.create_all
        # https://docs.sqlalchemy.org/en/14/core/engines.html#modifying-the-dbapi-connection-after-connect-or-running-commands-after-connect
        @event.listens_for(engine, 'connect')
        def connect(dbapi_connection, connection_record):
            cursor = dbapi_connection.cursor()
            cursor.execute('SET ROLE isaric_admin')
            cursor.connection.commit()  # without this, tables get created with wrong permissions
            cursor.close()

        return engine

    def get_meta(self):
        return MetaData(schema='isaric')

    def backup(self, release_dir):
        """
        Run pg_dump on the ISARIC database for all known tables and gzip the output.
        :param str release_dir:
        """
        self.logger.info('Backing up database')
        release_name = os.path.basename(release_dir)
        export_path = os.path.join(release_dir, f'pgdump_{release_name}.tar')

        pg_dump = cfg.query('postgres', 'pg_dump')
        if not pg_dump:
            pg_dump = subprocess.check_output(['which', 'pg_dump']).decode().strip()

        pgdump_cmd = [
            pg_dump,
            '-U', cfg['postgres']['user'],
            '-h', cfg['postgres']['server'],
            '-d', 'isaric',
            '-n', 'isaric',
            '--role', 'isaric_admin',
            '-f', export_path,
            '-F', 't',
            '-t', f'isaric.*'
        ]
        with subprocess.Popen(pgdump_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as p:
            exit_status = p.wait()
            for line in p.stdout:
                self.logger.info(line)

            self.logger.info('pg_dump finished with exit status %i', exit_status)

        if exit_status != 0 or not os.path.isfile(export_path):
            raise ScriptError(f'Failed to generate export {export_path}')

        with subprocess.Popen(['gzip', export_path], stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as p:
            exit_status = p.wait()
            for line in p.stdout:
                self.logger.info(line)

            self.logger.info('Compression finished with exit status %i', exit_status)

        export_path += '.gz'

        if not os.path.isfile(export_path):
            raise ScriptError(f'Failed to compress export {export_path}')

        utils.set_file_permissions(export_path, chmod=utils.permissions_urw_grw)

        self.logger.info('Backed up to %s', export_path)
