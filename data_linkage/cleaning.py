import re
import pandas
from numpy import NaN
from sqlalchemy import String
from datetime import datetime, date, time
from data_linkage.utils import setup_logger


"""
Patient ID regexes. Patient IDs should consist of an NHS site code, a dash and a 4-digit PIN, e.g. 'REMRQ-0001'. This is
enforced in the clinical data already, but is often entered manually elsewhere so there will be some inconsistencies
that need to be checked for.
"""
canonical_isaric_id_pattern = re.compile(r'^[A-Z0-9]{5}-[0-9]{4}$')
patient_id_cleaning_pattern = re.compile(r'^([A-Z0-9]{5})(|--|/)([0-9]{4})$')

"""
Timepoint regexes. Timepoints may be entered as 'Day x', 'x', or 'Convalescent'. 'x' is cleaned to 'Day x'. Day 1
samples can be recorded as 'Day 1', 'Recruitment' or 'Recruitment / Day 1' - these are cleaned to 'Day 1'.
"""
recruitment_day_1_pattern = re.compile(r'^Recruitment(| / Day 1)$')
numerical_timepoint_pattern = re.compile(r'^(Day |)([0-9]+)$')
valid_timepoint_pattern = re.compile(r'^(Day [0-9]+|Convalescent)$')


"""
Boolean regexes. Sometimes booleans are represented as yes/no strings
Todo: remove duplication with clinical schema inference
"""
bool_pattern_true = re.compile(r'^(yes|true|y|1)$', re.IGNORECASE)  # Yes, YES, True, true, TRUE, 1
bool_pattern_false = re.compile(r'^(no|false|n|0)$', re.IGNORECASE)

"""
Date/time regexes
Todo: remove duplication with clinical schema inference
"""
date_ymd_pattern = re.compile(r'^([0-9]{4})(|-|/)([0-1][0-9])(|-|/)([0-3][0-9])$')  # 20201214, 2020-12-14, 2020/12/14
date_dmy_pattern = re.compile(r'^([0-3][0-9])(|-|/)([0-1][0-9])(|-|/)([0-9]{4})$')  # 14122020, 14-12-2020, 14/12/2020
time_pattern = re.compile(r'^([0-2][0-9]):([0-5][0-9]):([0-5][0-9])$')


logger = setup_logger('cleaning')


class PatientID(String):
    """
    Represents an ISARIC patient ID as a SQLAlchemy type, so that it gets cleaned with clean_isaric_id(). Use in
    column definitions:

        sqlalchemy.Column('patient_id', IsaricID)
    """
    pass


class Timepoint(String):
    """
    Represents a timepoint as a SQLAlchemy type, to be cleaned with clean_timepoint(). Use in column definitions:

        sqlalchemy.Column('timepoint', Timepoint)
    """
    pass


class SampleID(String):
    """
    Not used in cleaning, but used to distinguish sample ID columns at runtime so we can do things like export tables
    without sample ID columns:

        sqlalchemy.Column('rnaseq_sample_id', SampleID)
        sqlalchemy.Column('clean_rnaseq_sample_id', SampleID)
        sqlalchemy.Column('clean_kit_id', SampleID)
    """
    pass


def clean_isaric_id(vect):
    """
    Clean any known errors and inconsistencies in patient IDs. Attempts to match if a fix is identified, reformat to a
    real patient ID:
    - no dash, e.g. REMRQ0001
    - double '-', e.g. REMRQ--0001
    - '/' instead of '-', e.g. REMRQ/0001

    This began as a series of SQL scripts from Gary Leeming: https://github.com/grazulis/isaric-data-link
    :param pandas.Series vect:
    """
    cleaned_vect = vect.str.replace(patient_id_cleaning_pattern, '\\1-\\3')
    invalid_ids = cleaned_vect[~cleaned_vect.str.match(canonical_isaric_id_pattern, na=False)].unique()
    if invalid_ids.any():  # unique() returns a numpy ndarray, not a pandas Series
        logger.info(f'Found {len(invalid_ids)} invalid ISARIC IDs: {invalid_ids}')

    return cleaned_vect


def clean_timepoint(vect):
    """
    :param pandas.Series vect:
    Clean a timepoint column:
    - 'Recruitment' or 'Recruitment / Day 1' is cast to 'Day 1'
    - The timepoint should be 'Convalescent', or 'Day x'
    """
    cleaned_vect = vect.astype(str).str.replace(recruitment_day_1_pattern, 'Day 1')
    cleaned_vect = cleaned_vect.str.replace(numerical_timepoint_pattern, 'Day \\2')
    cleaned_vect = cleaned_vect.str.extract(valid_timepoint_pattern, expand=False)
    uncastable_timepoints = vect.where(cleaned_vect.isnull()).unique()
    if uncastable_timepoints.any():
        logger.info(f'Could not cast {len(uncastable_timepoints)} timepoints: {uncastable_timepoints}')

    return cleaned_vect


def _clean_boolean(cell):
    if isinstance(cell, bool):
        return cell

    if isinstance(cell, int):
        return bool(cell)

    elif not cell:
        return None

    elif isinstance(cell, str):
        if bool_pattern_true.match(cell):
            return True

        elif bool_pattern_false.match(cell):
            return False


def clean_boolean(vect):
    """
    Cast string values for booleans, e.g. Yes, NO, true, N to boolean.
    :param pandas.Series vect:
    """
    return vect.apply(_clean_boolean)


def clean_float(vect):
    return pandas.to_numeric(vect, errors='coerce').astype('float64')


def clean_int(vect):
    return pandas.to_numeric(vect, errors='coerce').replace(pandas.NA, NaN).round().astype('Int64')


def _clean_date(cell):
    if isinstance(cell, datetime):
        return cell.date()

    elif isinstance(cell, date):
        return cell

    elif isinstance(cell, int):  # e.g, `20210112`
        cell = str(cell)

    if isinstance(cell, str):
        match = date_ymd_pattern.match(cell)
        if match:
            year, sep1, month, sep2, day = match.groups()
            return date(int(year), int(month), int(day))

        match = date_dmy_pattern.match(cell)
        if match:
            day, sep1, month, sep2, year = match.groups()
            return date(int(year), int(month), int(day))


def clean_date(vect):
    """
    Cast a column to datetime.date. Picks up dates, datetimes and formatted strings according to date_ymd_pattern and
    date_dmy_pattern.
    :param pandas.Series vect:
    """
    return vect.apply(_clean_date)


def _clean_time(cell):
    if isinstance(cell, time):
        return cell

    elif isinstance(cell, datetime):
        return cell.time()

    elif isinstance(cell, str):
        match = time_pattern.match(cell)
        if match:
            hour, minute, second = match.groups()
            return time(int(hour), int(minute), int(second))


def clean_time(vect):
    """
    Cast a column to datetime.time
    :param pandas.Series vect:
    """
    return vect.apply(_clean_time)


def clean_numeric_id(vect):
    """Clean IDs read from Excel as numbers, e.g. 202211000001.0, to 202211000001, taking 'na's into account."""
    return vect.astype(str).str.replace(r'.0$', '').mask(vect.isna(), None)


generic_cleaning_funcs = {
    bool: clean_boolean,
    date: clean_date,
    time: clean_time,
    float: clean_float,
    int: clean_int,
    PatientID: clean_isaric_id,
    Timepoint: clean_timepoint
}
