import os
import pandas
from sqlalchemy import Column, String
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from data_linkage.utils import discover_files, read_file
from data_linkage.config import cfg
from . import Assay


class WP5WGS(Assay):
    name = 'wp5_wgs'
    description = 'Patient WGS data from ISARIC and GENOMICC'
    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'

    columns = (
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('sample_id', SampleID),
        Column('kit_id', SampleID),
        Column('raw_sample_id', SampleID),
        Column('file_path', String),
        Column('file_type', String)
    )

    def __init__(self, db):
        wgs_cfg = cfg['raw_data_sources'][self.name]
        self.data_dirs = wgs_cfg['dirs']
        self.upstream_mapping_file = wgs_cfg['upstream_mappings']
        self.gel_mapping_file = wgs_cfg['gel_mappings']
        super().__init__(db)

    def get_source(self):
        return ','.join([d['path'] for d in self.data_dirs] + [self.upstream_mapping_file, self.gel_mapping_file])

    def init_dataframe(self):
        """
        Look for GVCFs from GENOMICC and ISARIC and read a mapping file to go from Illumina library prep IDs to ISARIC
        IDs.
        """
        upstream_mappings = read_file(
            discover_files(self.upstream_mapping_file)[-1],
            headerless=True
        )
        upstream_mappings[1] = upstream_mappings[1].str.replace('^rtcovid', '')

        gel_mappings = read_file(
            discover_files(self.gel_mapping_file)[-1]
        )
        gel_mappings = gel_mappings[['Illumina_ID', 'genomicc_ID']]

        for df in upstream_mappings, gel_mappings:
            df.columns = ('raw_sample_id', 'sample_id')

        all_mappings = pandas.concat((gel_mappings, upstream_mappings))

        all_files = []
        for dir_cfg in self.data_dirs:
            all_files.extend(
                [
                    (filename, raw_sample_id, file_type)
                    for filename, raw_sample_id, file_type in self.scrape_dir(dir_cfg['path'], dir_cfg['file_ext'])
                ]
            )

        df = pandas.DataFrame.from_records(
            all_files,
            columns=('file_path', 'raw_sample_id', 'file_type')
        )

        df = df.merge(all_mappings, how='left', left_on='raw_sample_id', right_on='raw_sample_id')
        return df.sort_values('file_path').reset_index(drop=True)

    @classmethod
    def scrape_dir(cls, top_level, file_ext):
        file_type = file_ext.replace('.gz', '')  # 'gvcf.gz' -> 'gvcf'
        for root, dirs, files in os.walk(top_level):
            for f in files:
                if f.endswith(file_ext):
                    yield os.path.join(root, f), cls.get_raw_sample_id(f), file_type

    @staticmethod
    def get_raw_sample_id(filename):
        if filename.startswith('rtcovidGCC'):
            return filename.replace('rtcovid', '').split('_')[0]

        elif filename.startswith('LP'):
            return filename.split('.')[0]
