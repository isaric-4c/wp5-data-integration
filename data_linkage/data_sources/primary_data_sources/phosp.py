from sqlalchemy import Column, String, Date
from data_linkage.cleaning import PatientID, SampleID
from data_linkage.data_sources.primary_data_sources.common import PrimaryDataSource


class PHOSPLIMSData(PrimaryDataSource):
    name = 'phosp_lims_data'
    description = 'PHOSP follow-up LIMS data'

    rename_columns = {
        'Sample Code': 'sample_code',
        'Site': 'site',
        'Patient ID': 'phosp_patient_id',
        'Kit ID': 'kit_id',
        'ISARIC ID (if known)': 'isaric_patient_id',
        'Sample Type': 'sample_type',
        'Date Taken': 'date_taken',
        'Freezer': 'freezer',
        'Tower/Drawer': 'tower_or_drawer',
        'Box': 'box',
        'Location': 'location',
        'Cell Count': 'cell_count',
        'Number of Vials': 'number_of_vials',
        'Cell Count per Vial': 'cell_count_per_vial',
        'Quality Incidents': 'quality_incidents'
    }

    columns = (
        Column('sample_code', SampleID, primary_key=True, comment='Unique sample barcode'),
        Column('site', String),
        Column('phosp_patient_id', String, comment='PHOSP patient ID'),
        Column('kit_id', SampleID, comment='PHOSP kit ID'),
        Column('isaric_patient_id', PatientID, comment='ISARIC patient ID, if known'),
        Column('sample_type', String),
        Column('date_taken', Date),
        Column('freezer', String),
        Column('tower_or_drawer', String),
        Column('box', String),
        Column('location', String),
        Column('cell_count', String),
        Column('number_of_vials', String),
        Column('cell_count_per_vial', String),
        Column('quality_incidents', String),
    )
