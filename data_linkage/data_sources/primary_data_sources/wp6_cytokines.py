from sqlalchemy import Column, String, Float, Integer, Boolean
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class WP6PlasmaCytokines(Assay):
    """Parser for ISARIC plasma cytokine data.xlsx"""

    name = 'wp6_plasma_cytokines'
    dirty_sample_id_col = 'Barcode'
    clean_kit_id_col = 'Kit ID'

    rename_columns = {
        'UpdatedID': 'Patient ID',
        'ID': 'Patient ID'
    }

    description = ('Plasma cytokine data from WP-6. Some samples have barcodes, while others only have patient ID and '
                   'timepoint.')

    columns = (
        # can't have primary keys - there are samples with the same barcode, box ID and assay
        Column('Patient ID', PatientID),
        Column('Visit Day', Timepoint, comment='Sampling timepoint'),
        Column('Kit ID', SampleID, comment='Clean kit ID'),  # should always have something
        Column('Box ID', String),
        Column('Barcode', SampleID, comment='LIMS barcode if present'),  # can't be a primary key - sometimes null/0
        Column('Assay', String),
        Column('Value', Float)
    )

    def clean_datatypes(self, df):
        df = super().clean_datatypes(df)
        df['Barcode'] = df['Barcode'].apply(self._clean_barcode)
        return df

    @staticmethod
    def _clean_barcode(cell):
        if cell == '0' or cell == 0:
            return None
        else:
            return cell


class WP6FaecalCytokines(Assay):
    name = 'wp6_faecal_cytokines'
    description = 'Faecal cytokine data for WP6 update'
    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'

    columns = (
        Column('sample_id', SampleID),
        Column('bristol_score', Integer, comment='Bristol stool score, from 1-7'),
        Column('patient_id', PatientID),
        Column('assay', String, comment='Cytokine being measured for'),
        Column(
            'luminex_value',
            String,
            comment='Raw Luminex measurement in pg/ml. "OOR" = out of range, "*" = extrapolated value. Not intended to '
                    'be used directly - use "value" instead'
        ),
        Column('value', Float, comment='Cytokine measure, corrected for faecal weight to pg/g'),
        Column(
            'extrapolated',
            Boolean,
            comment='Whether the value fell outside the assay calibration curve and had to be extrapolated'
        ),
        Column('adj_value', Float, comment='value adjusted for luminex limits of detection'),
        Column('pg_g_value', Float, comment='value normalise by stool concentration (g/mL) from pg/mL to pg/g stool'),
        Column('pg_g_adj', Float, comment='adj_value normalised by stool concentration (g/mL) from pg/mL to pg/g stool'),
        Column('timepoint', Timepoint),
        Column('kit_id', SampleID)
    )
