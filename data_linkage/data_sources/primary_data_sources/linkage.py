from sqlalchemy import Column, String, Date
from data_linkage.cleaning import PatientID, SampleID
from . import PrimaryDataSource


class LinkageSampleToPatient(PrimaryDataSource):
    name = 'linkage_sample_to_patient'
    description = 'Table used behind the scenes in data integration. Maps sample IDs to REDCap patient IDs.'

    columns = (
        Column('sample_id', SampleID, primary_key=True, comment='Barcode or kit ID'),
        Column('canonical_isaric_id', PatientID, comment='The patient this sample corresponds to'),
        Column('added_date', Date),
        Column('source', String)
    )


class LinkageSampleToSample(PrimaryDataSource):
    name = 'linkage_sample_to_sample'
    description = ('Similar to the sample-patient linkage table, but re-maps sample IDs to sample IDs. Used, e.g. when '
                   'RNA-seq samples are reprocessed under a different name, or old kit IDs are updated. Barcodes or kit'
                   'IDs may be aliased here.')

    columns = (
        Column('sample_id', SampleID, primary_key=True, comment='Original raw sample ID'),
        Column('isaric_sample_id', SampleID, comment='The sample this alias corresponds to'),
        Column('added_date', Date),
        Column('source', String)
    )
