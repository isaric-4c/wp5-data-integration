from sqlalchemy import Column, String, Float
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class WP3BloodViralLoad(Assay):
    name = 'wp3_blood_viral_load'
    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'
    description = 'RT-qPCR viral load data for blood samples'

    rename_columns = {
        'Plate': 'plate',
        'Well': 'well',
        'Sample ID': 'sample_id',
        'Barcode': 'barcode',
        'Copies/ml Replicate 1': 'copies_ml_1',
        'Copies/ml Replicate 2': 'copies_ml_2'
    }

    columns = (
        Column('patient_id', PatientID),

        # can't be a primary key, some samples are blanks or repeats
        Column('kit_id', SampleID, comment='Clean ISARIC kit ID'),
        Column('timepoint', Timepoint),
        Column('plate', String),
        Column('well', String),

        Column('sample_id', SampleID, comment='Original kit ID/barcode'),
        # can't be a primary key, early samples don't have barcodes
        Column('barcode', String, comment='Internal CRF barcode'),

        Column('copies_ml_1', Float, comment='Viral copies per ml, replicate 1'),
        Column('copies_ml_2', Float, comment='Viral copies per ml, replicate 2')
    )
