from sqlalchemy import Column, String, Integer, Float
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class WP5ProteomicsPeptides(Assay):
    name = 'wp5_proteomics_peptides'
    description = 'Peptide quantifications, batch corrected'

    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'
    report_coverage_as = None
    rename_columns = {
        'Precursor.Id': 'precursor_id'
    }

    columns = (
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('kit_id', SampleID),
        Column('sample_id', SampleID, primary_key=True),
        Column('precursor_id', String, primary_key=True, comment='Peptide name'),
        Column('value', Float)
    )

    def clean_colnames(self, df):
        df = super().clean_colnames(df)
        return df.melt(id_vars=['precursor_id'], var_name='sample_id')


class WP5ProteomicsProteins(Assay):
    name = 'wp5_proteomics_proteins'
    description = 'Protein quantifications, batch corrected'

    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'
    report_coverage_as = None
    rename_columns = {
        'Protein.Ids': 'protein_ids',
        'Protein.Names': 'protein_names'
    }

    columns = (
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('kit_id', SampleID),
        Column('sample_id', SampleID, primary_key=True),
        Column('protein_ids', String, primary_key=True, comment='Semicolon-separated protein IDs'),
        Column('protein_names', String, primary_key=True, comment='Semicolon-separated protein names'),
        Column('value', Float)
    )

    def clean_colnames(self, df):
        df = super().clean_colnames(df)
        return df.melt(id_vars=['protein_ids', 'protein_names'], var_name='sample_id')


class WP5ProteomicsBatchData(Assay):
    name = 'wp5_proteomics_batch_data'
    description = 'Batch metadata for WP5 proteomics'

    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'
    report_coverage_as = 'wp5_proteomics'
    rename_columns = {
        'File.Name': 'file_name',
        'Batch': 'batch'
    }

    columns = (
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('kit_id', SampleID),
        Column('sample_id', SampleID),  # can't be a primary key - repeat control sample IDs
        Column('file_name', String),
        Column('unique_file', String, primary_key=True),
        Column('batch', String),
        Column('run_order', Integer),
        Column('plate_nr', Integer),
        Column('position', String),
        Column('run_order_all', Integer)
    )
