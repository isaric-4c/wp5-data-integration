import re
from pandas import DataFrame
from sqlalchemy import Column, String, Date
from data_linkage.config import cfg
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import PrimaryDataSource


class LIMSData(PrimaryDataSource):
    """Parser for the Liverpool LIMS data exports"""

    name = 'lims_data'
    description = 'LIMS export data detailing samples available, metadata and dispatch information'
    rename_columns = {
        'Sample_Code': 'sample_code',
        'Parent_sample': 'parent_sample',
        'Patient_ID': 'patient_id',
        'Kit_ID': 'kit_id',
        'Timepoint': 'timepoint',
        'Date_Stored': 'date_stored',
        'City': 'city',
        'Hospital': 'hospital',
        'Date_Collected': 'date_collected',
        'Kit_Component': 'kit_component',
        'Sample_Type': 'sample_type',
        'Label_Information': 'label_information',
        'Status': 'status',
        'Current_Location': 'current_location',
        'Shipping_Code': 'shipping_code',
    }

    columns = (
        Column('sample_code', SampleID, primary_key=True, comment='Sample barcode'),
        Column('parent_sample', SampleID),
        Column('patient_id', PatientID),
        Column('kit_id', SampleID),
        Column('timepoint', Timepoint),
        Column('date_stored', Date),
        Column('city', String, comment='City the sample was originally taken in'),
        Column('hospital', String, comment='Hospital site the sample was taken in'),
        Column('date_collected', Date),
        Column('kit_component', String, comment='Sample tube type'),
        Column('sample_type', String, comment='Sample tissue type'),
        Column(
            'clean_sample_type',
            String,
            comment='Clean sample type, based on rules from Hayley Hardwick applied to sample_type and kit_component'
        ),
        Column('label_information', String, comment='Any extra information about the sample'),
        Column(
            'status', String,
            comment="Either 'Central Lab' or 'Dispatched', depending on whether the sample has been dispatched"
        ),
        Column('current_location', String, comment='Dispatch location if dispatched, or freezer location if not'),
        Column('shipping_code', String, comment='Shipping ID for sample if dispatched'),
        Column(
            'dispatched_to_assay',
            String,
            comment='Which assay the sample has been assigned to - inferred from sample type and dispatch location'
        )
    )

    """
    Tuple (tuple[str,tuple[str]]) of clean sample types, and patterns to match on for that type. Compiled into regex
    objects in __init__. Different sample types are detected in order and the first matching pattern returns that clean
    type for the sample, which is why this isn't a dict.
    """
    type_patterns = (
        ('aliquoted_plasma', ('nunc.lav', '^aliquot.*plasma$')),
        ('aspirate', ('et.aspirate', 'ert.aspirate', 'endotracheal aspirate', '^eta.*eta$')),
        ('cell_pellet', ('cell pellet', 'cell fraction', 'nunc.r')),
        ('edta_blood', ('edta',)),
        ('blood', ('blood$', '^path.blood.*rna$')),
        ('citrated_plasma', ('citrate',)),
        ('coag_plasma', ('coag plasma',)),
        ('oracol', ('oracol', '^oracle - oral swab$')),
        ('oral_fluid', ('oral fl..d',)),
        ('pbmc', ('nunc.grn', '^lithium heparin.*pbmc$')),
        ('sam_strip', ('sam strip', 'nasal sam')),
        ('serum', ('serum', '^nunc.gld$')),
        ('clotted_tube', ('^clotted',)),
        ('sodium_heparin_plasma', ('nahep',)),
        ('sputum', ('sputum', 'saliva$')),
        ('stool', ('sterilin.*stool$', '^stool.tube.*faeces$', '^stool.tube.*stool$')),
        ('paxgene', ('pax.*gene$',)),
        ('tempus', ('tempus',)),
        ('urine', ('urine$', '^urine pot')),
        ('various_swabs', ('swab', 'rectal$', 'throat$', 'flocked', 'flu.*ox$', 'n.s.throat', 'throat.n.s', '^aliquot.*throat$', '^oral.*oral$')),
    )

    def __init__(self, db):
        super().__init__(db)
        self.assays = [
            DestinationAssay(k, v['sample_types'], v['dispatch_locations'])
            for k, v in cfg['sample_dispatch_parameters'].items()
        ]

        # Map of regex patterns to clean sample types, e.g. [re.compile('flu.*ox$', re.IGNORECASE), 'various_swabs']
        self.clean_sample_types = []
        for clean_sample_type, patterns in self.type_patterns:
            for p in patterns:
                self.clean_sample_types.append([re.compile(p, re.IGNORECASE), clean_sample_type])

    def output_clean_data(self):
        """
        Custom cleaning of the LIMS data using the linkage tables:
        - Basic cleaning and reformatting
        - Mapping from the kit ID to the clean patient ID

        :rtype: pandas.DataFrame
        """

        self.logger.info('Cleaning data for %s', self.source)
        df = self.init_dataframe()
        df = self.clean_dataframe(df)
        df = self._clean_patient_id(df, 'patient_id', 'kit_id')
        df = self.drop_unrelated_columns(df)
        return df

    def clean_datatypes(self, df):
        df = super().clean_datatypes(df)
        assigned_fields = DataFrame.from_records(
            tuple(self.assign_sample_fields(line) for line in df.itertuples()),
            columns=('dispatched_to_assay', 'clean_sample_type')
        )

        df['dispatched_to_assay'] = assigned_fields['dispatched_to_assay']
        df['clean_sample_type'] = assigned_fields['clean_sample_type']
        return df

    def assign_sample_fields(self, line):
        sample_type = line.kit_component + ' - ' + line.sample_type
        dispatch_location = line.current_location

        assigned_destination = self.assign_sample_dispatch(sample_type, dispatch_location)
        clean_sample_type = self.assign_clean_sample_type(sample_type)
        return assigned_destination, clean_sample_type

    def assign_sample_dispatch(self, sample_type, dispatch_location):
        """
        For a line in the table, decide what assays the sample could have been dispatched for, and if there is only one
        found then return it.
        :param str sample_type: Concatenated kit component and sample type
        :param str dispatch_location:
        """
        matches = [a.assay for a in self.assays if a.matches_sample(sample_type, dispatch_location)]
        if len(matches) == 1:
            return matches.pop()

    def assign_clean_sample_type(self, sample_type):
        """
        For a line in the table, assign a clean sample type.
        :param str sample_type: Concatenated kit component and sample type
        """
        for pattern, clean_sample_type in self.clean_sample_types:
            if pattern.search(sample_type):
                return clean_sample_type


class DestinationAssay:
    """
    Class for mapping sample types and dispatch locations to a known assay that a sample has been sent for. Checks a
    dispatched sample against self.sample_typpe_patterns and self.location_patterns and decides whether the sample has
    been dispatched to it in self.matches_sample().
    """
    def __init__(self, assay, sample_type_patterns, location_patterns):
        """
        :param str assay: The assay name
        :param sample_type_patterns: Regexes for sample types to look for
        :param location_patterns: Regex for dispatch locations to look for
        """
        self.assay = assay
        self.sample_type_patterns = [re.compile(s, re.IGNORECASE) for s in sample_type_patterns]
        self.location_patterns = [re.compile(l, re.IGNORECASE) for l in location_patterns]

    def matches_sample(self, sample_type, dispatch_location):
        """
        :param str sample_type:
        :param str dispatch_location:
        """
        return any(s.search(sample_type) for s in self.sample_type_patterns) and \
               any(d.search(dispatch_location) for d in self.location_patterns)
