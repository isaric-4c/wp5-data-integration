from sqlalchemy import Column, String, Integer, Float
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class WP4Biochemistry(Assay):
    """Parser for biochem-all.csv"""

    name = 'wp4_biochemistry'
    description = 'Biochemistry assay data from WP-4'
    dirty_sample_id_col = 'ISARIC sample ID'
    clean_kit_id_col = 'Kit ID'
    rename_columns = {'Timepoint (d)': 'Timepoint'}

    columns = (
        Column('ISARIC patient ID', PatientID),
        Column('Timepoint', Timepoint),
        Column('ISARIC sample ID', SampleID, primary_key=True, comment='LIMS barcode'),
        Column('Kit ID', SampleID, comment='Clean kit ID'),
        Column('RLH sample ID', String),
        Column('albumin', Integer),
        Column('iga', Float),
        Column('igg', Float),
        Column('igm', Float),
        Column('histones', Float),
        Column('cfdna', Float),
        Column('pct', Float),
        Column('amylase', Integer),
        Column('ast', Integer),
        Column('Hscore-AST', Integer),
        Column('ck', Integer),
        Column('crp', Integer),
        Column('ferritin', Integer),
        Column('Hscore-ferritin', Integer),
        Column('troponin', Integer),
        Column('ldh', Integer),
        Column('bnp', Integer),
        Column('trig', Float),
        Column('Hscore-trig', Integer),
        Column('Temp-hi', Float),
        Column('Hscore-temp', Integer),
        Column('Hb-lo', Integer),
        Column('Hscore-Hb', Integer),
        Column('WCC-lo', Float),
        Column('Hscore-wcc', Integer),
        Column('PLT-lo', Integer),
        Column('Hscore-PLT', Integer),
        Column('Hscore-cytopenia', Integer),
        Column('hscore', Integer)
    )
