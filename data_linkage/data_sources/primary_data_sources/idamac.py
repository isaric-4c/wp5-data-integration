from sqlalchemy import Column, Float, Date
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class IdamacComplement(Assay):
    name = 'idamac_complement'
    dirty_sample_id_col = 'kit_id'
    clean_kit_id_col = 'kit_id'
    description = 'Plasma complement results from Cardiff IDAMAC request'

    rename_columns = {
        'C1.inhibitor': 'C1 inhibitor',
        'Factor.D': 'Factor D',
        'Factor.H': 'Factor H',
        'Factor.I': 'Factor I'
    }

    columns = (
        Column('patient_id', PatientID, comment='ISARIC patient ID'),
        Column('kit_id', SampleID, comment='Clean kit ID'),
        Column('sample_collected', Date, comment='Locally recorded sample collection date'),
        Column('timepoint', Timepoint),
        Column('Ba', Float),
        Column('C1q', Float),
        Column('C3', Float),
        Column('C4', Float),
        Column('C5', Float),
        Column('C9', Float),
        Column('C1 inhibitor', Float),
        Column('Clusterin', Float),
        Column('Factor D', Float),
        Column('Factor H', Float),
        Column('Factor I', Float),
        Column('FHR125', Float),
        Column('FHR4', Float),
        Column('iC3b', Float),
        Column('Properdin', Float),
        Column('TCC', Float)
    )
