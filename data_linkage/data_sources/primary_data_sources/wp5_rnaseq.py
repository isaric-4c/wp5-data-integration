import os
import re
import csv
import pandas
from sqlalchemy import Column, String, Integer, Float, Date, Time, Boolean
from data_linkage import utils, cleaning
from data_linkage.config import cfg
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import PrimaryDataSource, Assay


class WP5RNASeqData(Assay):
    name = 'wp5_rnaseq_data'
    description = 'Locations of raw fastq RNA-seq data'
    dirty_sample_id_col = 'clean_rnaseq_sample_id'
    clean_kit_id_col = 'kit_id'

    report_coverage_as = 'wp5_rnaseq'
    rnaseq_dir = cfg['raw_data_sources']['wp5_rnaseq']

    columns = (
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('clean_rnaseq_sample_id', SampleID),
        Column('kit_id', SampleID),
        Column('run_id', String, comment='Sequencer run ID'),
        Column('r1_fastq', String),
        Column('r2_fastq', String)
    )

    def get_source(self):
        return ','.join(
            [
                d for d in sorted(os.listdir(f'{self.rnaseq_dir}/data'))
                if os.path.isdir(f'{self.rnaseq_dir}/data/{d}')
            ]
        )

    def init_dataframe(self):
        df = utils.list_files_to_dataframe(
            os.path.join(self.rnaseq_dir, 'data', '*', '*_R1_*.fastq.gz'),
            'r1_fastq'
        )
        df['r2_fastq'] = df['r1_fastq'].str.replace('_R1_', '_R2_')
        df['run_id'] = df['r1_fastq'].apply(lambda f: os.path.basename(os.path.dirname(f)))
        df['clean_rnaseq_sample_id'] = df['r1_fastq'].str.extract('([^\/]+)_S._L..._R._...\.fastq\.gz$')
        return df


class WP5RNASeqAnalysis(Assay):
    name = 'wp5_rnaseq_analysis'
    dirty_sample_id_col = 'clean_rnaseq_sample_id'
    clean_kit_id_col = 'kit_id'
    report_coverage_as = None

    description = ('Summary from scraping the output data from the rnaseq NF-Core pipeline. Gives paths to Salmon '
                   'output files, and raw/cleaned RNA-seq sample IDs.')

    columns = (
        Column('rnaseq_sample_id', SampleID, primary_key=True, comment='Raw RNA-seq sample ID - can be a kit ID or barcode, and may be marked as a repeat'),
        Column('clean_rnaseq_sample_id', SampleID, comment='As for rnaseq_sample_id but with any sequencer suffixes removed'),
        Column('kit_id', SampleID, comment='Clean kit ID'),
        Column('patient_id', PatientID, comment='Clean patient ID'),
        Column('timepoint', Timepoint),
        Column('sequencer_type', String, comment='Sequencing platform used, e.g. NS550, NS2000'),
        Column('run_id', String, comment='Run ID of the NF-Core rnaseq pipeline'),
        Column('gene_count_file', String, comment='Path to Salmon gene count file for this sample'),
        Column('multiqc_salmon_file', String, comment='Path to Salmon MultiQC data')
    )

    rnaseq_dir = cfg['raw_data_sources']['wp5_rnaseq']

    def __init__(self, db):
        with open(f'{self.rnaseq_dir}/analysis/rnaseq/exclude.txt') as f:
            self.ignore_dirs = [line.strip() for line in f]

        self.available_runs = self.rnaseq_processing_runs()
        super().__init__(db)

    def get_source(self):
        return ','.join(self.available_runs)

    def rnaseq_processing_runs(self):
        """
        List all RNA-seq processing pipelines run to date. This is not the same as RNA-seq sequencing runs, which
        contain fastqs.
        """
        return [
            d for d in sorted(os.listdir(f'{self.rnaseq_dir}/analysis/rnaseq'))
            if os.path.isdir(f'{self.rnaseq_dir}/analysis/rnaseq/{d}')
            and d not in self.ignore_dirs
        ]

    def list_samples_for_processing_run(self, analysis_id):
        # instrument_id = analysis_id.split('_')[1]
        salmon_file = f'{self.rnaseq_dir}/analysis/rnaseq/{analysis_id}/MultiQC/multiqc_data/multiqc_salmon.txt'
        with open(salmon_file, newline='') as f:
            reader = csv.DictReader(f, dialect='excel-tab')
            samples = [line['Sample'] for line in reader]

        return samples

    def init_dataframe(self):
        gene_count_files = []
        for a in self.available_runs:
            try:
                gene_count_files.extend(
                    utils.discover_files(f'{self.rnaseq_dir}/analysis/rnaseq/{a}/salmon/*_salmon_gene_counts.csv')
                )
            except FileNotFoundError:
                pass  # empty run

        df = pandas.DataFrame(data={'gene_count_file': gene_count_files})
        df['run_id'] = df['gene_count_file'].apply(lambda g: g.split('/')[-3])
        df[['run_date', 'sequencer_type']] = df['run_id'].str.split('_', expand=True)
        df['rnaseq_sample_id'] = df['gene_count_file'].str.extract('([^/]+)_salmon_gene_counts\.csv$', expand=False)
        df['clean_rnaseq_sample_id'] = df['rnaseq_sample_id'].str.replace('_NS2000_75$', '')
        df['multiqc_salmon_file'] = df['gene_count_file'].apply(self._get_multiqc_salmon_file)
        return df

    @staticmethod
    def _get_multiqc_salmon_file(salmon_file):
        f = os.path.join(
            os.path.dirname(os.path.dirname(salmon_file)),
            'MultiQC/multiqc_data/multiqc_salmon.txt'
        )
        if os.path.isfile(f):
            return f


class WP5RNASeqViralReadCounts(Assay):
    name = 'wp5_rnaseq_viral_read_counts'
    report_coverage_as = None
    dirty_sample_id_col = 'clean_rnaseq_sample_id'  # this makes no sense, but we need the _NS2000_75 removed before the actual cleaning
    clean_kit_id_col = 'kit_id'
    description = 'SARS-Cov2 read counts for WP5 RNA-seq samples'

    columns = (
        Column('patient_id', PatientID, comment='Clean patient ID'),
        Column('timepoint', Timepoint),
        Column('rnaseq_sample_id', SampleID, primary_key=True, comment='Original sample ID used in RNA-seq'),
        Column('clean_rnaseq_sample_id', SampleID, comment='As for rnaseq_sample_id but with any sequencer suffixes removed'),
        Column('kit_id', SampleID, comment='Clean ISARIC kit ID'),
        Column('sequencer_type', String, comment='Sequencing platform used, e.g. NS550, NS2000'),
        Column('read_count', Integer, comment='RNA-seq reads mapping uniquely to the SARS-Cov2 genome'),
        Column('bam_file', String),
        Column('vcf_file', String),
        Column('kraken_file', String)
    )

    def init_dataframe(self):
        df = utils.read_file(self.source, headerless=True)
        pipeline_run_dir = os.path.dirname(
            os.path.dirname(
                os.path.dirname(
                    cfg['raw_data_sources']['wp5_rnaseq_viral_read_counts']
                )
            )
        )

        tmp = df[0].str.split(' ', expand=True)
        df['sequencer_type'] = df[1]
        df['rnaseq_sample_id'] = tmp[0]
        df['read_count'] = tmp[1]
        df['clean_rnaseq_sample_id'] = df['rnaseq_sample_id'].str.replace('_NS2000_75$', '')

        for subdir, suffix, colname in (
            ('variants/bowtie2', '.sorted.bam', 'bam_file'),
            ('variants/bcftools', '.vcf.gz', 'vcf_file'),
            ('kraken2', '.kraken2.report.txt', 'kraken_file')
        ):
            _df = utils.list_files_to_dataframe(
                os.path.join(pipeline_run_dir, subdir, '*' + suffix),
                colname
            )
            _df['rnaseq_sample_id'] = _df[colname].str.extract('([^/]+)' + suffix, expand=False)

            df = df.merge(_df, how='left', on='rnaseq_sample_id')

        return df


class WP5RNASeqFilteredSamples(PrimaryDataSource):
    name = 'wp5_rnaseq_filtered_samples'
    description = ('RNA-seq samples that should not be used, for whatever reason. Lists sample ID, reason for '
                   'filtering out, who made the decision and when.')

    columns = (
        Column('rnaseq_sample_id', SampleID, primary_key=True),
        Column('name', String, comment='Researcher who marked this sample as unusable'),
        Column('date', Date),
        Column('reason', String)
    )


class WP5RNASeqCRFReport(Assay):
    name = 'wp5_rnaseq_crf_report'
    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'
    report_coverage_as = None
    description = 'CSV import of the Excel spreadsheet supplied by the CRF on RNA-seq processing status.'

    rename_columns = {
        'Patient ID': 'patient_id',
        'Date Taken': 'date_taken',
        'Sample ID': 'sample_id',
        'Day': 'day',
        'CRF Barcode': 'crf_barcode',
        'RNA Extraction Date': 'rna_extraction_date',
        'RNA Start Extraction Time': 'rna_start_extraction_time',
        'RNA Extraction Volume': 'rna_extraction_volume',
        'Kit Lot No': 'extraction_kit_lot_no',
        'Collection Tube': 'collection_tube',
        'Operator': 'extraction_operator',
        'Qubit BR RNA (ng/µL)': 'qubit_br_rna_ng/µl',
        'Qubit DNA HS (ng/µL)': 'qubit_dna_hs_ng/µl',
        'Qubit RNA:DNA (%)': 'qubit_rna_dna_percentage',
        'FA Conc (ng/µL)': 'fa_conc_ng/µl',
        'RQN': 'rqn',
        'DNAse Treatment': 'dnase_treatment',
        'Sample QC Pass/Fail': 'sample_qc_pass_fail',
        'RNA Volume to add (µL)': 'rna_volume_to_add_µl',
        'RNA Input (ng)': 'rna_input_ng',
        'Library Prep Start Date': 'library_prep_start_date',
        'Library Prep Finish Date': 'library_prep_finish_date',
        'Operator.1': 'library_prep_operator',
        'Kit Lot No.1': 'library_prep_kit_lot_no',
        'Plate ID': 'plate_id',
        'Well': 'well',
        'Index Plate': 'index_plate',
        'Index Well': 'index_well',
        'I7 Barcode': 'i7_barcode',
        'I5 Barcode': 'i5_barcode',
        'PCR cycles': 'pcr_cycles',
        'Library Conc (ng/µL)': 'library_conc_ng/µl',
        'Peak Fragment Size (bp)': 'peak_fragment_size_bp',
        'Library Molarity (nM)': 'library_molarity_nm',
        'Sequenced?': 'sequenced',
        'Sequence Date': 'sequence_date',
        'Platform': 'platform',
        'Run ID': 'run_id',
        'Number of Clusters PF': 'number_of_clusters_pf'
    }

    columns = (
        Column('patient_id', PatientID),
        Column('date_taken', Date, comment='Sample date taken - probable duplicate of lims_data."Date_Collected"'),
        Column('sample_id', SampleID, primary_key=True, comment='Raw RNA-seq sample ID'),
        Column('kit_id', SampleID, comment='Clean kit ID'),
        Column('day', Timepoint, comment='Sampling timepoint'),
        Column('crf_barcode', String),  # or int?
        Column('rna_extraction_date', Date),
        Column('rna_start_extraction_time', Time),
        Column('rna_extraction_volume', Float),
        Column('extraction_kit_lot_no', String),  # or int? Renamed from `Kit Lot No` - duplicate colname
        Column(
            'collection_tube',
            String,
            comment='This collection tube name is more accurate than the LIMS sample type - so use this where possible'
        ),
        Column('extraction_operator', String),  # renamed from `Operator` - duplicate colname
        Column('qubit_br_rna_ng/µl', Float),
        Column('qubit_dna_hs_ng/µl', Float),
        Column('qubit_rna_dna_percentage', Float),
        Column('fa_conc_ng/µl', Float),
        Column('rqn', Float),
        Column('dnase_treatment', String),
        Column('sample_qc_pass_fail', String),
        Column('rna_volume_to_add_µl', Float),
        Column('rna_input_ng', Integer),
        Column('library_prep_start_date', Date),
        Column('library_prep_finish_date', Date),
        Column('library_prep_operator', String),  # renamed from `Operator` - duplicate colname
        Column('library_prep_kit_lot_no', String),  # or int? Renamed from `Kit Lot No` - duplicate colname
        Column('plate_id', String),
        Column('well', String),
        Column('index_plate', String),
        Column('index_well', String),
        Column('i7_barcode', String),
        Column('i5_barcode', String),
        Column('pcr_cycles', Integer),
        Column('library_conc_ng/µl', Float),
        Column('peak_fragment_size_bp', Integer),
        Column('library_molarity_nm', Integer),
        Column('sequenced', Boolean),
        Column('sequence_date', Date),
        Column('platform', String),
        Column('run_id', String),
        Column('number_of_clusters_pf', Integer)
    )

    """Cleaning regex used to remove dots and commas from large integers"""
    clusters_pattern = re.compile(r'[.,]')

    def clean_datatypes(self, df):
        """Remove 'LV***' control samples and reformat some numbers and IDs."""
        df = super().clean_datatypes(df)
        df = df[~df['sample_id'].astype(str).str.startswith('LV')].copy()

        for col in ('crf_barcode', 'extraction_kit_lot_no', 'library_prep_kit_lot_no'):
            df[col] = cleaning.clean_numeric_id(df[col])

        df['number_of_clusters_pf'] = df['number_of_clusters_pf'].apply(self._clean_clusters_pf)
        return df

    @classmethod
    def _clean_clusters_pf(cls, cell):
        """Remove , and . from large integer numbers."""
        if isinstance(cell, int):
            return cell

        elif isinstance(cell, str):
            _cell = cls.clusters_pattern.sub('', cell)
            if _cell.isnumeric():
                return int(_cell)


class WP5RNASeqPseudotimes(Assay):
    name = 'wp5_rnaseq_pseudotimes'
    description = 'Calculated pseudotimes and clustering for RNA-seq samples, from the SCORPIUS R package'
    dirty_sample_id_col = 'clean_rnaseq_sample_id'
    clean_kit_id_col = 'kit_id'
    report_coverage_as = None

    columns = (
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('clean_rnaseq_sample_id', SampleID, comment='RNA-seq sample ID'),
        Column('kit_id', SampleID, comment='Clean kit ID'),
        Column('pseudotime', Float, comment='Estimation of where the patient is in the progression of their illness'),
        Column('cluster', String, comment='Calculated by Kristian Kalinak')
    )
