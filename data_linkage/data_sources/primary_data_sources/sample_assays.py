import pandas
from sqlalchemy import Column, String
from data_linkage import __version__
from . import PrimaryDataSource


class SampleAssays(PrimaryDataSource):
    name = 'sample_assays'
    description = ('Enumeration of all assays in the data linkage, known IDAMAC requests and other sample dispatch '
                   'destinations')
    columns = (
        Column('name', String, primary_key=True),
    )

    idamac_requests = [
        'idamac_request_stool',
        'idamac_request_urine_metabolomics'
    ]

    """Assays that are part of the ISARIC datasets, where we want to check for coverage"""
    # To do: make this dynamic once we're creating DataSource objects at run time
    data_linkage_assays = [
        'wp3_blood_viral_load',
        'wp4_biochemistry',
        'wp5_rnaseq',
        'wp5_wgs',
        'wp5_proteomics',
        'wp6_anticytokineantibodies_iga',
        'wp6_anticytokineantibodies_igg',
        'wp6_plasma_cytokines',
        'wp6_faecal_cytokines',
        'wp6_nasosorption_mediators',
        'wp7_serology_serum',
        'wp7_serology_oral_fluids',
        'wp7_serology_neutralisation'
    ]

    """Other assays that we don't have data for but still want to know about"""
    other_assays = [
        'wp3_viralseq_eta_16s',
        'wp3_viralseq_urt_16s',
        'wp4_biochemistry_vitd_steroids',
        'wp6_ngs_serology'
    ]

    def get_source(self):
        """Special case - we don't want to add this to the index table"""
        return None

    def init_dataframe(self):
        return pandas.DataFrame(data={'name': self.idamac_requests + self.data_linkage_assays + self.other_assays})


class Index(PrimaryDataSource):
    name = 'index'
    description = 'Release metadata, including build version and data sources for each assay'
    columns = (
        Column('name', String, primary_key=True, comment='Data source name'),
        Column('value', String, comment='What file, set of RNA-seq runs, etc. supplied the data')
    )

    def __init__(self, db):
        super().__init__(db)
        self.version_information = []

    def add_version_info(self, release_name, release_number):
        """
        Add versions of the release and pipeline to index table. Called in data_linkage.release before
        self.load_sql_table().
        :param str release_name:
        :param int release_number:
        """
        self.version_information = [
            ['automation_version', __version__],
            ['release_name', release_name],
            ['release_number', release_number]
        ]

    def get_source(self):
        """Special case - the index table shouldn't add itself"""
        return None

    def init_dataframe(self):
        """
        Dynamically populate raw data from all the other data sources.
        """
        return pandas.DataFrame(
            [
                [ds.name, ds.source]
                for ds in self.db.primary_data_sources.values()
                if ds.name != 'index'
                and ds.source is not None
            ] + self.version_information,
            columns=('name', 'value')
        )
