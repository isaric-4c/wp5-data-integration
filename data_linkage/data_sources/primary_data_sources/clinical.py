import re
import csv
import pandas
from collections import OrderedDict
from sqlalchemy import Column, String, Float, Integer, BigInteger, Date, Time, Boolean
from data_linkage.cleaning import PatientID
from . import PrimaryDataSource


def date_diff(left, right):
    """Diffing two dates by number of days in Pandas."""
    if not left or not right:
        return None

    return (left - right).days


class ClinicalDataSource(PrimaryDataSource):
    """Base class for REDCap exports that implements inferred schemas."""

    """Whether the dataset is one line per patient, i.e. the subjid should be a primary key"""
    one_line_per_patient = False

    """
    Whether to add severity scoring columns 'severity_score' and 'severity_indication'. These columns should be populated
    in self.clean_dataframe.
    """
    severity_scoring = False

    def __init__(self, db):
        self._columns = None
        super().__init__(db)

    @property
    def columns(self):
        if self._columns is None:
            self._columns = self.get_columns()

        return self._columns

    def get_columns(self):
        """
        Parse self.file and infer what data type each column should be. We have to do this because the clinical files
        have ~900 columns each and they change with every new export - this needs to stabilise if we want an explicit
        schema. Currently pure-Python, but can maybe be done in Pandas?
        """

        null_pattern = re.compile(r'^(NA|N/A|)$')
        integer_pattern = re.compile(r'^[0-9]+$')  # 1, 245
        float_pattern = re.compile(r'^[0-9]+\.[0-9]+$')  # 0.124, 1.0
        date_ymd_pattern1 = re.compile(r'^[0-9]{4}[-/][0-9]{2}[-/][0-9]{2}$')  # 2020-12-14, 2020/12/14
        date_ymd_pattern2 = re.compile(
            # 20201214 - some extra validation is done below on this to avoid misparsing, e.g, 14122020
            r'^[0-9]{4}[0-9]{2}[0-9]{2}$'
        )
        time_pattern = re.compile(r'^[0-9]{2}:[0-9]{2}(|:[0-9]{2})$')  # 10:30:00, 09:45
        bool_pattern = re.compile(r'^(yes|no|true|false|y|n)$', re.IGNORECASE)  # yes, Yes, YES, True, TRUE, Y, ...

        with open(self.source) as f:
            reader = csv.reader(f)
            # build a set of possible data types that the column could be
            cols = OrderedDict([(c, set()) for c in next(reader)])

            for line in reader:
                for col, cell in zip(cols, line):
                    if null_pattern.match(cell):
                        pass

                    elif date_ymd_pattern1.match(cell):
                        cols[col].add(Date)

                    elif date_ymd_pattern2.match(cell) and 1 <= int(cell[4:6]) <= 12 and 1 <= int(cell[6:8]) <= 31:
                        cols[col].add(Date)

                    elif time_pattern.match(cell):
                        cols[col].add(Time)

                    elif integer_pattern.match(cell):
                        # notes on numeric types: https://www.postgresql.org/docs/9.1/datatype-numeric.html
                        if int(cell) > 2147483647:
                            cols[col].add(BigInteger)
                        else:
                            cols[col].add(Integer)

                    elif float_pattern.match(cell):
                        cols[col].add(Float)

                    elif bool_pattern.match(cell):
                        cols[col].add(Boolean)

                    else:
                        cols[col].add(String)

            coltypes = OrderedDict()
            for c, types in cols.items():
                if not c:  # e.g. index column with empty name
                    continue

                if len(types) == 1:  # one possible type, so use that
                    t = types.pop()
                # treat a mix of ints and floats as a float column - Tidyverse casts floats ending in `.0` to ints
                # in upstream data -.-
                elif types == {Integer, Float}:
                    t = Float

                # if there are any large ints, use large integer SQL type
                elif types == {Integer, BigInteger}:
                    t = BigInteger

                else:
                    # default to string if multiple possible types - otherwise it is from the last iteration, and the
                    # coltype will be assigned the same value as the previous columns
                    t = String

                coltypes[c] = t

        # decide whether to set a primary key on subjid
        assert list(coltypes)[0] == 'subjid'
        coltypes['subjid'].type = PatientID
        if self.one_line_per_patient:
            coltypes['subjid'].primary_key = True

        return [Column(k, v) for k, v in coltypes.items()]

    @classmethod
    def severity_score(cls, row):
        pass

    def clean_dataframe(self, df):
        df = super().clean_dataframe(df)
        if self.severity_scoring:
            assert 'severity_score' not in df
            assert 'severity_indication' not in df

            severities = df.apply(
                lambda row: pandas.Series(self.severity_score(row), index=('severity_score', 'severity_indication')),
                axis='columns'
            )
            df = pandas.concat((df, severities), axis='columns')

        return df


class ClinicalData(ClinicalDataSource):
    name = 'clinical_data'
    severity_scoring = True

    @property
    def description(self):
        return (f'The full ccp_data.csv export, containing all recorded clinical observations. {len(self.columns)} '
                'columns. Includes daily severity scores.')

    def get_columns(self):
        columns = super().get_columns()
        columns.append(
            Column(
                'severity_score', Integer,
                comment="Daily severity score: dsterm == 'Death' or 'Palliative discharge' -> 5, daily_invasive_prtrt "
                        "true or invasive_proccur true -> 4, daily_noninvasive_prtrt true or noninvasive_proccur true "
                        "-> 3, daily_fio2_combined > 0.21 or oxy_vsorresu == 'Oxygen therapy' -> 2, "
                        "daily_fio2_combined == 0.21 or oxy_vsorresu == 'Room air' or (daily_invasive_prtrt false and "
                        "daily_noninvasive_prtrt false) -> 1, else -> null"
            )
        )
        columns.append(Column('severity_indication', String, comment='Severity scoring notes'))
        return columns

    @classmethod
    def severity_score(cls, row):
        """
        Apply severity scoring on daily clinical data. This should not be relied on alone, due to the patchiness of the
        daily clinical data.
        """

        # death/palliative discharge - can't compare with cestdat/hostdat because they're not populated in subsequent
        # events
        if row['dsterm'] in ('Death', 'Palliative discharge'):
            return 5, 'Death/palliative discharge'

        # invasive mechanical ventilation
        elif row['daily_invasive_prtrt'] is True:
            return 4, 'Invasive mechanical ventilation'

        # noninvasive mechanical ventilation
        elif row['daily_noninvasive_prtrt'] is True:  # and row['daily_invasive_prtrt'] is False:
            return 3, 'Noninvasive ventilation'

        # oxygen therapy
        elif round(row['daily_fio2_combined'], 2) > 0.21 or row['oxy_vsorresu'] == 'Oxygen therapy':
            return 2, 'Oxygen therapy'

        # ward oxygen
        elif (
            (round(row['daily_fio2_combined'], 2) == 0.21 or pandas.isna(row['daily_fio2_combined']))
            and (row['oxy_vsorresu'] == 'Room air' or pandas.isna(row['oxy_vsorresu']))
            and (row['daily_invasive_prtrt'] is False or pandas.isna(row['daily_invasive_prtrt']))
            and (row['daily_noninvasive_prtrt'] is False or pandas.isna(row['daily_noninvasive_prtrt']))
            # and (row['daily_nasaloxy_cmtrt'] is False or pandas.isna(row['daily_nasaloxy_cmtrt']))
        ):
            return 1, 'No oxygen requirement'

        else:
            return None, None


class ClinicalTopline(ClinicalDataSource):
    name = 'clinical_topline'
    one_line_per_patient = True

    @property
    def description(self):
        return ('The topline.csv REDCap export, containing admission information and initial observations per patient. '
                f'{len(self.columns)} columns.')


class ClinicalOneline(ClinicalDataSource):
    name = 'clinical_oneline'
    one_line_per_patient = True
    severity_scoring = True

    @property
    def description(self):
        return (f'The oneline.csv REDCap export, containing summary information per patient. {len(self.columns)} '
                'columns. Supersedes the old topline, treatment and outcome export objects. Includes peak severity '
                'scores.')

    def get_columns(self):
        columns = super().get_columns()
        columns.append(
            Column(
                'severity_score', Integer,
                comment="Patient severity score: dsterm == 'Death' or 'Palliative discharge' and ((hostdat->dsstdtc < "
                        "28 days) or (hostdat->dsstdtc >27 days and hostdat->cestdat >7 days and cestdat->dsstdtc <28 "
                        "days)) -> 5,  any_invasive true -> 4, any_noninvasive true -> 3, any_oxygen true or "
                        "any_daily_fio2_21 true or oxy_vsorresu == 'Oxygen therapy' -> 2, any_daily_fio2_21 false or "
                        "oxy_vsorresu == 'Room air' or (any_daily_invasive_prtrt false and any_daily_noninvasive_prtrt "
                        "false -> 1, else -> null"
            )
        )
        columns.append(Column('severity_indication', String, comment='Severity scoring notes'))
        return columns

    @classmethod
    def severity_score(cls, row):
        """Apply overall severity scoring for the patient's entire stay."""

        # unknown outcome
        if row['dsterm'] == 'Unknown' or pandas.isna(row['dsterm']):
            return None, None

        # death/palliative discharge...
        elif row['dsterm'] in ('Death', 'Palliative discharge'):
            if row['dsstdtc'] and row['hostdat']:

                # ...within 28 days of admission
                if date_diff(row['dsstdtc'], row['hostdat']) < 28:
                    return 5, 'Death/palliative discharge within 28 days of admission'

                # ... within 28 days of nosocomial infection, i.e. after 28 days in hospital, onset > 7 days after
                # admission
                if row['cestdat'] and \
                        date_diff(row['dsstdtc'], row['hostdat']) > 27 and \
                        date_diff(row['cestdat'], row['hostdat']) > 7 and \
                        date_diff(row['dsstdtc'], row['cestdat']) < 28:
                    return 5, 'Death/palliative discharge within 28 days of nosocomial infection'

        # invasive mechanical ventilation
        if row['any_invasive'] is True:
            return 4, 'Invasive mechanical ventilation'

        # noninvasive mechanical ventilation
        elif row['any_noninvasive'] is True:  # and row['any_invasive'] is False:
            return 3, 'Noninvasive ventilation'

        # oxygen therapy
        elif row['any_oxygen'] is True or row['oxy_vsorresu'] == 'Oxygen therapy':
            return 2, 'Oxygen therapy'

        # ward oxygen - no FiO2 over 0.21
        elif ((row['any_oxygen'] is False or pandas.isna(row['any_oxygen']))
            and (row['oxy_vsorresu'] == 'Room air' or pandas.isna(row['oxy_vsorresu']))
            and (row['any_invasive'] is False or pandas.isna(row['any_invasive']))
            and (row['any_noninvasive'] is False or pandas.isna(row['any_noninvasive']))):
            return 1, 'No oxygen requirement'

        elif row['any_daily_invasive_prtrt'] is False and row['any_daily_noninvasive_prtrt'] is False:
            return 1, 'No oxygen requirement'

        else:
            return None, None
