from sqlalchemy import Column, Float
from data_linkage.cleaning import PatientID
from . import PrimaryDataSource


class WP5ClinicalImputation(PrimaryDataSource):  # not an Assay because this dataset is not sample-centric
    name = 'wp5_clinical_imputation'
    description = 'Patient oxygen measurements for WP5, imputed from WP4 clinical data'
    columns = (
        Column('subjid', PatientID, primary_key=True),
        Column('sfr', Float, comment='sao2/fio2 ratio - calculated from oxygen saturation and fraction of inspired oxygen'),
        Column('sf94', Float, comment='sfr if sao2 <= 0.94 or fio2 == 0.21, otherwise null')
    )
