from sqlalchemy import Column, String, Float, Date
from data_linkage.cleaning import PatientID, SampleID, Timepoint
from . import Assay


class WP6NasosorptionMediators(Assay):
    name = 'wp6_nasosorption_mediators'
    description = 'Nasosorption mediator data for WP-6'

    dirty_sample_id_col = 'dirty_sample_id'
    clean_kit_id_col = 'kit_id'

    rename_columns = {
        'plateID': 'plate_id',
        'donor': 'patient_id',
        'time': 'timepoint',
        'Kit_ID': 'kit_id',
        'date': 'date_collected'
    }

    columns = (
        Column('plate_id', String, primary_key=True),
        Column('patient_id', PatientID),
        Column('timepoint', Timepoint),
        Column('barcode', SampleID),
        Column('kit_id', SampleID),
        Column('date_collected', Date),
        Column('Angiopoietin-1', Float),
        Column('Angiopoietin-2', Float),
        Column('APRIL', Float),
        Column('CCL1', Float),
        Column('CCL2', Float),
        Column('CCL3', Float),
        Column('CCL4', Float),
        Column('CCL5', Float),
        Column('CCL11', Float),
        Column('CCL15', Float),
        Column('CCL22', Float),
        Column('CD30', Float),
        Column('Coag Factor XIV', Float),
        Column('CXCL4', Float),
        Column('CXCL10', Float),
        Column('CXCL13', Float),
        Column('D-dimer', Float),
        Column('Endothelin-1', Float),
        Column('EN-RAGE', Float),
        Column('FGF basic', Float),
        Column('G-CSF', Float),
        Column('GDF-15', Float),
        Column('GM-CSF', Float),
        Column('IL-1a', Float),
        Column('IL-5', Float),
        Column('IL-6R alpha', Float),
        Column('IL-7', Float),
        Column('IL-15', Float),
        Column('IL-17', Float),
        Column('IL-18', Float),
        Column('Kallikrein 3', Float),
        Column('Kallikrein 5', Float),
        Column('Kallikrein 6', Float),
        Column('Lipocalin-2', Float),
        Column('MMP-1', Float),
        Column('Osteopontin', Float),
        Column('PDGF-BB', Float),
        Column('Pentraxin 3', Float),
        Column('Thrombomodulin', Float),
        Column('Tie-2', Float),
        Column('Tissue Factor', Float),
        Column('TNF RI', Float),
        Column('VEGF', Float),
        Column('VEGF-D', Float),
        Column('vWF-A2', Float),
        Column('IFN-g', Float),
        Column('IL-10', Float),
        Column('IL-12p70', Float),
        Column('IL-13', Float),
        Column('IL-1b', Float),
        Column('IL-2', Float),
        Column('IL-4', Float),
        Column('IL-6', Float),
        Column('IL-8', Float),
        Column('TNF-a', Float)
    )

    def _clean_sample_ids(self, df, clean_kit_col, dirty_sample_col):
        df['dirty_sample_id'] = df['kit_id'].copy()
        df['dirty_sample_id'].update(df['barcode'].where(df['barcode'].notnull()))
        return super()._clean_sample_ids(df, clean_kit_col, dirty_sample_col)
