import os
import pandas
import sqlalchemy
from data_linkage import utils, cleaning
from data_linkage.config import cfg
from data_linkage.exceptions import InvalidDataException
from data_linkage.data_sources.common import DataSource


class PrimaryDataSource(DataSource):
    """
    A Table to be automatically populated from input data. Creates a pandas DataFrame and defines methods for cleaning
    it before being populated into self.table with DataFrame.to_sql in ReleaseManager.

    Subclassing:
    Use this class to define a table to be added to the linkage database. The dataset may already be clean and only need
    type casting before inserting to SQL, or it may have sample/patient columns that need linkages cleaned using the
    linkage tables and the LIMS.

    Subclasses should define the attributes `name`, `description`, `columns`, and optionally
    `rename_columns`. If there is a sample ID column that needs cleaned, define it as dirty_sample_id_col, and set the
    resulting clean kit ID column as clean_kit_id_col. Each of these will need a corresponding Column in cls.columns.
    The linkage cleaning process will add the patient, clean kit and timepoint columns if not present in the input data.

    The DataSource automatically infers the patient ID and timepoint from the data types of self.columns. At the moment,
    it is not possible to define multiple patient or timepoint columns.

    If custom cleaning and reparsing of columns is required, then it is possible to override clean_dataframe - just be
    sure to call super().clean_dataframe.
    """

    """
    SQLAlchemy Column objects, containing names, data types and primary key information. Use the custom data types in
    cleaning.py to define sample/patient IDs and timepoints.
    """
    columns: tuple = ()

    """
    Raw data columns that should be renamed in the clean data. Necessary because database engines don't like certain
    characters in column names, and sometimes input files have inconsistent column names.
    """
    rename_columns: dict = {}

    """What the clean kit ID will be called in the cleaned dataset. Set this to activate cleaning of ID columns"""
    clean_kit_id_col: str = None

    """
    Which column to use to get the clean kit ID, if applicable. This could be kit IDs, LIMS barcodes, or an unholy
    maelstrom of both, as often happens in raw datasets.
    """
    dirty_sample_id_col: str = None

    def __init__(self, db):
        super().__init__(db)
        self.linker = None
        self.source = self.get_source()

        patient_cols = [col for col in self.columns if isinstance(col.type, cleaning.PatientID)]
        if not patient_cols:
            self.patient_id_col = None
        elif len(patient_cols) == 1:
            self.patient_id_col = patient_cols[0].name
        else:
            raise InvalidDataException('Multiple patient columns defined for %s', self.name)

        timepoint_cols = [col for col in self.columns if isinstance(col.type, cleaning.Timepoint)]
        if not timepoint_cols:
            self.timepoint_col = None
        elif len(timepoint_cols) == 1:
            self.timepoint_col = timepoint_cols[0].name
        else:
            raise InvalidDataException('Multiple patient columns defined for %s', self.name)

    def clean_colnames(self, df):
        """
        Trim whitespace from column names and apply any defined column renamings.
        :param pandas.DataFrame df:
        :rtype: pandas.DataFrame
        """
        old_colnames = df.columns

        # sometimes column names have trailing spaces
        whitespace_trimming = {}
        for colname in df.columns:
            if not isinstance(colname, str):  # int colnames from headerless files
                continue

            trimmed_name = colname.strip()
            if colname != trimmed_name:
                whitespace_trimming[colname] = trimmed_name

        if whitespace_trimming:
            df.rename(columns=whitespace_trimming, inplace=True)

        # after this, df.columns should == self.columns...
        df.rename(columns=self.rename_columns, inplace=True)

        new_colnames = df.columns
        self.logger.debug('Renamed columns: %s -> %s', old_colnames, new_colnames)
        return df

    def clean_datatypes(self, df):
        """
        Apply basic cleaning and reformatting based on column type.
        :param pandas.DataFrame df:
        :rtype: pandas.DataFrame
        """
        for col in self.columns:
            cleaning_func = cleaning.generic_cleaning_funcs.get(
                col.type.__class__,  # for custom column types, e.g. PatientID
                cleaning.generic_cleaning_funcs.get(col.type.python_type)  # for vanilla SQLAlchemy types, e.g. String, Integer
            )

            # ... unless the column doesn't exist yet because it will be added by the Linker
            if cleaning_func and col.name in df:
                self.logger.debug('Cleaning column %s with %s', col.name, cleaning_func.__name__)
                df[col.name] = cleaning_func(df[col.name])

        return df

    def clean_dataframe(self, df):
        """
        :param pandas.DataFrame df:
        :rtype: pandas.DataFrame
        """
        df = self.clean_colnames(df)
        df = self.clean_datatypes(df)
        return df

    def _init_table(self):
        return sqlalchemy.Table(self.name, self.db.meta, *[col.copy() for col in self.columns], comment=self.description)

    def get_source(self):
        """
        Deterministic string representation of the source of the data. Default is the latest (read: alphabetically last)
        file name based on cfg['raw_data_sources'][self.name]. Override if the dataset instead has a comma-separated
        list of files, list of RNA-seq runs, etc.
        :rtype: str
        """
        return utils.discover_files(os.path.abspath(cfg['raw_data_sources'][self.name]))[-1]

    def init_dataframe(self):
        """
        Create a pandas.DataFrame from the raw input data, to be cleaned and imported. Default behaviour is to read a
        single file based on self.source - override if the raw data needs to come from multiple files or scraping a
        filesystem.
        :rtype: pandas.DataFrame
        """
        return utils.read_file(self.source)

    def register_linker(self, linker):
        """
        :param data_linkage.utils.Linker linker:
        """
        self.linker = linker

    def output_clean_data(self):
        """
        Take the determined raw data and output a clean Pandas data frame that can be inserted into self.table with
        DataFrame.to_sql. If sample ID columns are defined, then also:

        - Account for sample repeats and ID remappings in self.dirty_sample_id_col, e.g. 'CCP0001-R' -> 'CCP0001'
        - Use the LIMS to clean self.dirty_sample_id_col into self.clean_id_ccol
          - Remaps full Liverpool barcodes (SM2004012345), full Glasgow barcodes (SMG012345), short Glasgow
            barcodes (012345) and Excel-ed short Glasgow barcodes (12345) into kit IDs (see utils.Linker)
        - Map from the clean kit ID to the patient and timepoint
          - Or if there's no sample ID and there is a patient/timepoint, try to map in the opposite direction

        :rtype: pandas.DataFrame
        """
        self.logger.info('Cleaning data for %s', self.source)
        df = self.init_dataframe()
        df = self.clean_dataframe(df)

        if self.clean_kit_id_col:
            if self.dirty_sample_id_col:
                df = self._clean_sample_ids(df, self.dirty_sample_id_col, self.clean_kit_id_col)  # adds clean kit ID

            if self.patient_id_col:
                df = self._clean_patient_id(df, self.patient_id_col, self.clean_kit_id_col)

            if self.patient_id_col and self.timepoint_col:
                df = self._clean_timepoints(df, self.clean_kit_id_col, self.timepoint_col)
                df = self._clean_leftover_kits(df, self.patient_id_col, self.timepoint_col, self.clean_kit_id_col)

        df = self.drop_unrelated_columns(df)
        self.logger.info('Cleaning done')
        return df

    def drop_unrelated_columns(self, df):
        """Reorder df.columns, drop any that aren't in the schema and drop any null primary keys."""
        df = df[[col.name for col in self.columns]].copy()
        primary_key_cols = [col.name for col in self.columns if col.primary_key]
        if primary_key_cols:
            df.dropna(subset=primary_key_cols, inplace=True)

        return df

    def load_data(self):
        """
        Load the results of self.output_clean_data() into a database
        """
        df = self.output_clean_data()
        with self.db.connect() as connection:
            self.logger.info('Importing to SQL')
            df.to_sql(self.name, connection, schema=self.db.meta.schema, if_exists='append', index=False)
            self.logger.info('SQL import done')

        return df

    def _join_dataframe(self, left, right, left_on, right_on, cleaning_colname):
        """
        Wrap pandas.DataFrame.merge() to do a left outer join between two data frames and return the desired column to
        use for cleaning.
        """
        # can't use one_to_one relationship because there may be multiple null values in left[left_on]
        joined_df = left.merge(right, how='left', left_on=left_on, right_on=right_on, validate='many_to_one')
        if cleaning_colname in joined_df:
            return joined_df[cleaning_colname]
        elif cleaning_colname + '_y' in joined_df:  # col conflicts in left and right -> use the right
            return joined_df[cleaning_colname + '_y']
        else:
            raise InvalidDataException(f'{self.name}: could not find column {cleaning_colname} when joining {left_on} -> {right_on}')

    @staticmethod
    def _merge_joined_data(left_column, right_column, conservative=False):
        """
        Override a data frame column using non-null values from another column:

        left_column  right_column -> result
        this         other           other
        None         another         another
        that         None            that

        :param pandas.Series left_column: The column to update
        :param pandas.Series right_column: The column to add data from
        :param bool conservative: Only update if left_column's value is null - this would make the result column above
                                  become ['this', 'another', 'that']
        """
        cleaning_conditions = right_column.notnull()  # there is clean data in the other table for this row
        if conservative:
            cleaning_conditions &= left_column.isnull()  # and there is no data pre-existing

        left_column.update(
            right_column.where(cleaning_conditions)
        )

    def _clean_patient_id(self, df, patient_col, clean_kit_col):
        """
        Use the clean kit ID to get clean patient IDs from the linkage table, falling back on the LIMS, e.g:

        assay                        linkage_table            lims
        sample_id     clean_kit_id   sample_id  patient_id    Kit_ID   Patient_ID
        SM20040123    CCP0001  --->  CCP0001    ABCDE-0002    ...
        012345
        012346-R      CVRI001   -------------------------->   CVRI001  ABCDE-0001

        :param pandas DataFrame df:
        :param str patient_col: The patient ID column in df to be cleaned
        :param str clean_kit_col: The clean kit ID column to be joined to the linkage/LIMS tables in order to get the
                                  clean patient ID
        """

        self.logger.info(
            'Cleaning patient ID column %s with kit ID column %s',
            patient_col,
            clean_kit_col
        )

        if patient_col not in df:
            df[patient_col] = pandas.Series(dtype=str)

        self._merge_joined_data(
            df[patient_col],
            self._join_dataframe(
                df, self.linker.samples_to_patients, clean_kit_col, 'sample_id', 'canonical_isaric_id'
            )
        )
        return df

    def _clean_sample_ids(self, df, dirty_sample_col, clean_kit_col):
        """
        Use the LIMS to clean dirty_sample_id_col into clean_kit_id_col
        :param pandas DataFrame df:
        :param str dirty_sample_col: The sample ID column to be checked against the LIMS
        :param str clean_kit_col: What to create the clean kit column in df as
        """

        self.logger.info(
            'Cleaning kit ID column %s with dirty sample ID column %s',
            clean_kit_col,
            dirty_sample_col
        )

        # start with dirty sample IDs, e.g:
        # dirty_sample_id
        #        012345-R
        #          012346
        #         CCP0001
        #     CCPBATCH001
        #      SM20040123
        tmp = pandas.DataFrame()
        tmp['dirty_sample_id'] = df[dirty_sample_col]

        # correct for any kit-kit/barcode-barcode remappings - we now have a mix of valid barcodes and kits, e.g:
        # dirty_sample_id
        #          012345
        #          012346
        #         CCP0001
        #    CCPNBATCH001
        #      SM20040123
        self._merge_joined_data(
            tmp['dirty_sample_id'],
            self._join_dataframe(
                tmp, self.linker.samples_to_samples, 'dirty_sample_id', 'sample_id', 'isaric_sample_id'
            )
        )

        # clean dirty barcodes into kit IDs, e.g:
        # dirty_sample_id  Kit_ID
        #          012345  CVRI001
        #          012346
        #         CCP0001
        #    CCPNBATCH001
        #      SM20040123  CCP0001
        tmp['_kit_id'] = self._join_dataframe(
            tmp, self.linker.lims_barcodes_to_kits, 'dirty_sample_id', 'barcode', 'clean_kit_id'
        )

        # fill in any IDs from dirty_sample_id that are actually kits into Kit_ID, e.g:
        # dirty_sample_id  Kit_ID
        #          012345  CVRI001
        #          012346
        #         CCP0001  CCP0001
        #    CCPNBATCH001  CCPBATCH001
        #      SM20040123  CCP0001
        # we should now have a clean kit ID unless the barcode is unlinkable
        self._merge_joined_data(
            tmp['_kit_id'],
            self._join_dataframe(
                tmp, self.linker.lims_kits['kit_id'], 'dirty_sample_id', 'kit_id', 'kit_id'
            )
        )

        df[clean_kit_col] = tmp['_kit_id']
        return df

    def _clean_timepoints(self, df, clean_kit_col, timepoint_col):
        """
        Use LIMS data and clean kit IDs to fill in timepoints, e.g:

        assay                        lims
        sample_id   clean_kit_id     Kit_ID   patient_id  timepoint
        SM20040123  CCP0001  ----->  CCP0001  ABCDE-0002  Day 1
        012345
        012346-R    CVRI001   ---->  CVRI001  ABCDE-0001  Day 1

        :param pandas.DataFrame df:
        :param str clean_kit_col: The kit ID column to join onto the LIMS to find timepoints
        :param str timepoint_col: The timepoint column in df to populate with LIMS data
        """

        self.logger.info('Cleaning timepoint column %s with kit column %s', timepoint_col, clean_kit_col)

        if timepoint_col not in df:
            df[timepoint_col] = pandas.Series(dtype=str)

        self._merge_joined_data(
            df[timepoint_col],
            self._join_dataframe(
                df, self.linker.lims_kits, clean_kit_col, 'kit_id', 'timepoint'
            )
        )
        return df

    def _clean_leftover_kits(self, df, patient_col, timepoint_col, clean_kit_col):
        """
        Try to fill in any kits still missing with the patient/timepoint from the LIMS, e.g. ABCDE-0001 / Day 1 -> CCP0001.
        Meant as a stop-gap for particularly messy datasets with no sample ID.
        :param pandas.DataFrame df:
        :param str patient_col: The patient column to join to the LIMS
        :param str timepoint_col: The timepoint column to join to the LIMS
        :param str clean_kit_col: The kit ID column to populate with any discovered mappings. Will only be written to if
                                  the column is empty for that line.
        """

        self.logger.info(
            'Filling in missing kits for column %s with %s and %s',
            clean_kit_col,
            patient_col,
            timepoint_col
        )

        self._merge_joined_data(
            df[clean_kit_col],
            self._join_dataframe(
                df,
                self.linker.lims_patient_timepoint_to_kits,
                [patient_col, timepoint_col],
                ['patient_id', 'timepoint'],
                'kit_id'
            ),
            conservative=True  # only if there's no clean kit yet
        )
        return df


class Assay(PrimaryDataSource):
    """
    A dataset to be cleaned and exported to the release dir and NHS safe haven. Convenience subclass for setting exports
    and for picking up only assay tables.
    """

    """
    What to report this assay as in view_assays_done. Set this to report it as some other name (e.g. report
    wp5_rnaseq_crf_report as wp5_rnaseq), or None to make this dataset not count towards assay coverage.
    """
    report_coverage_as: str = ''
