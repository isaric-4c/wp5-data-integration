"""
This module provides a set of parsers and objects for different ISARIC datasets. These are able to read in data,
populate and optionally clean a Pandas data frame, and output to SQL.
"""

from .common import PrimaryDataSource, Assay
from .clinical import ClinicalData, ClinicalTopline, ClinicalOneline
from .idamac import IdamacComplement
from .lims import LIMSData
from .linkage import LinkageSampleToPatient, LinkageSampleToSample
from .phosp import PHOSPLIMSData
from .wp3_viral_load import WP3BloodViralLoad
from .wp4_biochemistry import WP4Biochemistry
from .wp5_wgs import WP5WGS
from .wp5_rnaseq import WP5RNASeqData, WP5RNASeqAnalysis, WP5RNASeqCRFReport, WP5RNASeqFilteredSamples,\
    WP5RNASeqViralReadCounts, WP5RNASeqPseudotimes
from .wp5_clinical_imputation import WP5ClinicalImputation
from .wp5_proteomics import WP5ProteomicsPeptides, WP5ProteomicsProteins, WP5ProteomicsBatchData
from .wp6_cytokines import WP6PlasmaCytokines, WP6FaecalCytokines
from .wp6_anticytokineantibodies import WP6AntiCytokineAntibodiesIgA, WP6AntiCytokineAntibodiesIgG
from .wp6_nasosorption_mediators import WP6NasosorptionMediators
from .wp7_serology import WP7SerologySerum, WP7SerologyOralFluids, WP7SerologyNeutralisation
from .sample_assays import SampleAssays, Index

all_primary_data_sources = {
    cls.name: cls
    for cls in globals().values()
    if isinstance(cls, type)
    and issubclass(cls, PrimaryDataSource)
    and cls.name  # not a superclass
}

all_assays = {
    name: cls
    for name, cls in all_primary_data_sources.items()
    if issubclass(cls, Assay)
}
