from sqlalchemy import Column, String, Float
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class WP7SerologyOralFluids(Assay):
    name = 'wp7_serology_oral_fluids'
    dirty_sample_id_col = 'Sample_Code'
    clean_kit_id_col = 'Kit_ID'
    description = 'Serology capture assay data for oral swab samples.'

    columns = (
        Column('Patient_ID', PatientID),
        Column('Sample_Code', SampleID, comment='LIMS barcode'),  # can't be a primary key, sometimes null

        # can't be a primary key, sometimes multiple samples within a kit are analysed
        Column('Kit_ID', SampleID, comment='Clean kit ID'),
        Column('Time_Point', Timepoint),
        Column('IgM_Spike_OD', Float),
        Column('IgM_Spike_S.CO', Float),
        Column('IgM_Spike_Result', String),
        Column('IgG_Spike_OD', Float),
        Column('IgG_Spike_S.CO', Float),
        Column('IgG_Spike_Result', String),
        Column('IgM_NP_OD', Float),
        Column('IgM_NP_S.CO', Float),
        Column('IgM_NP_Result', String),
        Column('IgG_NP_OD', Float),
        Column('IgG_NP_S.CO', Float),
        Column('IgG_NP_Result', String),
        Column('IgM_S1_OD', Float),
        Column('IgM_S1_S.CO', Float),
        Column('IgM_S1_result', String),
        Column('IgG_S1_OD', Float),
        Column('IgG_S1_S.CO', Float),
        Column('IgG_S1_result', String),
    )


class WP7SerologySerum(WP7SerologyOralFluids):
    name = 'wp7_serology_serum'
    description = (f'Serology capture data for serum samples, as for {WP7SerologyOralFluids.name}, with additional '
                   'columns for DABA measurements.')

    columns = WP7SerologyOralFluids.columns + (
        Column('DABA_OD', Float),
        Column('DABA_S.CO', Float),
        Column('DABA_result', String),
        Column('DABA_Ab_quantification_AU', Float)
    )


class WP7SerologyNeutralisation(Assay):
    name = 'wp7_serology_neutralisation'
    dirty_sample_id_col = 'Sample Code'
    clean_kit_id_col = 'Kit ID'

    description = ('Neutralisation titres for WP7 serology. The lowest measurable titration value is 32 (i.e. 1/32 '
                   'dilution), so any lower values like `1` should be treated as `<32`.')
    columns = (
        Column('Patient ID', PatientID),
        Column('Time Point', Timepoint),
        Column('Kit ID', SampleID, comment='Clean kit ID'),
        Column('Sample Code', SampleID, primary_key=True, comment='Sample barcode'),
        Column('Serum IgG IC50', Float),
        Column('Serum IgG IC70', Float),
        Column('Serum IgG IC90', Float)
    )
