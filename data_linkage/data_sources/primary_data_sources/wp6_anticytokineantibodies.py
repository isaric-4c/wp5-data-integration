from sqlalchemy import Column, String, Float
from data_linkage.cleaning import PatientID, Timepoint, SampleID
from . import Assay


class WP6AntiCytokineAntibodiesIgA(Assay):
    """Parser for ISARIC autoantibodies data.xlsx"""

    name = 'wp6_anticytokineantibodies_iga'
    dirty_sample_id_col = 'sample_id'
    clean_kit_id_col = 'kit_id'

    rename_columns = {
        'SAMPLE_ID': 'sample_id',
        'Patient_ID': 'patient_id',
        'Day': 'day',
        'Kit.ID': 'kit_id',
        'ACA': 'aca',
        'MFI': 'mfi',
    }

    description = 'Anti-cytokine antibody data from WP-6'
    columns = (
        # can't have primary keys
        Column('sample_id', SampleID),
        Column('patient_id', PatientID),
        Column('day', Timepoint, comment='Sampling timepoint'),
        Column('kit_id', SampleID, comment='Clean kit ID'),
        Column('isotype', String),
        Column('aca', String, comment='Anti-cytokine antibody'),
        Column('mfi', Float, comment='Mean fluorescence intensity'),
        Column('threshold', Float, comment='Cut-off for anti-cytokine antibody positivity')

    )


class WP6AntiCytokineAntibodiesIgG(WP6AntiCytokineAntibodiesIgA):
    """Parser for ISARIC autoantibodies data.xlsx"""

    name = 'wp6_anticytokineantibodies_igg'
    description = f'Serology capture data for serum samples, as for {WP6AntiCytokineAntibodiesIgA}'
