from data_linkage import utils


class DataSource:
    """
    A table object that is to be automatically populated from input data. This could be a cleaned pandas DataFrame or
    the results of a SQL view or query. Not meant to be subclassed directly - use DataSource, Assay, SecondaryDataSource
    or View.
    """

    """
    The dataset that this object represents. This will be the name of the SQL table. For PrimaryDataSources, this will
    also control what config keys are used to configure the object - see tests/assets/config.yaml for examples.
    """
    name: str = None

    """
    Brief description of the dataset and any quirks. Gets passed to SQL table comments and templated release
    documentation
    """
    description: str = None

    def __init__(self, db):
        self.logger = utils.setup_logger(self.name)
        self._table = None
        self.db = db

    @property
    def table(self):
        """
        Use meta and possibly other attributes like self.columns or self.select_statement to define a sqlalchemy table
        and assign to self.table.
        """
        if self._table is None:
            self._table = self._init_table()

        return self._table

    def _init_table(self):
        raise NotImplementedError

    def __lt__(self, other):
        """
        Comparison method to allow sorting of data_sources.all_tables.values()
        :param data_linkage.data_sources.DataSource other:
        """
        return self.name < other.name
