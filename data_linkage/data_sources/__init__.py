"""
This module provides a set of parsers and objects for different ISARIC datasets. These are able to read in data,
populate and optionally clean a Pandas data frame, and output to SQL.
"""

from .primary_data_sources import all_primary_data_sources
from .secondary_data_sources import all_secondary_data_sources
from .views import all_views

all_tables = {
    ds.name: ds
    for ds in list(all_primary_data_sources.values()) + list(all_views.values()) + list(all_secondary_data_sources.values())
}
