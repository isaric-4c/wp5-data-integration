import datetime
import dataclasses
import pandas
from sqlalchemy import select, Table, Column, String, Float, Boolean, Date
from data_linkage import cleaning
from .primary_data_sources import SampleAssays
from .common import DataSource

today = datetime.date.today()
data_linkage_assays = SampleAssays.data_linkage_assays


@dataclasses.dataclass
class Patient:
    """Representation of a patient to be reported on. May contain multiple Kits."""
    name: str
    site: str
    onset: datetime.date
    admission: datetime.date
    recruitment: datetime.date
    pbmc: bool
    kits: dict = dataclasses.field(default_factory=dict)


@dataclasses.dataclass
class Kit:
    """
    Representation of a Kit - links back to its containing patient, and contains multiple assay statuses, priorities and priority reasons.
    """

    name: str  # kit ID
    timepoint: str
    patient: Patient
    date_collected: datetime.date

    """Dict of assay_name: status, populated by the SQL view view_sample_status"""
    assay_statuses: dict = dataclasses.field(default_factory=dict)

    """Sets of assays to be prioritised for this kit, populated by self.evaluate_priorities()"""
    priorities: set = dataclasses.field(default_factory=set)
    priority_reasons: set = dataclasses.field(default_factory=set)

    @property
    def ndispatched(self):
        """How many known assays have been at least started for this kit"""
        return len([v for v in self.assay_statuses.values() if v != 'not_started'])

    @property
    def assay_coverage(self):
        """
        Proportion of assays that have been at least started for this kit. If everything's done then this will be 1.0
        and if nothing's started then this will be 0.0. If the kit has had WP5 RNA-seq done but not WP6 Plasma
        Cytokines, then it will be a number somewhere in between."""
        return self.ndispatched / len(self.assay_statuses)

    def _prioritise(self, priority, reason):
        self.priorities.add(priority)
        self.priority_reasons.add(reason)

    def evaluate_priorities(self):
        """
        Prioritisation rules for kits. Current rules:
        - Try to maximise deep assay coverage for samples - if a kit has had 60+% of available assays done, then
          prioritise the rest
        - Prioritise WP6 Plasma Cytokines for samples where the patient has had a PBMC sample taken
        - Prioritise RNA-seq for samples taken more than 28 days ago and where the patient has had a PBMC sample taken
        """
        if self.assay_coverage >= 0.6:  # if more than 60% coverage, then prioritise the rest
            for k, v in self.assay_statuses.items():
                if v == 'not_started':
                    self._prioritise(k, 'assay_coverage')

        if self.patient.pbmc:
            if self.assay_statuses['wp6_plasma_cytokines'] == 'not_started':
                self._prioritise('wp6_plasma_cytokines', 'cytokines for PBMC patients')

            if self.date_collected:
                sample_age = today - self.date_collected
                if sample_age.days > 28 and self.assay_statuses['wp5_rnaseq'] == 'not_started':
                    self._prioritise('wp5_rnaseq', 'RNA-seq for PBMC patients and samples >28 days old')

    def report_priorities(self):
        """Output a CSV-exportable line of data that can be passed to export_to_csv()"""
        line = [self.patient.name, self.name, self.timepoint, self.patient.site]
        line += [self.assay_statuses[k] for k in data_linkage_assays]
        line += [
            '%.2f' % (self.ndispatched / len(self.assay_statuses)), self.patient.pbmc,
            ';'.join(sorted(self.priorities)), ';'.join(sorted(self.priority_reasons)), self.patient.onset,
            self.patient.admission, self.patient.recruitment, self.date_collected
        ]
        return line


class SecondaryDataSource(DataSource):
    """A table built from other tables."""

    columns = None

    def output_data(self, connection):
        """
        Query data out of the database and return a transformed data frame
        :param sqlalchemy.engine.Connection connection:
        """
        raise NotImplementedError

    def _init_table(self):
        return Table(self.name, self.db.meta, *[col.copy() for col in self.columns])

    def load_data(self):
        """Load the results of self.output_data() into a database."""

        with self.db.connect() as connection:
            self.logger.info('Building dataframe')
            df = self.output_data(connection)
            self.logger.info('Importing to SQL')
            df.to_sql(self.name, connection, schema=self.db.meta.schema, if_exists='append', index=False)
            self.logger.info('SQL import done')

        return df  # returns the sqlite or postgres data frame, but we don't do anything with the return value here


class DispatchPriorities(SecondaryDataSource):
    name = 'dispatch_priorities'
    description = 'Assay priority table. Shows kit/patient information, assays statuses and assigned priorities'

    columns = [
        Column('patient_id', cleaning.PatientID),
        Column('kit_id', cleaning.SampleID),
        Column('timepoint', cleaning.Timepoint),
        Column('clinical_site', String)
    ] + [Column(col, String) for col in data_linkage_assays] + [
        Column('assay_coverage', Float, comment='Proportion of assays where this kit is either dispatched or has data back'),
        Column('pbmc_available', Boolean, comment='Whether a PBMC sample exists in the LIMS for this patient'),
        Column('priorities', String, comment='Auto-assigned priorities. For full rules, see https://git.ecdf.ed.ac.uk/'
                                             'isaric-4c/wp5-data-integration/blob/master/data_linkage/data_sources/'
                                             'secondary_data_sources.py'),
        Column('priority_reasons', String, comment='Explanations for the priorities applied'),
        Column('cestdat', Date, comment='Disease onset'),
        Column('hostdat', Date, comment='Date of admission'),
        Column('dsstdat', Date, comment='Date of recruitment'),
        Column('date_collected', Date, comment='Date of sample collection')
    ]

    def initial_query(self, connection):
        kits_by_patient = {}

        # query the LIMS for current PBMC samples and keep a register of PBMC-ed patients - maybe this would be better
        # integrated into a SQL query somewhere?
        lims = self.db.primary_data_sources['lims_data'].table
        pbmc_patients = set(
            line['patient_id'] for line in connection.execute(
                select([lims.c.patient_id]).where(lims.c.sample_type == 'PBMC').distinct()
            )
        )

        sample_status = self.db.views['view_sample_status'].table
        cursor = connection.execute(sample_status.select())
        for patient_id, kit_id, timepoint, assay, status, site, onset, admission, recruitment, collected in cursor:
            if not patient_id or not kit_id:
                continue

            if patient_id not in kits_by_patient:
                kits_by_patient[patient_id] = Patient(
                    patient_id,
                    site,
                    onset,
                    admission,
                    recruitment,
                    patient_id in pbmc_patients
                )

            patient = kits_by_patient[patient_id]
            if kit_id not in patient.kits:
                patient.kits[kit_id] = Kit(kit_id, timepoint, patient, collected)

            if assay in data_linkage_assays:
                patient.kits[kit_id].assay_statuses[assay] = status

        for p in sorted(kits_by_patient):
            patient = kits_by_patient[p]
            for k in sorted(patient.kits):
                kit = patient.kits[k]
                kit.evaluate_priorities()
                yield kit.report_priorities()

    def output_data(self, connection):
        df = pandas.DataFrame.from_records(
            list(self.initial_query(connection)),
            columns=[col.name for col in self.columns]
        )

        for col in self.columns:
            cleaning_func = cleaning.generic_cleaning_funcs.get(
                col.type.__class__,  # for custom column types, e.g. PatientID
                cleaning.generic_cleaning_funcs.get(col.type.python_type)  # for vanilla SQLAlchemy types, e.g. String, Integer
            )
            if cleaning_func:
                df[col.name] = cleaning_func(df[col.name])

        return df


all_secondary_data_sources = {
    cls.name: cls
    for cls in locals().values()
    if isinstance(cls, type)
    and issubclass(cls, SecondaryDataSource)
    and cls.name  # not a superclass
}
