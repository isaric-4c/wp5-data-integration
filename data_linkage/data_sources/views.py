from sqlalchemy import select, union, case, literal, and_, func
from data_linkage.db_engine.schema import view, days_elapsed, catunique, value_if_unique
from data_linkage.data_sources.common import DataSource
from data_linkage.data_sources.primary_data_sources.common import Assay


class View(DataSource):
    """A DataSource that defines a view of other data sources."""

    """Descriptions to attach to any dynamic case statements or aggregations in self.select_statements."""
    statement_descriptions = None

    def _init_table(self):
        return view(self.name, self.db.meta, self.select_statement, self.description, self.statement_descriptions)

    @property
    def select_statement(self):
        raise NotImplementedError


class ViewIdamacComplement(View):
    name = 'view_idamac_complement'
    description = 'Complement IDAMAC dataset, linked to clinical severity information'

    @property
    def select_statement(self):
        complement = self.db.primary_data_sources['idamac_complement'].table
        clinical = self.db.primary_data_sources['clinical_data'].table
        oneline = self.db.primary_data_sources['clinical_oneline'].table
        lims = self.db.views['view_lims_kits'].table

        return select(
            [
                complement,
                lims.c.date_collected.label('lims_sample_collected'),
                oneline.c.age,
                oneline.c.sex,
                oneline.c.any_icu,
                oneline.c.dsterm,
                oneline.c.cestdat,
                oneline.c.hostdat,
                oneline.c.onset2admission,
                days_elapsed('onset2sample', oneline.c.cestdat, lims.c.date_collected),
                days_elapsed('admission2sample', oneline.c.hostdat, lims.c.date_collected),
                oneline.c.ethnicity,
                oneline.c.severity_score.label('peak_severity_score'),
                oneline.c.severity_indication.label('peak_severity_indication'),
                clinical.c.severity_score.label('daily_severity_score'),
                clinical.c.severity_indication.label('daily_severity_indication')
            ],
            from_obj=complement.outerjoin(oneline, complement.c.patient_id == oneline.c.subjid)
                .outerjoin(lims, complement.c.kit_id == lims.c.kit_id)
                .outerjoin(clinical, and_(complement.c.patient_id == clinical.c.subjid, lims.c.date_collected == clinical.c.daily_dsstdat))
        ).order_by(complement.c.patient_id, complement.c.timepoint).distinct()


class ViewLimsKits(View):
    name = 'view_lims_kits'
    description = ('LIMS kit information. Use this view when joining on Kit IDs to avoid duplicate rows. If multiple '
                   'values are present in a kit for Date_Stored and Date_Collected, then these are returned as null')

    @property
    def select_statement(self):
        lims = self.db.primary_data_sources['lims_data'].table

        kit = lims.c.kit_id
        patient = catunique(lims.c.patient_id).label('patient_id')
        timepoint = catunique(lims.c.timepoint).label('timepoint')

        return select(
            [
                patient,
                kit,
                timepoint,
                value_if_unique(lims.c.date_stored).label('date_stored'),
                catunique(lims.c.city).label('location'),
                catunique(lims.c.hospital).label('site'),
                value_if_unique(lims.c.date_collected).label('date_collected'),
                catunique(lims.c.date_collected).label('all_dates_collected')
            ],
            group_by=lims.c.kit_id
        ).distinct().order_by(patient, timepoint, kit)


class ViewWP4Biochemistry(View):
    name = 'view_wp4_biochemistry'
    description = 'WP4 biochemistry linked to clinical data'

    statement_descriptions = {
        'duration': 'Number of days from symptom onset (clinical_oneline.cestdat) to sample collection '
                    '(lims_data."Date_Collected")',
        'time2icu': 'Number of days from admission (hostdat) to ICU admission (icu_hostdat)',
        'group': "Group assignment as per biochem_all_README.txt. any_icu == True and dsterm == 'death' -> 'Fatal', "
                 "any_icu == True and dsterm != 'death' -> 'Critical survivor', any_icu == False and dsterm != 'death' "
                 "-> 'Non-critical survivor'"
    }

    @property
    def select_statement(self):
        biochem = self.db.primary_data_sources['wp4_biochemistry'].table
        clinical = self.db.primary_data_sources['clinical_data'].table
        topline = self.db.primary_data_sources['clinical_topline'].table
        oneline = self.db.primary_data_sources['clinical_oneline'].table
        lims = self.db.primary_data_sources['lims_data'].table

        return select(
            [
                biochem,
                topline.c['sex'].label('Sex'),
                topline.c['calc_age'].label('Age'),
                topline.c['onset2admission'],
                topline.c['hostdat'].label('date_admit'),

                # all NA in topline.csv - used to come from outcome.csv, which has been superseded by oneline.csv
                oneline.c['icu_hostdat'].label('date_icu'),
                days_elapsed('duration', topline.c['cestdat'], lims.c.date_collected),
                days_elapsed('time2icu', topline.c['hostdat'], oneline.c['icu_hostdat']),
                topline.c['daily_invasive_prtrt'].label('IMV'),
                topline.c['any_icu'].label('ICU'),
                topline.c['dsterm'].label('outcome'),
                case(
                    [
                        (and_(topline.c['any_icu'] == True, topline.c['dsterm'] == 'Death'), 'Fatal'),
                        (and_(topline.c['any_icu'] == True, topline.c['dsterm'] != 'Death'), 'Critical survivor'),
                        (and_(topline.c['any_icu'] == False, topline.c['dsterm'] != 'Death'), 'Non-critical survivor')
                    ],
                    else_='unknown'
                ).label('group'),
                oneline.c.severity_score.label('peak_severity_score'),
                oneline.c.severity_indication.label('peak_severity_indication'),
                clinical.c.severity_score.label('daily_severity_score'),
                clinical.c.severity_indication.label('daily_severity_indication')
            ],
            from_obj=biochem.outerjoin(
                lims,
                biochem.c['ISARIC sample ID'] == lims.c.sample_code
            ).outerjoin(
                topline,
                biochem.c['ISARIC patient ID'] == topline.c.subjid
            ).outerjoin(
                oneline,
                biochem.c['ISARIC patient ID'] == oneline.c.subjid
            ).outerjoin(
                clinical,
                and_(
                    biochem.c['ISARIC patient ID'] == clinical.c.subjid,
                    lims.c.date_collected == clinical.c.daily_dsstdat
                )
            )
        ).order_by(biochem.c['ISARIC patient ID'], biochem.c['Timepoint'])


class ViewWP5RNASeqIntegration(View):
    name = 'view_wp5_rnaseq_integration'
    description = 'WP5 RNA-seq samples, including CRF metadata and information on sample filtering'
    statement_descriptions = {
        'filtered': 'If sample is present in wp5_rnaseq_filtered_samples -> True, otherwise -> False'
    }

    @property
    def select_statement(self):
        gene_counts = self.db.primary_data_sources['wp5_rnaseq_analysis'].table
        crf_report = self.db.primary_data_sources['wp5_rnaseq_crf_report'].table
        filtered_samples = self.db.primary_data_sources['wp5_rnaseq_filtered_samples'].table
        lims = self.db.primary_data_sources['lims_data'].table

        return select(
            list(gene_counts.c)[:5] + [
                lims.c.date_collected.label('date_taken'),
                case(
                    [(filtered_samples.c.reason != None, True)],
                    else_=False
                ).label('filtered'),
                filtered_samples.c.reason.label('filtered_reason'),
                crf_report.c['collection_tube'],
                crf_report.c['plate_id'],
                crf_report.c['run_id'].label('rnaseq_run_id'),
                crf_report.c['rqn']
            ] + list(gene_counts.c)[5:],
            from_obj=gene_counts.outerjoin(
                crf_report,
                gene_counts.c.clean_rnaseq_sample_id == crf_report.c['sample_id']
            ).outerjoin(
                lims,
                gene_counts.c.kit_id == lims.c.kit_id
            ).outerjoin(
                filtered_samples,
                gene_counts.c.rnaseq_sample_id == filtered_samples.c.rnaseq_sample_id
            )
        ).order_by(gene_counts.c.rnaseq_sample_id).distinct()


class ViewWP6PlasmaCytokines(View):
    name = 'view_wp6_plasma_cytokines'
    description = 'WP6 plasma cytokines linked to clinical data'
    statement_descriptions = {
        'onset2sample': 'Number of days from disease onset (cestdat) to sample_collected',
        'admission2sample': 'Number of days from admission (hostdat) to sample_collected'
    }

    @property
    def select_statement(self):
        cytokines = self.db.primary_data_sources['wp6_plasma_cytokines'].table
        clinical = self.db.primary_data_sources['clinical_data'].table
        oneline = self.db.primary_data_sources['clinical_oneline'].table
        lims = self.db.views['view_lims_kits'].table
        return select(
            [
                cytokines,
                lims.c.date_collected.label('sample_collected'),
                oneline.c.age,
                oneline.c.sex,
                oneline.c.any_icu,
                oneline.c.dsterm,
                oneline.c.cestdat,
                oneline.c.hostdat,
                oneline.c.onset2admission,
                days_elapsed('onset2sample', oneline.c.cestdat, lims.c.date_collected),
                days_elapsed('admission2sample', oneline.c.hostdat, lims.c.date_collected),
                oneline.c.ethnicity,
                oneline.c.severity_score.label('peak_severity_score'),
                oneline.c.severity_indication.label('peak_severity_indication'),
                clinical.c.severity_score.label('daily_severity_score'),
                clinical.c.severity_indication.label('daily_severity_indication')
            ],
            from_obj=cytokines.outerjoin(oneline, cytokines.c['Patient ID'] == oneline.c.subjid)
                .outerjoin(lims, cytokines.c['Kit ID'] == lims.c.kit_id)
                .outerjoin(
                    clinical,
                    and_(
                        cytokines.c['Patient ID'] == clinical.c.subjid,
                        lims.c.date_collected == clinical.c.daily_dsstdat
                    )
                )
            ).order_by(cytokines.c['Kit ID'], cytokines.c['Assay']).distinct()


class ViewWP6NasosorptionMediators(View):
    name = 'view_wp6_nasosorption_mediators'
    description = 'WP6 nasosorption mediators linked to clinical data and with date_taken cleaned'
    statement_descriptions = {
        'onset2sample': 'Number of days from disease onset (cestdat) to sample_collected',
        'admission2sample': 'Number of days from admission (hostdat) to sample_collected',
    }

    @property
    def select_statement(self):
        mediators = self.db.primary_data_sources['wp6_nasosorption_mediators'].table
        clinical = self.db.primary_data_sources['clinical_data'].table
        oneline = self.db.primary_data_sources['clinical_oneline'].table
        lims = self.db.views['view_lims_kits'].table

        return select(
            [
                mediators,
                oneline.c.age,
                oneline.c.sex,
                oneline.c.any_icu,
                oneline.c.dsterm,
                oneline.c.cestdat,
                oneline.c.hostdat,
                oneline.c.onset2admission,
                days_elapsed('onset2sample', oneline.c.cestdat, lims.c.date_collected),
                days_elapsed('admission2sample', oneline.c.hostdat, lims.c.date_collected),
                oneline.c.ethnicity,
                oneline.c.severity_score.label('peak_severity_score'),
                oneline.c.severity_indication.label('peak_severity_indication'),
                clinical.c.severity_score.label('daily_severity_score'),
                clinical.c.severity_indication.label('daily_severity_indication')
            ],
            from_obj=mediators.outerjoin(oneline, mediators.c.patient_id == oneline.c.subjid)
                .outerjoin(lims, mediators.c.kit_id == lims.c.kit_id)
                .outerjoin(
                clinical,
                    and_(
                        mediators.c.patient_id == clinical.c.subjid,
                        lims.c.date_collected == clinical.c.daily_dsstdat
                    )
                )
        ).order_by(mediators.c.plate_id).distinct()


class ViewWP7Serology(View):
    serology_table = None
    description = 'WP7 serology data linked to clinical data'
    statement_descriptions = {
        'Days_since_onset': 'Number of days from disease onset (cestdat) to sample_collected'
    }

    @property
    def select_statement(self):
        serology_table = self.db.primary_data_sources[self.serology_table].table
        topline = self.db.primary_data_sources['clinical_topline'].table
        lims = self.db.primary_data_sources['lims_data'].table

        sample_metadata = [
            lims.c.date_collected.label('Sample_Collected'),
            days_elapsed('Days_since_onset', topline.c['cestdat'], lims.c.date_collected)
        ]
        cols = list(serology_table.c)[:4] + sample_metadata + list(serology_table.c)[4:]
        cols.extend(
            [
                topline.c[colname]
                for colname in (
                    'age', 'sex', 'dsstdat', 'cestdat', 'hostdat', 'rr_vsorres', 'oxy_vsorres', 'daily_gcs_vsorres',
                    'daily_bun_lborres', 'daily_crp_lborres', 'any_icu', 'dsterm', 'death', 'any_invasive',
                    'daily_nasaloxy_cmtrt'
                )
            ]
        )
        return select(
            cols,
            from_obj=serology_table.outerjoin(
                lims,
                serology_table.c['Kit_ID'] == lims.c.kit_id
            ).outerjoin(topline, serology_table.c['Patient_ID'] == topline.c.subjid)
        ).order_by(serology_table.c['Kit_ID']).distinct()


class ViewWP7SerologyOralFluids(ViewWP7Serology):
    name = 'view_wp7_serology_oral_fluids'
    serology_table = 'wp7_serology_oral_fluids'


class ViewWP7SerologySerum(ViewWP7Serology):
    name = 'view_wp7_serology_serum'
    serology_table = 'wp7_serology_serum'


class ViewAssaysDone(View):
    """What sample-centric assays we have data for, per sample"""

    name = 'view_assays_done'
    description = 'List of kits and assays performed'

    @property
    def select_statement(self):
        assay_kits = []
        for ds in sorted(self.db.primary_data_sources.values()):
            if isinstance(ds, Assay) and ds.report_coverage_as is not None:
                kit_column = ds.table.c[ds.clean_kit_id_col]
                assay_kits.append(
                    select([kit_column.label('kit_id'), literal(ds.report_coverage_as or ds.name).label('assay_name')])
                )

        u = union(*assay_kits).alias()
        return select([u], whereclause=u.c.kit_id != None).order_by(u.c.kit_id, u.c.assay_name)


class ViewSampleStatus(View):
    name = 'view_sample_status'
    description = 'Sample statuses for each assay per kit. Status will be either not_started, dispatched or complete.'
    statement_descriptions = {
        'status': "'complete' if we have data back for this kit/assay, 'dispatched' if a sample has been dispatched "
                  "for the kit/assay, or otherwise 'not_started'"
    }

    @property
    def select_statement(self):
        sample_assays = self.db.primary_data_sources['sample_assays'].table
        assays_done = self.db.views['view_assays_done'].table
        topline = self.db.primary_data_sources['clinical_topline'].table
        lims = self.db.views['view_lims_kits'].table
        dispatches = self.db.primary_data_sources['lims_data'].table

        return select(
            [
                lims.c.patient_id.label('patient_id'),
                lims.c.kit_id.label('kit_id'),
                lims.c.timepoint.label('timepoint'),
                sample_assays.c.name.label('assay_name'),
                case(
                    [
                        (assays_done.c.assay_name != None, 'complete'),
                        (dispatches.c.dispatched_to_assay != None, 'dispatched'),
                    ],
                    else_='not_started'
                ).label('status'),
                topline.c.sitename.label('clinical_site'),
                topline.c.cestdat.label('cestdat'),  # onset
                topline.c.hostdat.label('hostdat'),  # admission
                topline.c.dsstdat.label('dsstdat'),  # recruitment
                lims.c.date_collected.label('date_collected')
            ],
            from_obj=lims.join(
                sample_assays,
                literal(True)  # crossjoin - all possible combinations
            ).outerjoin(
                # what sample assays have been dispatched
                # we join by kit ID and assay because we don't want to multiply assays per barcode
                dispatches,
                and_(
                    lims.c.kit_id == dispatches.c.kit_id,
                    sample_assays.c.name == dispatches.c.dispatched_to_assay
                )
            ).outerjoin(
                assays_done,
                and_(
                    lims.c.kit_id == assays_done.c.kit_id,
                    sample_assays.c.name == assays_done.c.assay_name
                )
            ).outerjoin(
                topline,
                lims.c.patient_id == topline.c.subjid
            )
        ).order_by(
            lims.c.patient_id,
            lims.c.kit_id,
            sample_assays.c.name
        ).distinct()


class ViewPeakSeverity(View):
    name = 'view_peak_severity'
    description = 'Peak severity per patient, from clinical_oneline'

    @property
    def select_statement(self):
        clinical = self.db.primary_data_sources['clinical_oneline'].table

        return select(
            [
                clinical.c.subjid,
                clinical.c.severity_score,
                clinical.c.severity_indication
            ]
        )


class ViewDailySeverity(View):
    name = 'view_daily_severity'
    description = 'Daily disease severity based on subjid and daily_dsstdat'

    @property
    def select_statement(self):
        clinical = self.db.primary_data_sources['clinical_data'].table

        return select(
            [
                clinical.c.subjid,
                clinical.c.daily_dsstdat,
                clinical.c.severity_score,
                clinical.c.severity_indication
            ],
            whereclause=clinical.c.daily_dsstdat != None
        ).distinct()


class ViewFullSampleLinkage(View):
    name = 'view_full_sample_linkage'
    description = ('Full list of all known sample IDs and aliases (e.g. `_R` repeats), and what kits/patients they '
                   'correspond to.')

    @property
    def select_statement(self):
        sample_to_sample = self.db.primary_data_sources['linkage_sample_to_sample'].table
        sample_to_patient = self.db.primary_data_sources['linkage_sample_to_patient'].table
        kit_to_patient = self.db.views['view_lims_kits'].table
        lims = self.db.primary_data_sources['lims_data'].table

        sample_to_kit = union(
            select(
                [
                    lims.c.sample_code.label('sample_id'),
                    lims.c.kit_id,
                    literal('lims barcode').label('source')
                ]
            ),

            select(
                [
                    func.substr(lims.c.sample_code, 4).label('sample_id'),  # SMG0123456 -> 0123456
                    lims.c.kit_id,
                    literal('lims barcode/SMG prefix').label('source')
                ],
                whereclause=lims.c.sample_code.startswith('SMG')
            ),

            select(
                [
                    func.substr(lims.c.sample_code, 5).label('sample_id'),  # SMG0123456 -> 123456
                    lims.c.kit_id,
                    literal('lims barcode/SMG0 prefix').label('source')
                ],
                whereclause=lims.c.sample_code.startswith('SMG0')
            ),

            select(
                [
                    lims.c.kit_id.label('sample_id'),
                    lims.c.kit_id,
                    literal('lims kit').label('source')
                ]
            )
        ).alias()

        all_samples_to_kit = union(
            select(
                [
                    sample_to_sample.c.sample_id,
                    sample_to_kit.c.kit_id,
                    sample_to_sample.c.source
                ],
                from_obj=sample_to_sample.outerjoin(
                    sample_to_kit,
                    sample_to_sample.c.isaric_sample_id == sample_to_kit.c.sample_id
                )
            ),

            select(
                [sample_to_kit]
            )
        ).alias()

        return select(
            [
                all_samples_to_kit.c.sample_id,
                all_samples_to_kit.c.kit_id,
                kit_to_patient.c.patient_id,
                catunique(all_samples_to_kit.c.source).label('sample_linkage_notes'),
                catunique(sample_to_patient.c.source).label('patient_linkage_notes')
            ],
            from_obj=all_samples_to_kit.outerjoin(
                kit_to_patient,
                all_samples_to_kit.c.kit_id == kit_to_patient.c.kit_id
            ).outerjoin(
                sample_to_patient,
                all_samples_to_kit.c.sample_id == sample_to_patient.c.sample_id
            )
        ).group_by(
            all_samples_to_kit.c.sample_id,
            all_samples_to_kit.c.kit_id,
            kit_to_patient.c.patient_id,
        ).order_by(all_samples_to_kit.c.sample_id)


all_views = {
    cls.name: cls
    for cls in locals().values()
    if isinstance(cls, type)
    and issubclass(cls, View)
    and cls.name  # not a superclass
}
