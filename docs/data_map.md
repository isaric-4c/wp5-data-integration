# WP5 Data Integration - data map

This document describes the datasets currently available in the data linkage.


## CSV exports

Several of the SQL tables described below are exported as CSVs in the release folder:
{% for dsname in export_csvs | sort %}- {{ '%-32s -> %s' % (dsname, export_csvs[dsname]) }}
{% endfor %}

Since the REDCap clinical data is quite large and is already clean, CSVs of this are not included in the release, but
the input CSVs are available in the shared/data location. 


## Datasets

To generate the ISARIC datasets, samples are dispatched to collaborators, who send back assay data, which is added into
the research environment. Cleaned data is available in two places: the c19-isaric01 database, and in release folders
like this one.

If you wish to work in Postgres with the up-to-date live data, use the c19-isaric01 database, either with psql or a
language-specific interface like SQLAlchemy or RPostgreSQL. If you wish to perform SQL joins with versioned data, use
the `db.sqlite` SQLite database in this folder. For convenience, some of the tables from db.sqlite are also available as
CSV files in this folder - see below for further information.

{% for ds in primary_data_sources %}
### {{ ds.name }}

{{ ds.description | string | wordwrap(width=110) }}

Raw data: {{ ds.source | string }}
{% if ds.name in export_csvs %}CSV export at: {{ export_csvs[ds.name] }}
{% endif %}
{% if ds.columns | length <= 50 %}Columns:
{% for col in ds.columns %}- {{ '%-25s %10s' % (col.name, col.type) }}{% if col.primary_key %}  Primary key. {% else %}  {% endif %}{% if col.comment %}{{ col.comment | wordwrap(78) | indent(42, first=False) }}{% endif %}
{% endfor %}{% endif %}
{% endfor %}

## SQL views

Some SQL views are also defined to provide pre-rolled linkage queries. These can be selected and filtered in the same
way as ordinary SQL tables.

{% for v in views %}
### {{ v.name }}

{{ v.description | string | wordwrap(width=110)}}
{% if v.name in export_csvs %}CSV export at: {{ export_csvs[v.name] }}
{% endif %}
Columns:

{% for col in v.table.c %}- {{ '%-25s' % col.name }}{% if col.comment %}  {{ col.comment | wordwrap(81) | indent(31, first=False) }}{% endif %}
{% endfor %}
{% endfor %}
{% if secondary_data_sources %}
## Secondary datasets

These are pivots, reports and other tables built from the other primary data sources and views above.

{% for ds in secondary_data_sources %}
### {{ ds.name }}

{{ ds.description | string | wordwrap(width=110)}}

{% if ds.name in export_csvs %}CSV export at: {{ export_csvs[ds.name] }}
{% endif %}
Columns:
{% for col in ds.columns %}- {{ '%-25s %10s' % (col.name, col.type) }}{% if col.primary_key %}  Primary key{% else %}{{ ' '.ljust(13) }}{% endif %}{% if col.comment %}  {{ col.comment | wordwrap(67) | indent(55, first=False) }}{% endif %}
{% endfor %}{% endfor %}{% endif %}
