# WP5 Data Integration - linkage notes

As part of WP-5, an automated data linkage pipeline scrapes and links this data into SQL tables and exported CSVs. These
tables are available in the c19-isaric01 database and in the data releases.


## Data integration pipeline

The c19-isaric01 database is populated by an automated pipeline, which picks up data from the various work packages, and
loads it into Pandas data frames. These data frames are cleaned and imported into SQL tables - for more information on
the cleaning performed, see linkage.md in the release folder, e.g. release/20210521_v32/linkage.md. The pipeline is
open-source, and its source code repository is available at https://git.ecdf.ed.ac.uk/isaric-4c/wp5-data-integration.


## Sample IDs

In virtually all cases, lab assays will have been performed on individual samples. As such, your data should include a
sample ID. This can be a kit ID (e.g. `CCP0001`), but would ideally be a sample barcode (e.g. `SM2004012345`).

For Glasgow samples, in the LIMS the kit ID will look something like `CVRI0001` and the barcode will have an `SMG`
prefix, e.g. `SMG0123456`. The barcode printed on the physical sample may however look like `0123456` which, if it has
been loaded into Excel at any point, may have been trimmed to `123456`. The data linkage is able to account for all of
these cases automatically.

The data linkage cannot account for nonexistent or ambiguous IDs (e.g. `kit not known`, or
`0123456 - or maybe 0123458?`), or sample aliquots leading to mismatches with the LIMS - e.g. if sample 0123456 is
aliquoted out into 0123456A and 0123456B. 


## Cleaning a dataset

Production of clean data requires:

- a raw dataset to be cleaned
- the LIMS data

Ideally this would be all we need, but to account for sample repeats and corrections to the LIMS, there are also two
linkage tables:

- sample-patient linkage
- sample-sample linkage


### Field reformatting

First, basic reformatting is applied:

- ISARIC patient IDs are reformatted where possible, e.g. `REMRQ0001` -> `REMRQ-0001`
- Timepoints are cleaned and standardised into the form `Day x` or `Convalescent`
- Dates and times in various known formats are parsed into date/time/datetime objects
- Booleans are parsed to true/false values, e.g. when expressed in the raw data as yes/no, 1/0, etc.
- Any columns not defined in the dataset's schema (i.e, columns not native to the dataset) are dropped. We do this
  because often raw data submitted contains some linked clinical data already, and it's preferable to link dynamically
  in SQL and avoid data duplication.

These data frames are then inserted into SQL tables. SQL views are then defined, where any columns dropped from the
original files are re-added as dynamically linked columns.

A log file is given for each assay/data source, as well as a main 'release' log, detailing the versions of the
automation and linkage tables used, and any problems identified during cleaning.


### Linkage cleaning

In the real world, a raw dataset's sample identifiers may be messy, consisting of a mix of kit IDs and barcodes.
Sometimes, we don't even have that and only have patient IDs and timepoints. In cases where we have some form of sample
ID, we can use the linkage tables to clean this into a kit ID that should always map to the LIMS. Given the input
dataset:

    assay_data
    sample_id    notes
    CCP0001      kit ID
    SM20040001   barcode
    0123456      Glasgow barcode
    123456       Excel-ed Glasgow barcode
    123456-R     Repeat of sample 123456
    CCPBATCH001  Old kit ID to be remapped


First we use the sample-sample linkage table to remap any sample IDs that require it:

    assay_data        sample-sample linkage
    sample_id         sample_id     clean_sample_id
    CCP0001      ---------------->  CCP0001
    SM20040001   ---------------->  SM20040001
    0123456      ---------------->  0123456
    123456       ---------------->  123456   
    123456-R     -->  123456        123456
    CCPBATCH001  -->  CCPNBATCH001  CCPNBATCH001


Now we need to clean any barcodes into kit IDs. To do this, we can use the LIMS. Glasgow barcodes can be extremely messy
though, which we need to allow for. To do this, when the data integration pipeline is being run the data frame for the
LIMS is expanded to include all possible cases for Glasgow barcodes:

    samplecode    Kit_ID
    SMG0123456    CVRI001
    0123456       CVRI001
    123456        CVRI001


With this, we can now join any vaguely linkable barcode to get a kit ID: 

    assay_data                        lims
    sample_id    clean_sample_id      samplecode  Kit_ID
    CCP0001      CCP0001          ------------->  CCP0001
    SM20040001   SM20040001       --> SM20040001  CCP0002
    0123456      0123456          --> 0123456     CVRI001
    123456       123456           --> 123456      CVRI001
    123456-R     123456           --> 123456      CVRI001
    CCPBATCH001  CCPNBATCH001     ------------->  CCPNBATCH001


From here, we can jump to the patient ID and timepoint. This is also done using the LIMS, except the sample-patient
linkage table is used to override cases where the LIMS mapping is wrong:

    assay_data                          linkage_table                     lims
    sample_id    clean_sample_id  clean_kit_id      sample_id   canonical_isaric_id   Kit_ID        Patient_ID
    CCP0001      CCP0001          CCP0001      -->  CCP0001     ABCDE-0001            CCP0001       ABCDE001
    SM20040001   SM20040001       CCP0002      ------------------------------------>  CCP0002       ABCDE-0002
    0123456      0123456          CVRI001      -->  CVRI001     ABCDE-0003            CVRI001       ABCDE-0003
    123456       123456           CVRI001      -->  CVRI001     ABCDE-0003            CVRI001       ABCDE-0003
    123456-R     123456           CVRI001      -->  CVRI001     ABCDE-0003            CVRI001       ABCDE-0003
    CCPBATCH001  CCPNBATCH001     CCPNBATCH001 ------------------------------------>  CCPNBATCH001  ABCDE-0004


It is possible to obtain timepoints in the same way. We now have a fully cleaned dataset:

    sample_id    clean_kit_id  clean_patient_id  clean_timepoint  ...
    CCP0001      CCP0001       ABCDE001          Day 1
    SM20040001   CCP0002       ABCDE-0002        Day 1
    0123456      CVRI001       ABCDE-0003        Day 1
    123456       CVRI001       ABCDE-0003        Day 1
    123456-R     CVRI001       ABCDE-0003        Day 1
    CCPBATCH001  CCPNBATCH001  ABCDE-0004        Convalescent


In cases where there is no sample ID and only patient/timepoint, there is some cleaning that we can still do. In cases
where there is an unambiguous LIMS mapping from a patient and timepoint to a kit, we can fill in the kit ID:

    assay_data              lims
    Patient ID  Timepoint   Patient_ID  Timepoint  Kit_ID
    ABCDE-0001  Day 1   --> ABCDE-0001  Day 1      CCP0001  # successful mapping
    ABCDE-0002  Day 1       ABCDE-0002  Day 1      CCP0002  # this patient/timepoint maps to two kits - no mapping made
                            ABCDE-0002  Day 1      CCP0003

    ABCDE0003   Day 1       ABCDE-0003  Day 1      CCP0004  # if the patient ID isn't already clean in the assay data, it cannot be mapped

Todo: add patient-patient linkage table
