# Data linkage release {{ release }}

This is a snapshot of the cleaned ISARIC data stored in the c19-isaric01 database. It consists of:

- Data tables in two forms:
  - db.sqlite - SQLite database of all tables and views included in this release 
  - CSV versions of the clean SQL tables
- Readme documentation
  - Readme.md for general information
  - data_map.md for descriptions of specific datasets
  - linkage.md for notes on how the data linkage works

It may also include:

- Log files from the data cleaning and export process
- pgdump snapshot of the main database at the time of this release - for admin use

All datasets here have passed though the ISARIC data integration pipeline. For more information on this process, see
linkage.md.


## About ISARIC data releases

Raw data from lab assays, REDCap and the Liverpool LIMS are collated into a series of datestamped releases.

- A new release folder is created
- The raw data is cleaned and imported into SQL tables in the c19-isaric01 Postgres database
- SQL snapshots and CSV exports are made in the release folder

This is under active development, so the content of the data releases changes over time.

The main source of truth for current cleaned data is c19-isaric01. This is a Postgres database containing the most
up-to-date versions of all datasets and linkages. For more information on accessing this database, see the ISARIC Use of
Ultra 2 document.

For reproducible research, use the SQLite and CSV exports in this release folder. The document `data_map.md` contains
descriptions of datasets in the release, including columns and datatypes.


### Contributing

The data held by ISARIC comes from a wide range of collaborators, and new datasets are added over time. If you have a
dataset to contribute, then:

- Speak to the data management team to discuss the dataset.
- Provide your data to be uploaded into Ultra 2. This may be via the ISARIC SharePoint, or other arrangements may be
  made.
- The data integration pipeline will then be run and your cleaned, linkable data will become available in the ISARIC
  database and release folders.

The data management team currently consists of Murray Wham and Andy Law.


## Linking datasets

There are three main types of table in the ISARIC data releases:

- Clinical data - attributed to the patient, referenced by the ISARIC patient ID
- LIMS sample data - attributed to individual samples, can be referred to by sample barcode, kit ID, patient ID and
  sampling timepoint. 
- Assays - attributed to samples. The raw data will usually have some form of sample ID - either barcodes or kits
  (sometimes within the same column). Because of this, the cleaned versions of these tables will always
  have a column for the clean kit ID, usually named something like 'clean_kit_id'. This is then used along with the LIMS
  and linkage tables to populate clean patient IDs and timepoints. This way, it should always be possible to jump
  between datasets.

There are also:

- Linkage tables - curated tables that correct any mismapped samples and invalid patient IDs in the LIMS, and account
  for repeated samples and kit ID remappings.
- Auxiliary tables - these include the index of raw data sources per table, registers of samples that should be ignored
  for certain analyses, and other helper tables.


### 'view_*' datasets

There are a few pre-canned data linkages available in the release folders. You can tell these datasets apart because
they will have the word 'view_wp' in their names, indicating that their data comes not from a single table but from
joining across many tables.

For example, the `view_wp5_rnaseq_integration` dataset:

    wp5_rnaseq_integration.csv
    fields from wp5_rnaseq_gene_counts                      fields from LIMS
    rnaseq_sample_id     ...  kit_id   canonical_isaric_id  lims_timepoint  date_taken  ...
    CCP0001-R_NS2000_75  ...  CCP0001  ABCDE-0001           Day 1           2020-08-16  ...
    012345               ...  CVRI002  ABCDE-0001           Day 3           2020-08-19  ...

For more information on each dataset, see data_map.md.


### Custom data joins

For more specific cases, it is necessary to perform custom joins/merges. This can be done in SQL, with R/Pandas data
frame joins, or anything capable of working with SQL/CSV data.

There are generally two common scenarios for linking data:


#### From an assay to clinical data

To do this, join the assay table's patient ID column to 'subjid' in the clinical data:

    WP6 Cytokines                                        clinical_data
    Kit ID   Visit Day  Assay  Value  Patient ID         subjid      cestdat     ...
    CCP0001  Day 1      IL-2   1.234  ABCDE-0001    -->  ABCDE-0001  2021-09-01  ...
    CCP0002  Day 3      IL-10  2.345  ABCDE-0001    -->  ABCDE-0001  2021-09-01  ...


#### From an assay to another assay

To do this, join using the clean kit ID column in each dataset: 

    WP6 Cytokines                                      WP7 Serum Serology
    Patient ID  Visit Day  Assay  Value  Kit ID        Kit_ID   IgM_Spike_OD  ...
    ABCDE-0001  Day 1      IL-2   1.234  CCP0001  -->  CCP0001  0.12345       ...
    ABCDE-0001  Day 3      IL-10  2.345  CCP0002  -->  CCP0002  3.45678       ...


In R, this would work something like:

    library(dplyr)
    # load the CSV files - or load from the SQL database
    cytokines <- read_csv('path/to/wp6_plasma_cytokines.csv')
    serology <- read_csv('path/to/wp7_serology_neutralisation_titres.csv')
    
    cytokines %>%
        inner_join(
            serology,
            by=c('Kit_ID', 'Kit ID')  # or left_join to include unlinked data
        )

In Pandas:

    import pandas
    cytokines = pandas.read_csv('path/to/wp6_plasma_cytokines.csv')
    serology = pandas.read_csv('path/to/wp7_serology_neutralisation_titres.csv')
    
    join = pandas.merge(
        cytokines,
        serology,
        how='inner',  # or 'left' to include unlinked data
        left_on='Kit ID',
        right_on='Kit_ID'
    )


In SQL:

    SELECT
        wp6_plasma_cytokines."Patient ID",
        wp6_plasma_cytokines."Visit Day",
        wp6_plasma_cytokines."Assay",
        wp6_plasma_cytokines."Value",
        wp6_plasma_cytokines."Kit ID",
        wp7_serology_serum."Kit_ID",
        wp7_serology_serum."IgM_Spike_OD"
    
    FROM wp6_plasma_cytokines
    LEFT OUTER JOIN wp7_serology_serum ON wp6_plasma_cytokines."Kit ID" = wp7_serology_serum."Kit_ID";


### Using the linkage tables

You may have your own data that has not been through the data integration pipeline, but want to link to the rest of
the ISARIC datasets. This can be done with the views `view_full_sample_linkage` and `view_lims_kits`. Both these tables
are available in SQL and CSV in the release folder.

The mappings in `view_full_sample_linkage` include clean kit and patient mappings for:

- All barcodes and kits in the curated linkage tables, e.g. SMG0123456, CCP0001
- All barcode/kit aliases in the curated linkage tables, e.g. CCP0001_R
- All kits in the clean LIMS data, e.g. CCP0001
- All barcodes in the clean LIMS data, e.g. SM20040123456, SMG0123456
- All common misformattings of Glasgow barcodes in the clean LIMS data, e.g. 0123456, 123456

To use this table, join your sample ID to the column `sample_id`:

    your_data        view_full_sample_linkage              clinical_topline
    Sample_ID        sample_id  kit_id    patient_id       subjid        dag_id  ...
    CCP0001    -->   CCP0001    CCP0001   ABCDE-1234  -->  ABCDE-1234    ABCDE
    123456     -->   123456     CVRI0001  ABCDE-1234  -->  ABCDE-1234    ABCDE
    CCP0003_R  -->   CCP0003_R  CCP0003   ABCDE-1235  -->  ABCDE-1235    ABCDE

It should not normally be necessary to directly use the underlying linkage tables that this view is based on, but you
can use them to audit where a particular mapping for a sample came from.

If all your sample IDs are known to be clean kit IDs and you want to link to patient IDs, you can also use the more
concise table `view_lims_kits`:

    your_data        view_lims_kits            clinical_topline
    Sample_ID        kit_id   patient_id       subjid        dag_id  ...
    CCP0001    -->   CCP0001  ABCDE-1234  -->  ABCDE-1234    ABCDE
    CVRI0001   -->   0123456  ABCDE-1235  -->  ABCDE-1234    ABCDE
