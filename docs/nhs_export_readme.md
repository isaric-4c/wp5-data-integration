# Release {{ release }} - NHS export

This data export consists of CSV exports from the ISARIC data integration on Ultra (release {{ release }}), to be added
to the NHS safe haven. Tables consist of patient ID, timepoint and assay measurements. Linked columns, sample IDs and
barcodes are currently not included.

To request clarifications or changes to what data is provided, contact the ISARIC data integration team.

Each exported table is included as a tar archive, `isaric-{table}.csv-{date}.tar`. Each archive consists of:
  - `isaric-{table}.csv-{date}.bz2` - compressed CSV export of the table
  - `isaric-{table}.csv-{date}.json` - metadata for the table, including number of records and uncompressed byte size
    and MD5 checksum
  - `isaric-{table}.csv-{date}.sql` - Postgres table definition. This is auto-generated from the data query that
    generated the export.

Exports are made for the following tables:

{% for ds in tables %}- {{ ds.name }}: {{ ds.description }}
{% endfor %}

## Notes on data cleaning

Raw data on Ultra (CSVs, spreadsheets, NGS pipeline outputs) is imported into SQL tables in the data integration via
data frames. Some initial cleaning is done on them:

- ISARIC patient IDs are passed through automated reformatting for simple fixes, e.g. `REMRQ0001` -> `REMRQ-0001`
- LIMS data and curated linkage tables are used to:
  - Clean sample IDs into a kit ID
  - Clean the patient ID using the clean kit ID 
- Timepoints are cleaned and standardised into the form of either `Day x` or `Convalescent`
