"""
Runs a data release for all available data types.

- For each defined data source, data files are picked up and loaded into a Pandas data frame
- The data frame is cleaned
  - Values are cast for each column according to its corresponding SQL datatype
  - Common formatting errors are corrected, e.g. datetimes, timepoints, ISARIC patient IDs
  - Linkages of sample barcodes, kits, patients and timepoints are fixed using the LIMS and linkage tables
- The cleaned data frame is inserted into SQL
- A set of pre-rolled queries are then run:
  - linked data is written to release_dir/ for convenience in the ISARIC research environment
  - cleaned but not linked data is added to nhs_export_dir/, sans sample IDs
- A log for the whole pipeline is written in --log_dir and in data_release/logs/data_release.log
  - Logs per data source are written in release_dir/logs/data_source_name.log, showing timestamps and any uncorrectable
    errors identified

The release dir will be named after the date it was run and a version number based on how many releases have been done
before, e.g, '20200911_v1'.
"""

import os
import sys
import logging
import argparse
import traceback
from data_linkage import utils, __now__
from data_linkage.release import ReleaseManager


def main(argv=None):
    a = argparse.ArgumentParser()
    a.add_argument(
        '--ls',
        action='store_true',
        help='List all available datasets for cleaning'
    )
    a.add_argument(
        '--db-only',
        action='store_true',
        help='Build the database and log outputs in the release dir, but do not write any released data'
    )
    a.add_argument(
        '--export-only',
        action='store_true',
        help='Do not rebuild the database, but run the SQLite and CSV exports for the existing data'
    )
    a.add_argument('--dry-run', action='store_true')
    a.add_argument(
        '-f',
        '--force',
        action='store_true',
        help="Always perform data cleaning, even if the automation thinks it doesn't need to"
    )
    a.add_argument('-q', '--quiet', action='store_true', help='Turn off logging to stdout')
    a.add_argument('-l', '--log-dir', help='Log to a file in this location')
    a.add_argument('-c', '--capture-warnings', help='Log warnings instead of printing to stderr')
    args = a.parse_args(argv)

    if args.quiet:
        utils.remove_handler(utils.stdout_handler)

    if args.log_dir:
        handler = logging.FileHandler(os.path.join(args.log_dir, f'data_release_{__now__}.log'))
        utils.setup_handler(handler)

    if args.capture_warnings:
        utils.setup_logger('py.warnings')
        logging.captureWarnings(True)

    release = ReleaseManager(args.dry_run, args.force)
    if args.ls:
        release.list_datasets()
        return 0

    try:
        if release.should_run():
            if args.db_only:
                release.build_database()
            else:
                release.full_release()

        elif args.export_only:
            release.run_exports()

        return 0

    except Exception as e:
        release.logger.critical('%s: %s', e.__class__.__name__, e)
        etype, value, tb = sys.exc_info()
        if tb:
            stacktrace = ''.join(traceback.format_exception(etype, value, tb))
            release.logger.critical('Stacktrace:\n' + stacktrace)

        return 1


if __name__ == '__main__':
    exit_status = main()
    sys.exit(exit_status)
