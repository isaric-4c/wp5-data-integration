import re
import csv
import argparse
import datetime
from collections import defaultdict
from data_linkage.db_engine.engine import Database

pattern = re.compile('^([A-Z0-9]{5})-*([0-9]{3,4})$')


class Sample:
    def __init__(self, sample_id, site_code, patient_number, cache):
        self.name = sample_id
        self.given_site_code = site_code
        self.given_patient_number = patient_number
        self.lims_date_collected = None
        date_collected = cache.get_lines('lims', Kit_ID=sample_id)
        if date_collected:
            self.lims_date_collected = datetime.datetime.strptime(date_collected[0]['Date_Collected'], '%Y%m%d').date()
        self.potential_matches = {}

    def add_line(self, line):
        patient_id = line['subjid']
        if patient_id not in self.potential_matches:
            self.potential_matches[patient_id] = MatchCandidate(line)

        self.potential_matches[patient_id].update(line)

    def viable_matches(self):
        if not self.lims_date_collected:
            # no date available - match on name only
            return self.potential_matches.values()

        matches = []
        for k in sorted(self.potential_matches):
            m = self.potential_matches[k]
            if m.first_observation > self.lims_date_collected:
                # sample taken before the patient arrived in hospital - can't be this one
                continue

            if m.latest_observation < self.lims_date_collected:
                # sample taken after the patient left - can't be this one
                continue

            matches.append(m)

        return matches

    def __lt__(self, other):
        return self.name < other.name


class MatchCandidate:
    def __init__(self, top_line):
        self.patient_id = top_line['subjid']
        self.site_code, self.patient_number = pattern.match(self.patient_id).groups()
        self.data_access_group = top_line['redcap_data_access_group']
        self.place = top_line['place_name']
        self.observations = set()

    def update(self, line):
        try:
            obs_date = datetime.datetime.strptime(line['daily_dsstdat'], '%Y-%m-%d').date()
            self.observations.add(obs_date)
        except (ValueError, TypeError):
            pass

    @property
    def first_observation(self):
        if self.observations:
            return sorted(self.observations)[0]

    @property
    def latest_observation(self):
        if self.observations:
            return sorted(self.observations)[-1]


def find_matches(cache, ccp_data_file, *samples_and_given_mappings):
    match_attempts = defaultdict(lambda: defaultdict(list))

    for sample_id, site_code, patient_number in samples_and_given_mappings:
        match_attempts[site_code][patient_number].append(Sample(sample_id, site_code, patient_number, cache))

    with open(ccp_data_file) as f:
        reader = csv.DictReader(f)
        for line in reader:
            site_code = line['dag_id']
            patient_number = line['subjid'].split('-')[1]

            if site_code in match_attempts:
                for k in match_attempts[site_code]:
                    if k in patient_number:
                        for sample in match_attempts[site_code][k]:
                            sample.add_line(line)

    for site_code in sorted(match_attempts):
        for patient_number in sorted(match_attempts[site_code]):
            for sample in match_attempts[site_code][patient_number]:
                print(f'Sample {sample.name}, site code {sample.given_site_code}, patient number {sample.given_patient_number}, collected: {sample.lims_date_collected}')
                print('Potential matches:')

                for m in sample.viable_matches():
                    msg = f'{m.patient_id} - clinical observations between {m.first_observation} and {m.latest_observation}'
                    if sample.lims_date_collected in m.observations:
                        msg += f', including one on the day sample was taken'

                    print(msg)


def main(argv=None):
    a = argparse.ArgumentParser()
    a.add_argument('ccp_file')
    a.add_argument('positionals', nargs='+')
    args = a.parse_args(argv)
    pos = args.positionals

    cache = Database()
    samples_and_mappings = [(pos[i], pos[i + 1], pos[i + 2]) for i in range(0, len(pos), 3)]
    find_matches(cache, args.ccp_file, *samples_and_mappings)


if __name__ == '__main__':
    main()
