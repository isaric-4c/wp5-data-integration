library('readr')
library('dplyr')

statuses <- data.frame(
    score=c(5, 5, 5, 4, 3, 2, 1, NA),
    indication=c(
        'Death/palliative discharge',
        'Death/palliative discharge within 28 days of admission',
        'Death/palliative discharge within 28 days of nosocomial infection',
        'Invasive mechanical ventilation',
        'Noninvasive ventilation',
        'Oxygen therapy',
        'No oxygen requirement',
        NA
    ),
    row.names=c('pdisch', 'pdisch28', 'pdisch28nos', 'imv', 'niv', 'ox', 'nox', 'na')
)


daily_severity <- function(tb) {
    tb %>%

    mutate(
        daily_invasive_prtrt=case_when(
            daily_invasive_prtrt == 'N/A' ~ NA_character_,
            daily_invasive_prtrt == 'YES' ~ 'Yes',
            daily_invasive_prtrt == 'NO' ~ 'No',
            TRUE ~ daily_invasive_prtrt
        ),

        daily_noninvasive_prtrt=case_when(
            daily_noninvasive_prtrt == 'N/A' ~ NA_character_,
            daily_noninvasive_prtrt == 'YES' ~ 'Yes',
            daily_noninvasive_prtrt == 'NO' ~ 'No',
            TRUE ~ daily_noninvasive_prtrt
        ),

        oxy_vsorresu=case_when(
            oxy_vsorresu == 'N/A' ~ NA_character_,
            TRUE ~ oxy_vsorresu
        )
    ) %>%

    mutate(
        tmp_severity=case_when(
            dsterm == 'Death' | dsterm == 'Palliative discharge' ~ 'pdisch',

            daily_invasive_prtrt == 'Yes' ~ 'imv',

            daily_noninvasive_prtrt == 'Yes' ~ 'niv',

            round(daily_fio2_combined, 2) > 0.21 | oxy_vsorresu == 'Oxygen therapy' ~ 'ox',

            (round(daily_fio2_combined, 2) == 0.21 | is.na(daily_fio2_combined)) &
            (oxy_vsorresu == 'Room air' | is.na(oxy_vsorresu)) &
            (daily_invasive_prtrt == 'No' | is.na(daily_invasive_prtrt)) &
            (daily_noninvasive_prtrt == 'No' | is.na(daily_noninvasive_prtrt))
            ~ 'nox',

            TRUE ~ 'na'
        )
    ) %>%

    mutate(
        daily_severity_score=statuses[tmp_severity, 'score'],
        daily_severity_indication=statuses[tmp_severity, 'indication'],
        tmp_severity=NULL
    )
}


peak_severity <- function(tb) {
    tb %>%

    mutate(
        oxy_vsorresu=case_when(
            oxy_vsorresu == 'N/A' ~ NA_character_,
            TRUE ~ oxy_vsorresu
        )
    ) %>%

    mutate(
        tmp_severity=case_when(

            dsterm == 'Unknown' | is.na(dsterm) ~ 'na',

            # death/palliative discharge within 28 days of admission
            (dsterm == 'Death' | dsterm == 'Palliative discharge') &
                !is.na(dsstdtc) &
                !is.na(hostdat) &
                dsstdtc - hostdat < 28
                ~ 'pdisch28',

            # death/palliative discharge within 28 days of nosocomial infection, i.e. after 28 days in hospital, onset > 7 days after admission
            (dsterm == 'Death' | dsterm == 'Palliative discharge') &
                !is.na(dsstdtc) &
                !is.na(hostdat) &
                !is.na(cestdat) &
                dsstdtc - hostdat > 27 &
                cestdat - hostdat > 7 &
                dsstdtc - cestdat < 28
                ~ 'pdisch28nos',

            any_invasive == 'Yes' ~ 'imv',

            any_noninvasive == 'Yes' ~ 'niv',

            any_oxygen == 'Yes' | oxy_vsorresu == 'Oxygen therapy' ~ 'ox',

            (any_oxygen == 'No' | is.na(any_oxygen)) &

                                                                # pipeline cleans 'N/A' -> None, so don't need this workaround there
            (oxy_vsorresu == 'Room air' | is.na(oxy_vsorresu) | oxy_vsorresu == 'N/A') &
            (any_invasive == 'No' | is.na(any_invasive)) &
            (any_noninvasive == 'No' | is.na(any_noninvasive))
            ~ 'nox',

            any_daily_invasive_prtrt == 'No' & any_daily_noninvasive_prtrt == 'No' ~ 'nox',

            TRUE ~ 'na'
        )

    ) %>%

    mutate(
        peak_severity_score=statuses[tmp_severity, 'score'],
        peak_severity_indication=statuses[tmp_severity, 'indication'],
        tmp_severity=NULL
    )
}


main <- function() {
    args <- commandArgs(trailingOnly=TRUE)
    mode <- args[1]
    input_file <- args[2]
    output_file <- args[3]

    print(statuses)

    if (is.na(input_file) | !file.exists(input_file) | is.na(output_file) | !(mode %in% c('peak', 'daily'))) {
        print('Usage: Rscript severity_scoring.r <mode> <input_file> <output_file>')
        print("  Where mode is either 'peak' or 'daily'")
        return(NA)
    }

    data <- read_csv(input_file)

    if (mode == 'peak') {
        data <- peak_severity(data)
    } else if (mode == 'daily') {
        data <- daily_severity(data)
    }

    write_csv(data, output_file)

}

main()
