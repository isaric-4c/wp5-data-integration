"""
Initial script for doing some checks on the WP5 RNA-seq facility report. Eventually all the data aggregation will be in
SQL joins. This script:

- loads up the data from lims_ccp_flat.csv and RNA_Data_ISARIC_All.xlsx and reads them, reporting any errors
- compares all the samples per patient, figures out what 'Day' each one is depending on 'Date Taken', stores this as a
  column called 'Calculated Day', and reports any instances of this mismatching the column 'Day'
- checks the 'Date Taken' column in the RNA data against the 'Date_Collected' column in the Lims/CCP data, and reports
  any mismatches

Warning: on current (26th Aug 2020) production data, the parsers report a LOT of errors. Pipe stdout into `less` if you
want to avoid spamming your terminal.

"""

import sys
import csv
import argparse
from data_linkage.utils import setup_logger, stdout_handler
from data_linkage.data_sources import CCPLIMSFlat, RNASeqFacilityReport


logger = setup_logger(__name__)


def linkage(output_stream, error_file=None, annotate=False):
    ccp_lims = CCPLIMSFlat()
    rnaseq = RNASeqFacilityReport()

    rnaseq.load_data()

    for isaric_id, samples in rnaseq.data_by_isaric_id.items():
        # This part is fairly strict about reporting errors. If any samples are inconsistent with each other, they are
        # reported
        try:
            sorted_samples = sorted(samples.values(), key=lambda s: s['Date Taken'])
            day1 = sorted_samples[0]['Date Taken']

            for s in sorted_samples:
                sample_day = s['Date Taken']
                day_delta = sample_day - day1
                # at the moment this is not quantised to Day 1, 3, 9, etc, because 'Date Taken' RNA data doesn't seem to
                # be either
                calculated_day = day_delta.days + 1

                s.line['Calculated Day'] = calculated_day
                if calculated_day != s['Day']:
                    s.errors.add(
                        "Calculated day '%s' does not match entered day '%s'" % (
                            calculated_day,
                            s['Day']
                        )
                    )
        except (ValueError, TypeError) as e:
            for s in samples.values():
                s.errors.add(
                    'Could not determine Calculated Day from Date Taken values %s for samples %s' % (
                        ', '.join(str(s.get('Date Taken')) for s in samples.values()),
                        ', '.join(str(s['Sample ID']) for s in samples.values())
                    )
                )

        for sample_id, data_point in samples.items():
            # Todo: this duplicates CCPLims.validate_sample_data. We won't need this once we have parsers inserting clean data into Postgres
            ccp_lims_sample_data = ccp_lims.cache.get_lines(KitID=sample_id)
            date_collected = set(line['Date_Collected'] for line in ccp_lims_sample_data)
            if not date_collected:
                # turning off for now
                # data_point.errors.add('Date_Collected (CCP-Lims): no value found in CCP/Lims data for this sample')
                continue

            elif len(date_collected) > 1:
                data_point.errors.add(
                    'Date_Collected (CCP-Lims): inconsistent values found for this sample: %s' % ', '.join(str(d) for d in date_collected)
                )
                continue

            try:
                date_collected = date_collected.pop()
                date_taken = data_point['Date Taken']
                if date_taken != date_collected:
                    data_point.errors.add('Date Taken: mismatch with CCP/Lims Date_Collected %s' % date_collected)
            except (ValueError, TypeError):
                data_point.errors.add('Date Taken: could not compare with CCP/Lims Date_Collected %s' % date_collected)

    data_points = []
    for isaric_id, samples in rnaseq.data_by_isaric_id.items():
        data_points.extend(samples.values())

    def output_data_points(data_points, output_stream):
        header = [col.name for col in rnaseq.columns]
        header.insert(4, 'Calculated Day')

        output_writer = csv.writer(output_stream)
        if annotate:
            output_writer.writerow(header + ['Errors'])
        else:
            output_writer.writerow(header)

        for sample in data_points:
            line = [sample.line.get(k) for k in header]
            if annotate:
                line.append(', '.join(sorted(sample.errors)))

            output_writer.writerow(line)

    output_data_points(data_points, output_stream)
    if error_file:
        with open(error_file, 'w') as f:
            output_data_points([dp for dp in data_points if dp.errors], f)


def main():
    a = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    a.add_argument('-o', '--output', help='File to output the checked data to. Defaults to stdout')
    a.add_argument('-a', '--annotate', action='store_true', help='Append any errors identified as a column in the output/error file')
    a.add_argument('-e', '--errors', help='Output all errored lines to a file')
    a.add_argument('-q', '--quiet', action='store_true')
    args = a.parse_args()

    if args.output:
        output_stream = open(args.output, 'w', newline='')
    else:
        output_stream = sys.stdout

    if args.quiet:
        stdout_handler.setLevel(50)

    linkage(output_stream, args.errors, args.annotate)


if __name__ == '__main__':
    main()
