# ISARIC Data Integration

This repo contains a data linkage pipeline for cleaning and improving linkability of data across the different Work
Packages in ISARIC.

- `bin`: Contains the main entry point, `data_release.py`
- `data_linkage`: Python module containing the pipeline and back-end database interface
- `tests`: Unit tests, example configs and test data

Core dependencies for this pipeline are:
- Python 3.8
- Pandas
- SQLAlchemy
- Postgres client libraries (optional)
- SQLite3


## Usage

Dependency management can be done through Conda:

    $ conda env create -p path/to/env --file environment.yml

Or if you have a Python interpreter already set up and wish to use pip:

    $ pip install -r requirements.txt

Config files for this pipeline are pointed to with environment variables:

- ISARIC_LINKAGE_CONFIG - main config file, see example at tests/assets/config.yaml
- PGPASSFILE - Postgres [pgpass](https://www.postgresql.org/docs/current/libpq-pgpass.html) file, containing database
  credentials. Required if using with Postgres. Defaults to ~/.pgpass.

Once the above configuration is done and the right Conda/Python environment is active, the pipeline can be run with:

    python bin/data_release.py

This will scan the available system, read input data into data frames, clean them and push to the configured database.
Some command line options are available to make it easier to run with resource managers - the command:

    python bin/data_release.py -q -l path/to/logs

will turn off stdout (`-q`), and save a full log in the specified directory (`-l path/to/logs`). The option `-h` can be
used to list all available command-line options.


## Database back end

The pipeline will push tables to a SQLite database in the configured release location, and will also push to a Postgres
database if configured. All known tables are dropped and then rebuilt - so historical data is not currently kept in the
live database. Database snapshots are kept in each data release folder.

Only known tables are dropped and rebuilt, so if the database is being used for other things then other
tables should not be affected unless there are naming conflicts - although it's not recommended to do this.


## Development

Since the input data is constantly changing, this pipeline needs to be updated accordingly. The general architecture of
the pipeline is:

- db_engine - the SQLAlchemy database interface
- release - the pipeline that calls everything in the right order
- data_sources - classes that describe all the tables to be added to the database
  - primary_data_sources
    - assays
  - views
  - secondary_data_sources

Primary data sources take a source of raw data and use it to populate a clean SQL table. These include clinical and LIMS
data, lab assays and linkage and metadata tables. These will be loaded in the right order in the release process,
cleaned using the linkage tables and LIMS (which themselves are data sources, hence why they have to be loaded in the
right order) and inserted into SQL via `pandas.to_sql`. Sample-centric lab assays are described using the superclass
`Assay`, which contains extra functionality for reporting and exporting. 

The other data sources are built on top of other data sources. Views define SQL views that perform preset linkages on
the underlying clean data.

Secondary data sources load data from views and primary sources, transform it and insert back into SQL as a new table.
These include datasets transformed between long and wide form, the sample prioritisation report, and other reports that
we want to be available as SQL tables.

### Automated testing

This pipeline is validated in two ways:

- unit tests of individual components
- end-to-end tests of the pipeline on example data - see tests/test_build.py

These tests can be run in a development environment with:

    PYTHONPATH=. ISARIC_LINKAGE_CONFIG=tests/assets/config.yaml python -m unittest

To see Pandas memory warnings, set `PYTHONTRACEMALLOC=1`.

If a Postgres Docker container called `isaric_db` is running locally, then the end-to-end tests will use it. If no
container is running but Docker is available, the pipeline will attempt to set one up, use it then stop and remove it
afterwards. Otherwise, the end-to-end Postgres test will be skipped. The Postgres build can also be turned off even when
Docker is available by setting `NO_PGBUILD=true`.

#### GitLab CI

The tests are run automatically upon pushing to GitLab, via [GitLab CI](https://docs.gitlab.com/ee/ci/README.html). See
the entrypoint file at `.gitlab-ci.yaml`. This build uses a Postgres container via GitLab CI Services, and points to it
with tests/assets/ci_pgpass.


### Updating a data source

This may be required, e.g. if the format/columns of a dataset's input data changes.

- Adding/removing columns: update the attribute `columns`, making sure all Column objects have either a direct match to
  the input data, or a mapping to it via `rename_columns`. 
- Column name change: to update what the SQL column will be called, update the relevant column name in the attribute
  `columns`; updating `rename_columns` to rename the column from the raw data if necessary.
- Fixing a column cleaning/casting error: debug the function from cleaning.py being called via the column type, plus any
  definition of `clean_dataframe` by the class (ensuring that any subclasses always call `super().clean_dataframe()`). 

### Adding a new data source

This will be required e.g. if a new assay becomes available to ISARIC, or we want to add a new report or linkage preset.

- Add a new class in the module `data_linkage.data_sources`
  - If adding a new type of input metadata, this will be a new PrimaryDataSource. Define the attributes:
    - `name` - the work package if applicable, plus a brief descriptive title, e.g. `wp6_plasma_cytokines`. This will be
      used to name the SQL table and the config file section used to specify input data locations  
    - `description` - brief description of the dataset
    - `columns` - SQLAlchemy Column objects describing colnames, data types and any primary keys for the final cleaned dataset
      - When naming columns, bear in mind that Postgres doesn't like certain punctuation characters in column names
      - The usual SQLAlchemy data types are available, plus some custom ones defined in cleaning.py
        - Use PatientID to define an ISARIC patient ID and validate/clean it as such
        - Use Timepoint to define a sampling timepoint and clean it into either the format `Day x` or `Convalescent`
        - Use SampleID to define a sample ID and tell the pipeline to exclude it when exporting to the NHS safe haven
      - If possible, add a description for the column as well - this wil show up in the output of `\d+ <table>` in
        Postgres.
    - `rename_columns` - any remappings of column names from the input data to the final clean SQL colnames
    - `dirty_sample_id_col` - which column in `columns` represents the sample ID given in the raw data. This may contain
      barcodes or kit IDs, even within the same dataset - the pipeline has had to be able to tolerate this because so many
      assays use different ways of referencing a sample depending on where it comes from
    - `clean_kit_id_col` - which column in `columns` represents the cleaned kit ID that can be used to refer to the LIMS and
      across other assays. Generally this will not be pre-existing in the input data, and will be created and populated by
      the pipeline.
    - `clean_dataframe` - optional override for custom data cleaning for this dataset. Ensure to call
      `super().clean_dataframe`
    
  - If adding a new sample-centric lab assay, use the Assay superclass. Generally assays are split up into one file per
    work package, e.g. `wp5_rnaseq.py`. Usage is identical to PrimaryDataSource except:
      - `export_safe_haven` defaults to True, so does not need to be set explicitly
      - `report_coverage_as` - set this to report the assay as a different name in the sample coverage reports (e.g.
        report 'wp5_wgs_upstream_gvcfs' as 'wp5_wgs'), or None to exclude it. Defaults to cls.name.

  - If adding a new linkage preset or pure-SQL report, use the View superclass:
    - `name`, `description` and `export_as` as for PrimaryDataSource
    - `select_statement` - this needs to be a `@property`, defining a SQLAlchemy select statement for the view.
      Reference other tables using the `all_primary_data_sources`, `all_views` and `all_secondary_data_sources` dicts,
      e.g. `all_primary_data_sources['lims_data']`
  
  - If adding a new report or transformed dataset requiring custom dataframe manipulation, this will be a
    SecondaryDataSource:
    - `name`, `description` and `export_as` as for PrimaryDataSource
    - `output_data` - method that runs a SQL query on other Views and PrimaryDataSources and returns a pandas DataFrame.
      Reference other tables with `all_primary_data_sources` and `all_views`, e.g. `all_views['view_assays_done'].table`

- Add test data in `tests/assets/`. This should approximately reflect what would be seen in production, but should not
  contain any real data. Add example input data at `tests/assets/<dataset_name>/<dataset_<date>.csv`, and if the new
  dataset is sample-centric, add new test samples to the LIMS data at `tests/assets/lims_data/`, with sensible sample
  types and example dispatch locations. Generally, it's a good idea to include a sample in the input data with a
  deliberately unlinkable ID, to ensure that it's handled properly.

- Add example output data in `tests/assets/expected_outputs/<dataset.name>.csv`. This should represent what the raw test
  data looks like once it's been cleaned. If you get different results in the SQLite and Postgres builds (e.g. different
  sorting, display of certain data types), then you can account for this by adding `<dataset.name>_postgres.csv` and
  `<dataset.name>_sqlite.csv`. These expected data files are loaded dynamically into unittest cases.

- Add the dataset name to MainExport in `data_exports.isaric_research_environment` to export it to CSV in the release
  folder
  - You may not want to add the SQL table, but rather a view that links in some other metadata. For example, the WP6
    plasma cytokine data exports `view_wp6_plasma_cytokines` to CSV, but not the raw SQL table.
  - Only specified datasets are exported to CSV, but everything is exported to SQLite automatically 

- If the table is a new lab dataset, add the data name to SafeHavenExport in `data_exports.nhs_safe_haven`

- Ensure that:
  - The class is imported properly so it's available to `all_primary_data_sources`, `all_views` or
    `all_secondary_data_sources`
  - If it's a primary data source, a config for it exists in tests/assets/config.yaml, pointing to the test data
  - If it's an Assay, cls.name is present in data_sources.sample_assays.SampleAssays

### Testing and production environments

When changes as above are done, it's generally advisable to test the changes before putting them into production. This
involves having a separate testing or preview environment:

```
production_environment/
    wp5-data-integration/
    logs/
    config.yaml
    run_data_linkage.sh  # script that can be called on a Cron job - activates environment, sets any env vars, and calls the pipeline

testing_environment/
    wp5-data-integration/  # separate Git repo. Can be checked out to different versions, branches, etc, independently of production
    logs/
    config.yaml  # separate config. Input data should be the same as production, but set the database engine to SQLite, and outputs to ~home
    release/  # test release outputs
        isaric.sqlite  # test instance of the ISARIC database, analogous to c19-isaric01 in production
```

The testing environment may (or even should) use the same input data as production, but point to a separate database and
data release location. The pipeline is able to use a SQLite back-end for this purpose. The development cycle for the
pipeline is usually along the lines of:

- Make changes, running unit tests locally on test data in dev environment
- Push changes to version control
- Go to testing/preview environment, pull the changes and run, checking for errors or unexpected behaviour caused by
  real-world messy data. Fix as needed.
- Open a Merge Request and get a new release done as per Contributing.md
- Go to production environment, pull changes and run
