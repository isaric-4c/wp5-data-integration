# Changelog

## v1.14
7th Apr 2022

- Merged view_full... linkages into view_full_sample_linkage
- New tables: wp5_rnaseq_data, wp5_rnaseq_pseudotimes
- Renamed wp5_wgs_upstream_gvcfs to wp5_wgs, expanded to other WGS sources
- Renamed wp5_rnaseq_gene_counts to wp5_rnaseq_analysis, added run_id, multiqc_salmon_file
- Added bam_file, vcf_file, kraken_file to wp5_rnaseq_viral_read_counts
- Doc updates


## v1.13
9th Feb 2022

- New idamac_complement data format
- New severity views - view_daily_severity, view_peak_severity
  - Integrated into complement, cytokine and biochem views
  - R version of severity scoring in bin/

- Linkage updates
  - Added `view_full_sample_patient_linkage`, linking from any known sample ID to patients
  - Added `view_full_sample_kit_linkage`, linking from any known sample ID to kits
  - Renamed `linkage` -> `linkage_sample_to_patient`
  - Renamed `wp5_rnaseq_sample_linkage` -> `linkage_sample_to_sample`

- Exported CSVs now named directly after the dataset name
- New command line options
  - `--db-only` to only build the database
  - `--export-only` to only run data exports from the current database
  - `--ls` to list datasets

- Removed top-level Readme, consolidated all Readme content into `<release_folder>/Readme.md` 
- Re-enabled pgdump Postgres backups
- Option to set file permissions on data exports  
- Overhauled the way data sources reference each other at run time
- Fixed views missing from exports
- Reduced terminal spam via Pandas/NumPy fixes, logging.captureWarnings and stacktrace logging


## v1.12.1
7th Dec 2021

- Fixed wp6_anticytokineantibodies table exports
- Updated documentation for adding tables to main data exports


## v1.12
7th Dec 2021

- Simplified SQLAlchemy engine/tables
- Configurable class-based data exports
- New tables:
  - wp6_anticytokineantibodies_iga
  - wp6_anticytokineantibodies_igg


## v1.11
15th Oct 2021

- New column: `lims_data.clean_sample_type`
- New severity scoring algorithms, available in columns `severity_score` and `severity_indication`
  - Daily severity in `clinical_data`, peak severity in `clinical_oneline`


## v1.10
4th Oct 2021

- New table: `phosp_lims_data`
- Updated complement table: added column `freeze_thaws`
- New contributing policy

## v1.9
12th Aug 2021

- New datasets
  - WP5 proteomics - proteins, peptides, batch metadata
  - IDAMAC complement

## v1.8
3rd Aug 2021

- Fixed sample dispatch assignment for Glasgow samples
- New LIMS format
  - Stored and dispatched samples now stored as one table
  - Freezer locations for undispatched samples

## v1.7
22nd Jul 2021

- Added WP6 nasosorption mediators
- Fixed duplicate lines in LIMS joins

## v1.6
16th Jul 2021

- Fixed onset-to-sample column in serology linkage
- Renamed wp3_viral_load to wp3_blood_viral_load
- Added view_lims_kits
- Added hostdat and admission2sample to plasma cytokine linkage
- New NHS safe haven export format
- Refactor

## v1.5
22nd Jun 2021

- New datasets
  - WP5 WGS
  - WP5 RNA-seq viral read counts
  - WP6 faecal cytokines

## v1.4
31st May 2021

- Automated documentation release
- New cleaning model
  - Cleaning in Pandas before SQL insertion
  - Added clean kit ID column to all sample-centric datasets
  - Cleaning kits, barcodes and truncated barcodes into a kit ID

## v1.3
27th Apr 2021

- Deprecated clinical oneline table in favour of oneline
- Added onset information to plasma cytokine linkage
- Refactor

## v1.2
19th Apr 2021

- Added Glasgow samples to LIMS dispatch report
- Renamed serology datasets from WP6 to WP7
- Added WP7 serology neutralisation

## v1.1
9th Apr 2021

- Doc updates
- Added WP3 viral load

## v1.0
9th Apr 2021

- Postgres back end
  - Drops known tables and rebuilds each time
  - pgdump archiving

## v0.13
18th Mar 2021

- Cytokines
  - Added cleaning for column names
  - Better cleaning for patients/timepoints

## v0.12
5th Mar 2021

- Added cleaning for ints, floats and clusters PF in RNA-seq report

## v0.11
19th Feb 2021

- Added NHS safe haven export
  - CSV tables
  - Dynamic SQL schemas

## v0.10
29th Jan 2021

- Overhauled back end
  - Pandas instead of R Tidyverse 
  - Explicit columns for all datasets except clinical data (over 800 columns, changes all the time, already clean)
  - Added data cleaning for booleans

- Added LIMS dispatched samples table, wide-form sample status report
- Fixed loading of TSV files
- Loading tables into SQL then exporting to CSV
- Added sample barcode to LIMS data

## v0.9
18th Nov 2020

- Added serology tables

## v0.8
4th Nov 2020

- Added RNA-seq filtered samples table
- Added config for ignoring RNA-seq pipeline runs

## v0.7
30th Oct 2020

- Added reports to data release
- Added RNA-seq -> LIMS -> clinical data view

## v0.6 
15th Oct 2020

- Switched data cleaning to R Tidyverse
- Linkage tables now used for data cleaning

## v0.5
8th Oct 2020

- Added RNA-seq facility report
- New LIMS data format
- Fixed viral load columns

## v0.4
24th Sept 2020

- New clinical schema
- Stamping output files with date instead of version name
- More lenient parsing of columns
- Added RNA-seq linkage table

## v0.3
18th Sept 2020

- Refactor
- Added viral load parser
- Added docs/ folder

## v0.2
17th Sept 2020

- Added report for missing assays per sample

## v0.1
16th Sept 2020

- Initial release
- Parser classes performing validation
- Release manager collecting CSVs into release folder
- SQLite caching